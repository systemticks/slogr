/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl.tests

import com.google.inject.Inject
import de.systemticks.slogr.dsl.qDsl.QModel
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.Assert.assertEquals
import de.systemticks.slogr.dsl.generator.QDslQueryGenerator
import de.systemticks.slogr.dsl.qDsl.QPatternMatch

@ExtendWith(InjectionExtension)
@InjectWith(QDslInjectorProvider)
class QDslPatternResolverTest {
	@Inject
	ParseHelper<QModel> parseHelper
	@Test
	def void simplePattern() {
		val model = parseHelper.parse('''
			pattern NUMBER: "\\d+"
		''')
		
		val pattern = model.patterns.last
		assertEquals('NUMBER', pattern.name)
		
		val generator = new QDslQueryGenerator		 
		assertEquals('\\d+', generator.resolvePattern(pattern, newArrayList ))						
	}

	@Test
	def void referencedPattern() {
		val model = parseHelper.parse('''
			pattern NUMBER: "\\d+"
			pattern OTHER_NUMBER: %{NUMBER}
		''')
		
		val pattern = model.patterns.last
		assertEquals('OTHER_NUMBER', pattern.name)
		
		val generator = new QDslQueryGenerator		 
		assertEquals('\\d+', generator.resolvePattern(pattern, newArrayList ))						
	}
	
	@Test
	def void mixedContentPattern() {
		val model = parseHelper.parse('''
			pattern NUMBER: "\\d+"
			pattern FLOAT: %{NUMBER}"\\."%{NUMBER}
		''')
		
		val pattern = model.patterns.last
		assertEquals('FLOAT', pattern.name)
		
		val generator = new QDslQueryGenerator		 
		assertEquals('\\d+\\.\\d+', generator.resolvePattern(pattern, newArrayList ))						
	}

	@Test
	def void nestedPatternWithKeys() {
		val model = parseHelper.parse('''
			pattern NUMBER: "\\d+"
			pattern WORD: "\\w+"
			pattern SERVICE: %{WORD:name}"#"%{NUMBER:id}
		''')
		
		val pattern = model.patterns.last
		assertEquals('SERVICE', pattern.name)
		
		val generator = new QDslQueryGenerator		 
		assertEquals('\\w+#\\d+', generator.resolvePattern(pattern, newArrayList ))						
	}
	
	@Test
	def void substitute() {
		val model = parseHelper.parse('''
			pattern NUMBER: "\\d+"
			pattern WORD: "\\w+"
			pattern SERVICE: %{WORD:name}"#"%{NUMBER:id}
			query test: field matches SERVICE{name="myName", id="myId"}
		''')
				
		val match = model.eAllContents.filter[e|e instanceof QPatternMatch].last as QPatternMatch
		
		val generator = new QDslQueryGenerator		 
		assertEquals('myName#myId', generator.resolvePattern(match.ref, match.subs ))						
		
		
	}
	
}
