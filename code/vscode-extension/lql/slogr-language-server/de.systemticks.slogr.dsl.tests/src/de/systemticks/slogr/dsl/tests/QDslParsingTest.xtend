/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl.tests

import com.google.inject.Inject
import de.systemticks.slogr.dsl.qDsl.QModel
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.Assert.assertEquals
import de.systemticks.slogr.dsl.qDsl.QDslFactory
import static extension de.systemticks.slogr.dsl.generator.QDslGeneratorUtils.*

@ExtendWith(InjectionExtension)
@InjectWith(QDslInjectorProvider)
class QDslParsingTest {
	@Inject
	ParseHelper<QModel> parseHelper
	@Test
	def void filter1() {
		val model = parseHelper.parse('''
			query other : logLevel = "INFO"
		''')
		
		assertEquals('other', model.queries.last.name) 
						
	}
	
	@Test
	def void filter2() {
		val model = parseHelper.parse('''
			query newFilter : (not appId = "UI") and (contextId ~ 'IF1' or payload ? '.*PHONE.*')
		''')
		
		assertEquals('newFilter', model.queries.last.name) 
							
	}
	
	@Test
	def void filter3()
	{
		val model = parseHelper.parse('''
			query if1 : appId = "UI" and contextId ~ "IF1" 
			query phone: if1 and not (logLevel = "WARNING")
		''')	
		
		assertEquals('if1', model.queries.head.name) 
		assertEquals('phone', model.queries.last.name) 
		
	}		
	
	@Test
	def void filter4()
	{
		val model = parseHelper.parse('''
			group base (
			    filter if1 : appId = "UI" and contextId = "IF1"
			    filter phone: not (payload ~ "PHONE1234")
			)
			
			
			filter newFilter : not (base.if1 and base.phone)
		''')
				
	}
	
	@Test	
	def void getKeyWithoutPath() {
				
		val factory = QDslFactory.eINSTANCE;
		
		val field = factory.createQFieldExpr
		field.name = "topLevel"
		
		assertEquals('topLevel', field.key);			
	}
	
	@Test	
	def void getKeyWithPathOneLevel() {
				
		val factory = QDslFactory.eINSTANCE;
		
		val field = factory.createQFieldExpr
		field.name = "topLevel"
		field.path = factory.createQPath
		field.path.pathElement.add('subLevel1')
		
		assertEquals('topLevel.subLevel1', field.key);			
	}

	@Test	
	def void getKeyWithPathTwoLevels() {
				
		val factory = QDslFactory.eINSTANCE;
		
		val field = factory.createQFieldExpr
		field.name = "topLevel"
		field.path = factory.createQPath
		field.path.pathElement.add('subLevel1')
		field.path.pathElement.add('subLevel2')
		
		assertEquals('topLevel.subLevel1.subLevel2', field.key);			
	}
	
}
