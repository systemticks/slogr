package de.systemticks.slogr.dsl.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.systemticks.slogr.dsl.generator.PyGrokDictGenerator;
import de.systemticks.slogr.dsl.qDsl.QModel;
import de.systemticks.slogr.dsl.qDsl.QPattern;

import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.util.ParseHelper;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(InjectionExtension.class)
@InjectWith(QDslInjectorProvider.class)
public class PyGrokDictGeneratorTest {
    
	@Inject
	ParseHelper<QModel> parseHelper;
    private PyGrokDictGenerator generator;

    private static String BUILT_IN_PATTERN = 
        "pattern SPACE: \"\\\\s*\"" +
        "pattern GREEDYDATA: \".*\"" +
        "pattern DATA: \".*?\"" +
        "pattern YEAR: \"(?>\\\\d\\\\d){1,2}\"" +
        "pattern MONTHNUM: \"(?:0?[1-9]|1[0-2])\"" +
        "pattern MONTHDAY: \"(?:(?:0[1-9])|(?:[12][0-9])|(?:3[01])|[1-9])\"" +
        "pattern HOUR: \"(?:2[0123]|[01]?[0-9])\"" +
        "pattern MINUTE: \"(?:[0-5][0-9])\"" +
        "pattern SECOND: \"(?:(?:[0-5]?[0-9]|60)(?:[:.,][0-9]+)?)\"" +
        "pattern ISO8601_TIMEZONE: \"(?:Z|[+-]\"%{HOUR}\"(?::?\"%{MINUTE}\"))\"" +
        "pattern TIMESTAMP: %{YEAR}\"-\"%{MONTHNUM}\"-\"%{MONTHDAY}%{SPACE}%{HOUR}\":\"%{MINUTE}\":\"%{SECOND}" +
        "pattern TIMESTAMP_ISO8601: %{YEAR}\"-\"%{MONTHNUM}\"-\"%{MONTHDAY}\"[T ]\"%{HOUR}\":?\"%{MINUTE}\"(?::?\"%{SECOND}\")?\"%{ISO8601_TIMEZONE}\"?\"" +
        "pattern LOGLEVEL: \"(TRACE|DEBUG|INFO|WARNING|ERROR)\"" +
        "pattern IMPORTER: %{TIMESTAMP:ts}%{SPACE}%{LOGLEVEL:loglevel}%{SPACE}%{GREEDYDATA:message}";

    @BeforeEach
    public void setup() {
        generator = new PyGrokDictGenerator();
    }

    @ParameterizedTest
    @MethodSource("provideTransformedPattern")    
    void transformSimplePattern(String dslPattern, String pyPattern) throws Exception {

        QPattern pattern = parseHelper.parse(dslPattern).getPatterns().get(0) ;

        assertThat(generator.transformPattern(pattern)).isEqualTo(pyPattern);
    }

    @Test
    void transformPatternWithReferences() throws Exception {
        QModel model = parseHelper.parse(BUILT_IN_PATTERN);
        QPattern timestampPattern = model.getPatterns().stream().filter( pattern -> pattern.getName().equals("TIMESTAMP")).findFirst().get();

        String expected = "%{YEAR}-%{MONTHNUM}-%{MONTHDAY}%{SPACE}%{HOUR}:%{MINUTE}:%{SECOND}";

        assertThat(generator.transformPattern(timestampPattern)).isEqualTo(expected);
    }

    @Test
    void transformImporterPattern() throws Exception {
        QModel model = parseHelper.parse(BUILT_IN_PATTERN);
        QPattern importerPattern = model.getPatterns().stream().filter( pattern -> pattern.getName().equals("IMPORTER")).findFirst().get();

        String expected = "%{TIMESTAMP:ts}%{SPACE}%{LOGLEVEL:loglevel}%{SPACE}%{GREEDYDATA:message}";

        assertThat(generator.transformPattern(importerPattern)).isEqualTo(expected);
    }

    @Test
    void rerieveAllReferencedCustomPaterns() throws Exception {

        QModel model = parseHelper.parse(BUILT_IN_PATTERN);
        QPattern importPattern = model.getPatterns().stream().filter( pattern -> pattern.getName().equals("IMPORTER")).findFirst().get();

        Set<QPattern> customPatterns = generator.determineCustomPatterns(importPattern, new HashSet<>());

        assertThat(customPatterns.stream().map(QPattern::getName)).
            containsExactlyInAnyOrder("TIMESTAMP", "YEAR", "MONTHNUM", "MONTHDAY", "HOUR", "MINUTE", "SECOND", "LOGLEVEL", "GREEDYDATA", "SPACE");

    }

   @Test
   void toJson() throws Exception {

        QModel model = parseHelper.parse(BUILT_IN_PATTERN);
        QPattern importPattern = model.getPatterns().stream().filter( pattern -> pattern.getName().equals("IMPORTER")).findFirst().get();

        String jsonStr = generator.toJson(importPattern);

        JsonElement obj = JsonParser.parseString(jsonStr);
        assertThat(obj.getAsJsonObject().get("pattern").getAsString()).isEqualTo("%{TIMESTAMP:ts}%{SPACE}%{LOGLEVEL:loglevel}%{SPACE}%{GREEDYDATA:message}");

        JsonObject custom = obj.getAsJsonObject().get("custom_patterns").getAsJsonObject();
        assertThat(custom.get("HOUR").getAsString()).isNotEmpty();

    }

    private static Stream<Arguments> provideTransformedPattern() {
        return Stream.of(
            Arguments.of("pattern HOUR: \"(?:2[0123]|[01]?[0-9])\"", "(?:2[0123]|[01]?[0-9])"),
            Arguments.of("pattern GREEDYDATA: \".*\"", ".*"),
            Arguments.of("pattern SPACE: \"\\\\s*\"", "\\s*")
          );
    }


}
