/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl.generator

import de.systemticks.slogr.dsl.qDsl.CCheck
import de.systemticks.slogr.dsl.qDsl.QFieldExpr
import de.systemticks.slogr.dsl.qDsl.QQuery
import org.eclipse.emf.ecore.resource.Resource

class QDslGeneratorUtils {
	
	def static getFileName(QQuery query) {
		query.eResource.fileName
	}
	
	def static getFileName(CCheck check) {
		check.eResource.fileName		
	}

	def static getPythonFileName(CCheck check) {
		"test_"+check.name.toLowerCase+".py"
	}
	
	def static getQueryRootDirPath(QQuery query) {
		
	}

	def static getFileNameWithoutExtension(QQuery query) {
		query.eResource.fileNameWithoutExt
	}
	
	def static getFileNameWithoutExtension(CCheck check) {
		check.eResource.fileNameWithoutExt		
	}
	
	def static getKey(QFieldExpr field) {
		if(field.path === null)
			field.name
		else 
			field.name + '.' + field.path.pathElement.join('.')	
	}
	
	private def static getFileName(Resource resource) {
		resource.URI.toFileString
	}

	private def static getFileNameWithoutExt(Resource resource) {
		resource.URI.lastSegment.split('\\.').head
	}
	
}