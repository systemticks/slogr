/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl.generator

import de.systemticks.slogr.dsl.qDsl.Operation
import de.systemticks.slogr.dsl.qDsl.QCompareRule
import de.systemticks.slogr.dsl.qDsl.QExpression
import de.systemticks.slogr.dsl.qDsl.QFieldExpr
import de.systemticks.slogr.dsl.qDsl.QModel
import de.systemticks.slogr.dsl.qDsl.QNoNameQuery
import de.systemticks.slogr.dsl.qDsl.QPattern
import de.systemticks.slogr.dsl.qDsl.QPatternMatch
import de.systemticks.slogr.dsl.qDsl.QPatternRef
import de.systemticks.slogr.dsl.qDsl.QPatternSemantic
import de.systemticks.slogr.dsl.qDsl.QPatternSubsitute
import de.systemticks.slogr.dsl.qDsl.QQuery
import de.systemticks.slogr.dsl.qDsl.QQueryGroup
import de.systemticks.slogr.dsl.qDsl.QQueryRef
import de.systemticks.slogr.dsl.qDsl.QStringCompare
import de.systemticks.slogr.dsl.qDsl.QSubExpr
import de.systemticks.slogr.dsl.qDsl.StringCompareOperator
import java.util.List

import static extension de.systemticks.slogr.dsl.generator.QDslGeneratorUtils.*
import de.systemticks.slogr.dsl.qDsl.QExactSearchList

class QDslQueryGenerator {
	
	val ANONYMOUS_QUERY_CLASSNAME = "NoNameQuery"

	def transformModel(QModel model)
	{
		if(model.silent !== null)
		{
			transformNoNameQuery(model.silent)
		}
		else 
		{
			transformNamedQueries(model)
		}
	}
	
	private def transformNamedQueries(QModel model)
	{
		'''
			# CAUTION: DO NOT EDIT
			# This is generated code from «class.name»
			# It contains following query classes :
			«FOR q: model.eAllContents.toIterable.filter(QQuery)»
				# Query: «q.fileNameWithoutExtension».«q.createClassName»|«q.createFQN»
			«ENDFOR»
			
			
			from query_builder import *
			«FOR q: model.eAllContents.toIterable.filter(QQuery)»
				«q.transformQuery»
			«ENDFOR»
		'''
	}	
	
	private def transformQuery(QQuery query)
	{
		'''
			
			
			class «query.createClassName»():
			
			    def build(self):
			        return «query.expr.transformExpression».build()
		'''
	}

	private def transformNoNameQuery(QNoNameQuery query)
	{
		'''
			from query_builder import *
			
			
			class «ANONYMOUS_QUERY_CLASSNAME»():
			
			    def build(self):
			        return «query.expr.transformExpression».build()
		'''
	}
	
	private def createFQN(QQuery query)
	{
		if (query.eContainer instanceof QQueryGroup) 
			(query.eContainer as QQueryGroup).name + "." + query.name
		else 
			query.name
	}
	
	private def createClassName(QQuery query)
	{
		if (query.eContainer instanceof QQueryGroup) 
			(query.eContainer as QQueryGroup).name.toFirstUpper + query.name.toFirstUpper
		else 
			query.name.toFirstUpper
	}

	def dispatch String transformExpression(QExpression expr)
	{
		'no transformExpression for' + expr.class.name		
	}

	def dispatch String transformExpression(Operation operation)
	{		
		'''«operation.op.literal.toFirstUpper»(«operation.left.transformExpression», «operation.right.transformExpression»)'''
	}
	
	def dispatch String transformExpression(QSubExpr subExpression)
	{
 		
		if(subExpression.not)
			'''Not(«subExpression.sub.transformExpression»)'''
		else
			subExpression.sub.transformExpression

	}

	def dispatch String transformExpression(QQueryRef reference)
	{
		'''«reference.ref.createClassName»()'''
	}
	
	
	def dispatch String transformExpression(QFieldExpr fieldExpr)
	{
		fieldExpr.rule.transformCompareRule(fieldExpr)
	}
	
	def dispatch String transformCompareRule(QCompareRule compare, QFieldExpr field)
	{
		'no transformCompareRule for ' + compare.class.name
	}


	def dispatch String transformCompareRule(QPatternMatch compare, QFieldExpr field)
	{
		if(compare.ref !== null) {
			'''Matches(Details('«field.key»'),StrTerminal(r'«compare.ref.resolvePattern(compare.subs)»'))'''			
		}
		else {
			'''Matches(Details('«field.key»'),StrTerminal(r'«compare.value»'))'''						
		}
	
	}
	
	def String resolvePattern(QPattern pattern, List<QPatternSubsitute> substitutes) {
				
		val resolvedRegex = new StringBuilder;
		
		for(QPatternSemantic s: pattern.getSemantics()) {
			if(s instanceof QPatternRef ) {
				
				val subs = substitutes.findFirst[key.equals(s.id)]
				if(subs !== null) {
					resolvedRegex.append( subs.value )
				}
				else {
					resolvedRegex.append( (s as QPatternRef).ref.resolvePattern(newArrayList))				
				}
				
			}
			else {
				resolvedRegex.append(s.getRegex());
			}
		}	
		
		return resolvedRegex.toString;
	}
		
	def dispatch String transformCompareRule(QStringCompare compare, QFieldExpr field)
	{		
		'''«compare.op.transformCompareOperator»(Details('«field.key»'),StrTerminal('«compare.value»'))'''
	}

	def dispatch String transformCompareRule(QExactSearchList exactSearch, QFieldExpr field)
	{
		'''In(Details('«field.key»'),StrTerminalList(«exactSearch.str.map[e|"'"+e+"'"]»))'''
	}
		
	def transformCompareOperator(StringCompareOperator op)
	{
		switch (op) {
			case StringCompareOperator.EQUALS: 'EqualsTo'
			case StringCompareOperator.CONTAINS: 'Contains'
			default: 'Undefined'
		}
	}
}
