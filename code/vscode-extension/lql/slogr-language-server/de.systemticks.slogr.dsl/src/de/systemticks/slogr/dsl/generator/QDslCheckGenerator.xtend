/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl.generator

import static extension de.systemticks.slogr.dsl.generator.QDslGeneratorUtils.*
import de.systemticks.slogr.dsl.qDsl.QPath
import de.systemticks.slogr.dsl.qDsl.CCheck

class QDslCheckGenerator {
	
	private def toPathAccess(QPath path) {
		val pathAccess = new StringBuilder
		path.pathElement.forEach[ e | pathAccess.append("['").append(e).append("']")]
		pathAccess.toString
	}
	
	private def toField(CCheck check) {
		if(check.path !== null) {
			'''json.loads(evt.data["«check.field»"])«check.path.toPathAccess»'''			
		}
		else {
			check.field
		}
	}

	private def fromIndex(CCheck check) {
		if(check.indexRange !== null) {
			check.indexRange.startIndex
		}
		else {
			check.singleIndex
		}
	}

	private def toIndex(CCheck check) {
		if(check.indexRange !== null) {
			check.indexRange.stopIndex
		}
		else {
			check.singleIndex
		}
	}
	
	def transformCheck(CCheck check)
	{
		'''
		# CAUTION: DO NOT EDIT
		# This is generated code from the QDslToPythonRule Generator
		«importSection»
		
		
		«queryChanged»
		
		
		«onQueryChangedCB»
		
		
		def test_«check.name.toLowerCase»(stub, query_changed):
		
		    stub.changeQuery(api_msgs.QueryRequest(
		        queryRootDirPath=os.path.abspath("../../python-db-gen"),
		        queryName="«check.query.ref.fileNameWithoutExtension».«check.query.ref.name.toFirstUpper»")
		    )
		    
		    query_changed.wait()
		    
		    response = stub.getEventsFromTo(
		        api_msgs.GetEventsFromToRequest(fromIndex=«check.fromIndex», toIndex=«check.toIndex»))
		        
		    for evt in response.events:
		        value = «check.toField»
		        assert «check.check.min» <= int(value) <= «check.check.max»
		       
		'''
	}

	def importSection() 
	{
		'''
		import os
		import threading
		import json
		
		import pytest
		
		import count_down_latch as latch
		import slogr_service_pb2 as api_msgs
		'''
	}
	
	def queryChanged()
	{
		'''
		@pytest.fixture
		def query_changed(stub):
		    register_done = latch.CountDownLatch()
		    query_changed = latch.CountDownLatch()
		    try:
		        threading.Thread(target=_on_query_change_cb, args=(stub, register_done, query_changed)).start()
		    except:
		        assert False
		    register_done.wait()
		    yield query_changed
		    stub.unregisterOnQueryChanged(api_msgs.OnQueryChangedRequest())
		'''
	}

	def onQueryChangedCB()
	{
		'''
		def _on_query_change_cb(stub, register_done, query_changed):
		    iterator = iter(stub.registerOnQueryChanged(api_msgs.OnQueryChangedRequest()))
		    next(iterator)
		    register_done.count_down()
		    next(iterator)
		    query_changed.count_down()
		'''
	}
}