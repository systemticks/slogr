/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl;


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
public class QDslStandaloneSetup extends QDslStandaloneSetupGenerated {

	public static void doSetup() {
		new QDslStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}
