/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl.generator

import de.systemticks.slogr.dsl.qDsl.QModel
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

import static extension de.systemticks.slogr.dsl.generator.QDslGeneratorUtils.*

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class QDslMainGenerator extends AbstractGenerator {

	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {

		if(resource.contents.size > 0) {
			
			// Generate the query code
			val pyFileOut = resource.URI.lastSegment.replace('lql', 'py')
			val queryGenerator = new QDslQueryGenerator		
			val queryCode = queryGenerator.transformModel(resource.contents.get(0) as QModel)
			fsa.generateFile(pyFileOut, queryCode);
			
			// Generate the rule code
			(resource.contents.get(0) as QModel).checks.forEach[
				val ruleGenerator = new QDslCheckGenerator
				val ruleCode = ruleGenerator.transformCheck(it)
				fsa.generateFile(resource.URI.lastSegment.replaceAll('.*lql', pythonFileName), 
					QDslOutputConfiguration.RULES_OUTPUT, ruleCode);		
			]
						
		}
		
	
	}
	
	
}
