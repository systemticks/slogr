package de.systemticks.slogr.dsl.generator;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import de.systemticks.slogr.dsl.qDsl.QModel;
import de.systemticks.slogr.dsl.qDsl.QPattern;
import de.systemticks.slogr.dsl.qDsl.QPatternRef;

public class PyGrokDictGenerator extends AbstractGenerator  {
    
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public String transformPattern(QPattern pattern) {

        return pattern.getSemantics().stream().map( semantic -> {
            if(semantic instanceof QPatternRef) {
                QPatternRef reference = (QPatternRef) semantic;
                return toPyGrokReference(reference);
            }
            return semantic.getRegex();
        }).collect(Collectors.joining(""));

    }

    public Set<QPattern> determineCustomPatterns(QPattern pattern, Set<QPattern> allReferences) {

        pattern.getSemantics().forEach( s -> {
            if(s instanceof QPatternRef) {
                QPattern referencedPattern = ((QPatternRef) s).getRef(); 
                determineCustomPatterns(referencedPattern, allReferences);
                allReferences.add( referencedPattern );
            }            
        });

        return allReferences;
    }

    public String toJson(QPattern importPattern) {

        Set<QPattern> customPatterns = determineCustomPatterns(importPattern, new LinkedHashSet<>());

        JsonObject customInner = new JsonObject();
        customPatterns.forEach( p -> customInner.addProperty(p.getName(), transformPattern(p)));

        JsonObject rootElement = new JsonObject();
        rootElement.add("custom_patterns", customInner);

        rootElement.addProperty("pattern", transformPattern(importPattern));

        return gson.toJson(rootElement);
    }

    private String toPyGrokReference(QPatternRef reference) {
        return "%{"+reference.getRef().getName()+toId(reference.getId())+"}";
    }

    private String toId(String id) {
        return id == null || id.isEmpty() ? "" : ":"+id; 
    }

    @Override
    public void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
        
        if(resource.getContents().isEmpty()) {
            return;
        }
        
        QModel model = (QModel) resource.getContents().get(0);

        if(model.getPatterns().isEmpty()) {
            return;
        }

        URI uri = resource.getURI();

        int segmentsCount = uri.segmentCount();

        if(uri.segment(segmentsCount-2).startsWith("importer") && uri.segment(segmentsCount-1).startsWith("importer")) {
            var pyGrokDictAsJson = resource.getURI().lastSegment().replace("lql", "json");
            fsa.generateFile(pyGrokDictAsJson, QDslOutputConfiguration.PY_GROK_DICT_OUTPUT, 
                toJson( model.getPatterns().get( model.getPatterns().size() - 1))) ;
        }

    }

}

