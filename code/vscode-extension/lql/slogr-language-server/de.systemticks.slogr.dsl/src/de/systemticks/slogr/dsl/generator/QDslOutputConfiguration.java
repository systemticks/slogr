/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl.generator;

import java.util.Set;
import static com.google.common.collect.Sets.newHashSet;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.OutputConfiguration;
import org.eclipse.xtext.generator.OutputConfigurationProvider;

public class QDslOutputConfiguration extends OutputConfigurationProvider {

	public static final String RULES_OUTPUT = "rules-output";
	public static final String PY_GROK_DICT_OUTPUT = "pygrok-output";
	  	
	@Override
	public Set<OutputConfiguration> getOutputConfigurations() {
		
		OutputConfiguration defaultOutput = new OutputConfiguration(IFileSystemAccess.DEFAULT_OUTPUT);
		defaultOutput.setDescription("Output folder for generated Python files");
		defaultOutput.setOutputDirectory("../python-db-gen");
		defaultOutput.setOverrideExistingResources(true);
		defaultOutput.setCreateOutputDirectory(true);
		defaultOutput.setCanClearOutputDirectory(false);
		defaultOutput.setCleanUpDerivedResources(true);
		defaultOutput.setSetDerivedProperty(true);
		defaultOutput.setKeepLocalHistory(false);
		
		OutputConfiguration rulesOutput = new OutputConfiguration(RULES_OUTPUT);
		rulesOutput.setDescription("Output folder for generated rules");
		rulesOutput.setOutputDirectory("./rules_library");
		rulesOutput.setOverrideExistingResources(true);
		rulesOutput.setCreateOutputDirectory(true);
		rulesOutput.setCanClearOutputDirectory(false);
		rulesOutput.setCleanUpDerivedResources(true);
		rulesOutput.setSetDerivedProperty(true);
		rulesOutput.setKeepLocalHistory(false);

		OutputConfiguration pyGrokDictOutput = new OutputConfiguration(PY_GROK_DICT_OUTPUT);
		pyGrokDictOutput.setDescription("Output folder for generated pyGrok Dictionaries");
		pyGrokDictOutput.setOutputDirectory("./importer-gen");
		pyGrokDictOutput.setOverrideExistingResources(true);
		pyGrokDictOutput.setCreateOutputDirectory(true);
		pyGrokDictOutput.setCanClearOutputDirectory(false);
		pyGrokDictOutput.setCleanUpDerivedResources(true);
		pyGrokDictOutput.setSetDerivedProperty(true);
		pyGrokDictOutput.setKeepLocalHistory(false);

		return newHashSet(defaultOutput, rulesOutput, pyGrokDictOutput);
	}

	
}
