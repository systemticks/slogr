/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl.ide.contentassist

import com.google.inject.Inject
import de.systemticks.slogr.api.SlogrServiceProxy
import de.systemticks.slogr.dsl.qDsl.QFieldExpr
import de.systemticks.slogr.dsl.qDsl.QPatternMatch
import de.systemticks.slogr.dsl.qDsl.QPatternRef
import de.systemticks.slogr.dsl.services.QDslGrammarAccess
import java.io.File
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ide.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ide.editor.contentassist.IIdeContentProposalAcceptor
import org.eclipse.xtext.ide.editor.contentassist.IdeContentProposalProvider
import org.eclipse.xtext.resource.FileExtensionProvider
import static extension de.systemticks.slogr.dsl.generator.QDslGeneratorUtils.*
import de.systemticks.slogr.dsl.qDsl.QExactSearchList

class QueryDslProposalProvider extends IdeContentProposalProvider {

	@Inject extension QDslGrammarAccess

	@Inject QueryDslTemplateProposalProvider templateProvider;

	@Inject FileExtensionProvider extensionProvider;

	override dispatch void createProposals(Assignment assignment, ContentAssistContext context,
		IIdeContentProposalAcceptor acceptor) {
						
		switch (assignment) {

			case includeAccess.importURIAssignment_1: {
				val cwd = new File(context.currentModel.eResource.URI.toFileString).absoluteFile.parentFile		
				cwd.listFiles.filter[name.endsWith(extensionProvider.primaryFileExtension)].filter[!name.contentEquals(context.currentModel.eResource.URI.lastSegment)].forEach[
					addProposal('"'+name+'"', context, acceptor)					
				]							
			}
		
			case CCheckAccess.fieldAssignment_8,
			case QFieldExprAccess.nameAssignment_0: {
				for (element : SlogrServiceProxy.getDistinctKeys) {
					addProposal(element, context, acceptor)
				}
			}
			
			case QPatternSubsituteAccess.keyAssignment_0: {
				val patternMatch = (context.currentModel.eContainer as QFieldExpr).rule as QPatternMatch
				patternMatch.ref.semantics.forEach[ s |
					if(s instanceof QPatternRef) {
						addProposal((s as QPatternRef).id, context, acceptor)
					}
				]
			}
						
			case QStringCompareAccess.valueAssignment_1 : {
				
				val field = (context.currentModel.eContainer as QFieldExpr)
															
				for (element : SlogrServiceProxy.getDistinctValuesForKey(field.key, context.prefix.replace('\"',''))) {
					addProposal('"'+element+'"', context, acceptor)
				}					
			} 

			case QExactSearchListAccess.strAssignment_3_1: {
				
				val field = (context.currentModel.eContainer as QFieldExpr)
				
				val inElems = (field.rule as QExactSearchList).str
				
				for (element : SlogrServiceProxy.getDistinctValuesForKey(field.key, context.prefix.replace('\"',''))) {
					if(!inElems.contains(element)) {
						addProposal('"'+element+'"', context, acceptor)
					}
				}					
				
			}			
			
			case QExactSearchListAccess.strAssignment_2 : {

				val field = (context.currentModel as QFieldExpr)
				
				for (element : SlogrServiceProxy.getDistinctValuesForKey(field.key, context.prefix.replace('\"',''))) {
					addProposal('"'+element+'"', context, acceptor)
				}					
				
			}
			
			default: {
				super._createProposals(assignment, context, acceptor);
			}
		}
	}

	private def addProposal(String element, ContentAssistContext context, IIdeContentProposalAcceptor acceptor) {
		val entry = proposalCreator.createProposal(element, context)
		val prio = proposalPriorities.getDefaultPriority(entry)
		acceptor.accept(entry, prio)		
	}

	override dispatch createProposals(RuleCall ruleCall, ContentAssistContext context,
		IIdeContentProposalAcceptor acceptor) {

		switch ruleCall.rule {
									
			case QQueryRule: {
				templateProvider.createFilterProposal(context, acceptor)
			}
			
			case QQueryGroupRule: {
				templateProvider.createFilterGroupProposal(context, acceptor)				
			}
			
			default:
				super._createProposals(ruleCall, context, acceptor)
		}
	}

}
