/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl.ide.contentassist

import org.eclipse.xtend2.lib.StringConcatenationClient
import org.eclipse.xtext.ide.editor.contentassist.AbstractIdeTemplateProposalProvider
import org.eclipse.xtext.ide.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ide.editor.contentassist.IIdeContentProposalAcceptor

class QueryDslTemplateProposalProvider extends AbstractIdeTemplateProposalProvider {
	
	
	def void createFilterProposal(ContentAssistContext context, IIdeContentProposalAcceptor acceptor) {
		val StringConcatenationClient template = '''query «variable('newQuery')» : «cursor»'''
		acceptProposal('query', 'Create a new query', template, context, acceptor)
	}
	
	def void createFilterGroupProposal(ContentAssistContext context, IIdeContentProposalAcceptor acceptor) {
		val StringConcatenationClient template = 
		'''
		group «variable('newGroup')» «cursor» (
		   query «variable('newQuery')» :
		)
		'''
		acceptProposal('group', 'Create a new group of queries', template, context, acceptor)
	}	
		
}