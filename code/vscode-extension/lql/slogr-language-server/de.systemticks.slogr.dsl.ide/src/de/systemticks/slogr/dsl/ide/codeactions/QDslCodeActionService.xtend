/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.dsl.ide.codeactions

import org.eclipse.emf.common.util.URI
import org.eclipse.lsp4j.CodeAction
import org.eclipse.lsp4j.CodeActionKind
import org.eclipse.lsp4j.TextEdit
import org.eclipse.lsp4j.WorkspaceEdit
import org.eclipse.lsp4j.jsonrpc.messages.Either
import org.eclipse.xtext.ide.server.codeActions.ICodeActionService2
import org.eclipse.xtext.diagnostics.Diagnostic

class QDslCodeActionService implements ICodeActionService2 {
	
	protected def addTextEdit(WorkspaceEdit edit, URI uri, TextEdit... textEdit) {
		edit.changes.put(uri.toString, textEdit)
	}	
	
	override getCodeActions(Options options) {
		
		val document = options.document
		val params = options.codeActionParams
		val resource = options.resource
		val result = <CodeAction>newArrayList
		
		for (d : params.context.diagnostics) {
			
				println("dc: "+d.code)
			
				if (d.code == Diagnostic.SYNTAX_DIAGNOSTIC && d.message.contains("RULE_STRING")) {
					val text = document.getSubstring(d.range)
					result += new CodeAction => [
						kind = CodeActionKind.QuickFix
						title = "Put the string into quotes"
						diagnostics = #[d]
						// for more complex example we would use 
						// change serializer as in RenameService
						edit = new WorkspaceEdit() => [
							addTextEdit(resource.URI, new TextEdit => [
								range = d.range
								newText = '"'+text+'"'
							])
						]
	
					]
	
				}
			}
		
		return result.map[Either.forRight(it)]
				
	}
	
}