/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
package de.systemticks.slogr.api;

import java.util.ArrayList;
import java.util.List;

import de.systemticks.slogr.api.SlogrServiceOuterClass.GetDistinctFieldsRequest;
import de.systemticks.slogr.api.SlogrServiceOuterClass.GetDistinctFieldsResponse;
import de.systemticks.slogr.api.SlogrServiceOuterClass.GetDistinctValuesForFieldRequest;
import de.systemticks.slogr.api.SlogrServiceOuterClass.GetDistinctValuesForFieldResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

public class SlogrServiceProxy {

	private static SlogrServiceGrpc.SlogrServiceBlockingStub mStub;
	private static List<String> distinctKeys;

	private static void establishConnection() {
		String target = "localhost:50051";

		ManagedChannel channel = ManagedChannelBuilder.forTarget(target).usePlaintext().build();

		mStub = SlogrServiceGrpc.newBlockingStub(channel);
	}

	public static List<String> getDistinctKeys() {

		if (mStub == null) {
			establishConnection();
		}

		if(distinctKeys == null) {
			GetDistinctFieldsRequest request = GetDistinctFieldsRequest.newBuilder().build();
			GetDistinctFieldsResponse response;
			
			try {
				response = mStub.getDistinctFields(request);
				distinctKeys = response.getFieldsList();
			} catch (StatusRuntimeException e) {
				return new ArrayList<String>();
			}			
		}
		
		return distinctKeys;

	}

	public static List<String> getDistinctValuesForKey(String key, String prefix) {

		if (mStub == null) {
			establishConnection();
		}

		GetDistinctValuesForFieldRequest request = GetDistinctValuesForFieldRequest.newBuilder().setField(key).setPrefix(prefix).build();
		GetDistinctValuesForFieldResponse response;

		try {
			response = mStub.getDistinctValuesForField(request);
			return response.getValuesList();
		} catch (StatusRuntimeException e) {
			return new ArrayList<String>();
		}

	}

}
