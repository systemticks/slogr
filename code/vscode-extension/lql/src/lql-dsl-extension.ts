import { ExtensionContext, workspace, window } from "vscode";
import { LanguageClientOptions } from 'vscode-languageclient';
import { LanguageClient, ServerOptions } from 'vscode-languageclient/node';
import * as path from 'path';


export function activate(context: ExtensionContext) {

    //var proc: cp.ChildProcess

    const executable = process.platform === 'win32' ? 'lql-language-server.bat' : 'lql-language-server';
    const languageServerPath =  path.join(
        'server', 'lql-language-server', 'bin', executable);
    const serverLauncher = context.asAbsolutePath(languageServerPath);

    const logger = window.createOutputChannel("Log Query DSL Extension");
    logger.appendLine("Initializing");
 
    const clientOptions: LanguageClientOptions = {
        documentSelector: [{ scheme: 'file', language: 'lql' }],
        outputChannel: logger,
        synchronize: {
            fileEvents: workspace.createFileSystemWatcher('**/*.lql')
        }
    };

    const getServerOptions = function (): ServerOptions {
        return {
            run: {            
                command: serverLauncher
            },
            debug: {
                command: serverLauncher
            }    
        }              
    }

    const languageClient = new LanguageClient('lqlLanguageClient', 'Lql Language Server', getServerOptions(), clientOptions);

    languageClient.start()

    return languageClient
}

export function deactivate() {
    
} 
