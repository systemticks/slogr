/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import * as React from 'react';
import { injectable, inject, postConstruct } from 'inversify';
import { ReactWidget } from '@theia/core/lib/browser/widgets/react-widget';
import { CommandRegistry } from '@theia/core/lib/common';
import { LabelProvider } from '@theia/core/lib/browser';
import { SlogrServiceProxy } from '@slogr/core/lib/browser/slogr-jsonrpc-proxy';
import { SlogrServerWatcher } from '@slogr/core/lib/browser/slogr-server-watcher';

import '../../src/browser/style/index.css';
import URI from '@theia/core/lib/common/uri';
import { ISlogrServerTypes } from '@slogr/core/lib/common/slogr-protocol';

@injectable()
export class ImportedFilesWidget extends ReactWidget {

    static readonly ID = 'imported.file.widget';

    static readonly LABEL = 'Imported Files';

    protected recentFiles: string[] = [];

    protected currentlyLoadedFiles: string[] = [];

    @inject(CommandRegistry)
    protected readonly commandRegistry: CommandRegistry;

    @inject(LabelProvider)
    protected readonly labelProvider: LabelProvider;

    @inject(SlogrServiceProxy) 
    protected slogrProxy: SlogrServiceProxy;

    @inject(SlogrServerWatcher) 
    protected slogrWatcher: SlogrServerWatcher;

    @postConstruct()
    protected async init(): Promise<void> {
        this.id = ImportedFilesWidget.ID;
        this.title.label = ImportedFilesWidget.LABEL;
        this.title.caption = ImportedFilesWidget.LABEL;
        this.title.closable = true;
        this.title.iconClass = 'fa fa-database';

        this.recentFiles = (await this.slogrProxy.recentImportedFiles()).recentFiles

        this.slogrWatcher.onImportFile( async event => {
            if(event.notifcationData === ISlogrServerTypes.NotifcationData.OK && event.numberEvents > 0) {
                this.recentFiles = (await this.slogrProxy.recentImportedFiles()).recentFiles
                this.currentlyLoadedFiles.push(new URI(this.recentFiles[0]).displayName);
                this.update();
            }
        });


        this.slogrWatcher.onDataCleared( async event => {
            this.currentlyLoadedFiles.splice(0, this.currentlyLoadedFiles.length);
            this.update();
        });

        this.update();
    }

    /**
     * Render the content of the widget.
     */
    protected render(): React.ReactNode {
        return <div className='gs-container'>
            <div className='flex-grid'>
                <div className='col'>
                    {this.renderOpen()}
                </div>
            </div>
            <div className='flex-grid'>
                <div className='col'>
                    {this.renderCurrentlyImported()}
                </div>
            </div>
            <div className='flex-grid'>
                <div className='col'>
                    {this.renderRecentFiles()}
                </div>
            </div>            
        </div>;
    }

    /**
     * Render the `open` section.
     * Displays a collection of `open` commands.
     */
    protected renderOpen(): React.ReactNode {
        const open = <div className='gs-action-container'><a href='#' onClick={this.doOpen}>Import Log File(s)</a></div>;
        return <div className='gs-section'>
            <h3 className='gs-section-header'><i className='fa fa-folder-open'></i>Import</h3>
            {open}
        </div>;
    }

    protected renderCurrentlyImported(): React.ReactNode {
        const alreadyOpen = this.currentlyLoadedFiles.map( (item, index) =>
            <div key={index}>{item}</div>
        );
        return <div className='gs-section'>
            <h3 className='gs-section-header'><i className='fa fa-wrench'></i>Currently Imported</h3>
            {this.currentlyLoadedFiles.length > 0 ? alreadyOpen : <p className='gs-no-recent'>No file imported yet</p>}
        </div>;
    }

    protected renderRecentFiles(): React.ReactNode {
        const items = this.recentFiles.map( (path) => { return new URI(path)});
        const recent = items.map( (item, index) =>
            <div key={index}>
                <a href='#' title={item.toString()} onClick={a => this.doOpenRecent(item)}>{item.displayName}</a>
            </div>);
        return  <div className='gs-section'>
            <h3 className='gs-section-header'><i className='fa fa-clock-o'></i>Recent Imports</h3>
            {items.length > 0 ? recent : <p className='gs-no-recent'>No Recent Imports</p>}
            </div>
    }

    protected doOpen = () => this.commandRegistry.executeCommand('ImportLogFile.command');

    protected doOpenRecent = (path: URI) => this.commandRegistry.executeCommand('ImportRecent.command', path);

}
