/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject } from 'inversify';
import { CommandRegistry, MenuModelRegistry } from '@theia/core/lib/common';
import { CommonMenus, AbstractViewContribution, FrontendApplicationContribution, FrontendApplication } from '@theia/core/lib/browser';
import { ImportedFilesWidget } from './imported-files-widget';
import { FrontendApplicationStateService } from '@theia/core/lib/browser/frontend-application-state';
import { WorkspaceService } from '@theia/workspace/lib/browser';

export const ImportedFilesCommand = {
    id: ImportedFilesWidget.ID,
    label: ImportedFilesWidget.LABEL
};

@injectable()
export class ImportedFilesViewContribution extends AbstractViewContribution<ImportedFilesWidget> implements FrontendApplicationContribution {

    @inject(FrontendApplicationStateService)
    protected readonly stateService: FrontendApplicationStateService;

    @inject(WorkspaceService)
    protected readonly workspaceService: WorkspaceService;

    constructor() {
        super({
            widgetId: ImportedFilesWidget.ID,
            widgetName: ImportedFilesWidget.LABEL,
            defaultWidgetOptions: {
                area: 'left',
            }
        });
    }

    async onStart(app: FrontendApplication): Promise<void> {
        this.stateService.reachedState('ready').then(
            () => this.openView({ reveal: true })
        );
    }

    registerCommands(registry: CommandRegistry): void {
        registry.registerCommand(ImportedFilesCommand, {
            execute: () => this.openView({ reveal: true }),
        });
    }

    registerMenus(menus: MenuModelRegistry): void {
        menus.registerMenuAction(CommonMenus.VIEW_VIEWS, {
            commandId: ImportedFilesCommand.id,
            label: ImportedFilesCommand.label,
            order: 'a10'
        });
    }
}
