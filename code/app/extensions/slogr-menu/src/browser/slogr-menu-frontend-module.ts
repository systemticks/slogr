/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { SlogrMenuContribution, SlogrCommandContribution } from './slogr-menu-contribution';

import { CommandContribution, MenuContribution } from "@theia/core/lib/common";

import { ContainerModule } from "inversify";
import { bindViewContribution, FrontendApplicationContribution, WidgetFactory } from '@theia/core/lib/browser';
import { ImportedFilesViewContribution } from './imported-files-contribution';
import { ImportedFilesWidget } from './imported-files-widget';
import { WorkspaceChecker } from './workspace-checker';

export default new ContainerModule(bind => {

    // The clear data menu
    bind(CommandContribution).to(SlogrCommandContribution);
    bind(MenuContribution).to(SlogrMenuContribution);
    
    // The imported view page
    bindViewContribution(bind, ImportedFilesViewContribution);
    bind(FrontendApplicationContribution).toService(ImportedFilesViewContribution);
    bind(ImportedFilesWidget).toSelf();
    bind(WidgetFactory).toDynamicValue(context => ({
        id: ImportedFilesWidget.ID,
        createWidget: () => context.container.get<ImportedFilesWidget>(ImportedFilesWidget),
    })).inSingletonScope();

    bind(FrontendApplicationContribution).to(WorkspaceChecker).inSingletonScope();
    
});
