/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject } from 'inversify';
import { FrontendApplicationContribution, FrontendApplication } from "@theia/core/lib/browser";
import { WorkspaceService } from '@theia/workspace/lib/browser';
import { EnvVariablesServer } from '@theia/core/lib/common/env-variables';
import { FileService } from '@theia/filesystem/lib/browser/file-service';
import URI from '@theia/core/lib/common/uri';
import { ITracer } from '@slogr/core/lib/common/trace-protocol';

const RELATIVE_WORKSPACE_PATH = "/.slogr/workspace"

@injectable()
export class WorkspaceChecker implements FrontendApplicationContribution {

    @inject(EnvVariablesServer)
    protected readonly envService: EnvVariablesServer;

    @inject(FileService)
    protected readonly fileService: FileService;

    @inject(WorkspaceService)
    protected readonly workspaceService: WorkspaceService;

    @inject(ITracer) protected readonly logger: ITracer;

    async onStart(app: FrontendApplication): Promise<void> {

        //TODO: Is this still needed ?
        if(!this.workspaceService.opened) {
            this.logger.warn("No open workspace on start-up");
            const home = await this.envService.getHomeDirUri();
            this.logger.warn("Open default workspace under home directory: "+home+RELATIVE_WORKSPACE_PATH);
            const ws = await this.fileService.createFolder(new URI(home+RELATIVE_WORKSPACE_PATH));

            this.workspaceService.open(ws?.resource, {preserveWindow: true});            
        }
    }

}