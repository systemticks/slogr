/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject } from "inversify";
import { CommandContribution, CommandRegistry, MenuContribution, MenuModelRegistry } from "@theia/core/lib/common";
import { CommonMenus } from "@theia/core/lib/browser";
import { SlogrServiceProxy } from '@slogr/core/lib/browser/slogr-jsonrpc-proxy'


export const ClearAllDataCommand = {
    id: 'ClearAllData.command',
    label: "Clears all data"
};

@injectable()
export class SlogrCommandContribution implements CommandContribution {

    @inject(SlogrServiceProxy) private slogrProxy: SlogrServiceProxy;

    registerCommands(registry: CommandRegistry): void {
        registry.registerCommand(ClearAllDataCommand, {
            execute: () => this.slogrProxy.clearData( 
            ).then( ).catch( (error: Error) => console.log(error.message))
        });
    }
}

@injectable()
export class SlogrMenuContribution implements MenuContribution {

    registerMenus(menus: MenuModelRegistry): void {
        menus.registerMenuAction(CommonMenus.EDIT_FIND, {
            commandId: ClearAllDataCommand.id,
            label: 'clear All Data',
        });
    }
}
