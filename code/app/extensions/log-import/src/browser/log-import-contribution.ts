/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject, postConstruct } from "inversify";
import { CommandContribution, CommandRegistry, MenuContribution, MenuModelRegistry } from "@theia/core/lib/common";
import { CommonMenus, KeybindingContribution, KeybindingRegistry, ConfirmDialog } from "@theia/core/lib/browser";

import { ISlogrServerTypes } from '@slogr/core/lib/common/slogr-protocol'
import { SlogrServiceProxy } from '@slogr/core/lib/browser/slogr-jsonrpc-proxy'
import { SlogrServerWatcher } from '@slogr/core/lib/browser/slogr-server-watcher'

import { FileDialogService } from '@theia/filesystem/lib/browser/file-dialog/file-dialog-service'

import { Keybinding } from "@theia/core/lib/common/keybinding";

import '../../src/browser/style/progress-bar.css';
import URI from "@theia/core/lib/common/uri";
import { ITracer } from "@slogr/core/lib/common/trace-protocol";

export const ImportLogFileCommand = {    
    id: 'ImportLogFile.command',
    label: "Import Log File(s)"
};

export const ImportRecentCommand = {    
    id: 'ImportRecent.command',
};

const ALREADY_LOADED = "File already loaded: "
const ERROR_ON_LOADING = "Error loading file: "
const LOADING_ONGOING = "Now loading: "
const LOADING_SUCCEEDED = "Loading finished"

@injectable()
export class ImportLogContribution implements CommandContribution, KeybindingContribution, MenuContribution {

    @inject(FileDialogService) private fileDialogService: FileDialogService;
    @inject(SlogrServiceProxy) private slogrProxy: SlogrServiceProxy;
    @inject(ITracer) protected readonly logger: ITracer;
    @inject(SlogrServerWatcher) private slogrWatcher: SlogrServerWatcher;

    private currentlyLoadedFiles: Set<string>;

    @postConstruct()
    init(): void {
        this.currentlyLoadedFiles = new Set();

        this.slogrWatcher.onDataCleared( event => {
            this.logger.info("Cleanup currently loaded files");
            this.currentlyLoadedFiles.clear();
        });

    }

    registerMenus(menus: MenuModelRegistry): void {
        menus.registerMenuAction(CommonMenus.FILE_OPEN, {
            commandId: ImportLogFileCommand.id,
            label: 'Import Log File(s) ...'
        });
    }

    registerKeybindings(keybindings: KeybindingRegistry): void {
        const binding: Keybinding = {  command: ImportLogFileCommand.id, keybinding: "alt+i"};
        keybindings.registerKeybinding(binding);
    }

    registerCommands(registry: CommandRegistry): void {

        registry.registerCommand(ImportRecentCommand, {
            execute: (path: URI) => {
                this.importFiles( [path] as Array<URI>);
            }
        });

        registry.registerCommand(ImportLogFileCommand, {
            execute: () => this.fileDialogService.showOpenDialog( { 
                    openLabel: "Import", 
                    canSelectMany: true,                     
                    title: 'Import Log File(s)' 
                } 
            ).then( result => { 
                if(Array.isArray(result)) {
                    this.importFiles( result as unknown as Array<URI>);
                }
                else {
                    if(result) {
                        this.importFiles( [result] as unknown as Array<URI>);
                    }
                }
            }).catch( (error: Error) => console.log(error.message))
        });
    }

    
    async importFiles(uris: URI[]) {
    
        const confirm = this.createLoadingDialog();

        confirm.open();

        for(const [index, uri] of uris.entries()) {
            const filePath = uri.path.fsPath();
            // don't load, if already loaded
            if(this.currentlyLoadedFiles.has(filePath)) {
                this.logger.warn(ALREADY_LOADED+filePath)
                this.stopLoading(ALREADY_LOADED+filePath, confirm);
                return;
            }
            else {
                this.updateLoadingText(confirm, LOADING_ONGOING+filePath);
                confirm.update();
                console.log("Loading File "+index)

                const result: ISlogrServerTypes.GrokImporterFileResult = await this.slogrProxy.grokImporterFiles();

                const response: any = await this.slogrProxy.importFile(filePath, result.grokImporterFiles);
                
                if(response.resultCode !== ISlogrServerTypes.ImportFileResultCode.OK) {
                    this.logger.error(ERROR_ON_LOADING+response.resultCode)
                    this.slogrProxy.clearData(  );
                    this.stopLoading(ERROR_ON_LOADING+filePath, confirm);
                    return;        
                }
                else {
                    this.currentlyLoadedFiles.add(filePath);
                }    
            }
        }

        this.stopLoading(LOADING_SUCCEEDED, confirm);

    }

    private stopLoading(message: string, confirm: ConfirmDialog) {
        this.removeLoadingAnimation(confirm);
        this.updateLoadingText(confirm, message);
        confirm.update();
    }

    private updateLoadingText(dialog: ConfirmDialog, text: string) {
        (dialog.node.querySelector('#load-text') as HTMLDivElement).innerText = text
    }

    private removeLoadingAnimation(dialog: ConfirmDialog) {
        dialog.node.querySelector('#load-animation')?.remove();
    }

    private createLoadingDialog(): ConfirmDialog {

        const message = document.createElement('div');
        message.id = "load-container"

        const loadText = document.createElement('div');
        loadText.id = "load-text"
        loadText.innerText = "Loading...";
        
        const loadAnimation = document.createElement('div');
        loadAnimation.id = "load-animation";
        loadAnimation.innerHTML = 
            `
            <div class="progress">
                <div class="progress-bar-custom" style="--bg: #337ab7"></div>
            </div>    
            `

        message.appendChild(loadText);
        message.appendChild(loadAnimation);

        return new ConfirmDialog( {
            title: "Importing Log File(s)",
            msg: message,
        });

    }
}

