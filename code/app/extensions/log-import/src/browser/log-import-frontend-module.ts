/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { ImportLogContribution } from './log-import-contribution';

import { CommandContribution, MenuContribution } from "@theia/core/lib/common";

import { ContainerModule } from "inversify";
import { KeybindingContribution } from '@theia/core/lib/browser/keybinding';
import { OpenLogHandler } from './open-log-handler';
import { OpenHandler } from '@theia/core/lib/browser';

export default new ContainerModule(bind => {

    bind(OpenLogHandler).toSelf().inSingletonScope();
    bind(OpenHandler).toService(OpenLogHandler);

    bind(ImportLogContribution).toSelf().inSingletonScope();
    
    bind(CommandContribution).toService(ImportLogContribution);
    bind(MenuContribution).toService(ImportLogContribution);
    bind(KeybindingContribution).toService(ImportLogContribution);
});
