/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject } from 'inversify';
import { OpenHandler } from "@theia/core/lib/browser";
import URI from "@theia/core/lib/common/uri";
import { CommandService } from "@theia/core";

@injectable()
export class OpenLogHandler implements OpenHandler {

    id: "open.log";
    label = "Open Log File";
    iconClass?: string | undefined;

    @inject(CommandService)
    protected readonly commands: CommandService;

    canHandle(uri: URI): number {
        // TODO
        // Implemention a reasonable handler. At least check if file is a text file
        return 0;
    }

    async open(uri: URI): Promise<boolean> {

        await this.commands.executeCommand('ImportRecent.command', uri);

        return true;

    }
    
}