/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject } from 'inversify';
import { AbstractViewContribution } from '@theia/core/lib/browser/shell/view-contribution';
import { Widget, FrontendApplicationContribution, FrontendApplication, CommonMenus } from '@theia/core/lib/browser';
import { EventViewWidget } from './event-view-widget';
import { FrontendApplicationStateService } from '@theia/core/lib/browser/frontend-application-state';
import { MenuModelRegistry, CommandRegistry } from '@theia/core';

export const EventViewCommand = {
    id: EventViewWidget.ID,
    label: EventViewWidget.LABEL
};

@injectable()
export class EventViewContribution extends AbstractViewContribution<EventViewWidget> implements FrontendApplicationContribution {

    @inject(FrontendApplicationStateService)
    protected readonly stateService: FrontendApplicationStateService;

    constructor() {
        super({
            widgetId: EventViewCommand.id,
            widgetName: EventViewCommand.label,
            defaultWidgetOptions: {
                area: 'main'
            }
        });
    }

    async onStart(app: FrontendApplication): Promise<void> {
        this.stateService.reachedState('ready').then(
            () =>  { 
                this.openView({ reveal: true }) }
        );
    }

    registerCommands(registry: CommandRegistry): void {
        registry.registerCommand(EventViewCommand, {
            execute: () => this.openView({ reveal: true }),
        });
    }

    registerMenus(menus: MenuModelRegistry): void {
        menus.registerMenuAction(CommonMenus.VIEW_VIEWS, {
            commandId: EventViewCommand.id,
            label: EventViewCommand.label,
            order: 'a10'
        });
    }

    protected async clear(widget: EventViewWidget): Promise<void> {
        widget.clear();
    }

    protected withWidget<T>(widget: Widget | undefined = this.tryGetWidget(), cb: (problems: EventViewWidget) => T): T | false {
        if (widget instanceof EventViewWidget && widget.id === EventViewWidget.ID) {
            return cb(widget);
        }
        return false;
    }

}
