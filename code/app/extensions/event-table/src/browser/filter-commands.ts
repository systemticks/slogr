/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
export const ApplyFilterCommand = {    
    id: 'ApplyFilter.command',
    label: 'Apply Filter in Event Table'
};

export const ResetFilterCommand = {    
    id: 'ResetFilter.command',
    label: 'Reset Filter in Event Table'
};

// relative from workspace folder
export const PYTHON_GEN_FOLDER = "/../python-db-gen"

export const FILTER_EDITOR_QUERY = "predefined.NoNameQuery"