/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable } from 'inversify';
import { AbstractLoggingTable } from './logging-table';
import { DataModel, DataGrid, BasicKeyHandler, BasicMouseHandler, BasicSelectionModel } from '@phosphor/datagrid'
import { Widget, BoxPanel } from '@phosphor/widgets';
import { ISlogrServerTypes } from '@slogr/core/lib/common/slogr-protocol';
import { SlogrServiceProxy } from '@slogr/core/lib/browser/slogr-jsonrpc-proxy';
import throttle = require('lodash.throttle');

import * as moment from 'moment';

import '../../src/browser/style/datagrid.css';
import '@phosphor/default-theme/style/scrollbar.css';
import { inject } from 'inversify';

import { ITracer } from '@slogr/core/lib/common/trace-protocol';
import { SelectedFieldWidget } from '@slogr/core/lib/browser/select-field-widget'
import { EmptyPageWidget } from './empty-page';

@injectable()
export class DatagridTable extends AbstractLoggingTable  {

    @inject(ITracer) protected readonly logger: ITracer;

    dataModel: SloggerDataModel;
    dataGrid: DataGrid;
    containerWidget: BoxPanel;
    emptyPage: EmptyPageWidget;

    private showEmptyPage(): void {
        this.emptyPage = new EmptyPageWidget();
        this.containerWidget.addWidget(this.emptyPage);
        this.emptyPage.update();
        this.containerWidget.update();
    }

    clearTable(): void {
        this.dataGrid.dispose();
        this.showEmptyPage();
    }

    createTable(distinctFields: string[], numberEvents: any): void {
        
        this.dataModel = new SloggerDataModel(this.slogrProxy, this.logger);
        this.dataModel.setup(distinctFields, numberEvents);
  
        this.dataGrid = new DataGrid( { stretchLastColumn: true });

        this.dataGrid.dataModel = this.dataModel;
        this.dataGrid.keyHandler = new BasicKeyHandler();
        this.dataGrid.mouseHandler = new EventTableMouseHandler();
        this.dataGrid.selectionModel = new BasicSelectionModel({ dataModel: this.dataModel });

        this.emptyPage.dispose();

        this.containerWidget.addClass('content-wrapper');
        this.containerWidget.addWidget(this.dataGrid);
        this.containerWidget.update();

        this.containerWidget.title.label = "Events ("+numberEvents+")";
    }

    reRenderTable(numberEvents: number): void {
        this.dataModel.reinit(numberEvents);
        this.containerWidget.title.label = "Events ("+numberEvents+")";
//        this.dataGrid.update();
    }

    addTableToContainer(containerWidget: Widget): void {
        this.containerWidget = containerWidget as BoxPanel
        this.showEmptyPage();
    }

}

class EventTableMouseHandler extends BasicMouseHandler {

    onMouseUp(grid: DataGrid, event: MouseEvent): void {
        const row = grid.selectionModel?.cursorRow
        const col = grid.selectionModel?.cursorColumn

        if(row!==undefined && col!==undefined && grid.dataModel) {
            const selectedValue = grid.dataModel.data( 'body', row, col);
            const field = grid.dataModel.data( 'column-header', row, col);

            if(selectedValue) {
                new SelectedFieldWidget( {
                    title: field,
                    msg: selectedValue
                }).open();    
            }
        }
    }

}

const CHUNK_SIZE: number = 500;

class DataCache {
    
    events: any;
    firstIndex: number;
    lastIndex: number;    

    constructor() {
        this.reset();
    }

    reset() {
        this.firstIndex = -1;
        this.lastIndex = -1;
        this.events = [];
    }
}

const KEY_IDX = 0;
const VALUE_IDX = 1;

class SloggerDataModel extends DataModel {

    fields: string[];
    totalRows: number;
    cachedData: DataCache;

    readonly proxy: SlogrServiceProxy;
    readonly tracer: ITracer;
    readonly container: Widget;
    readonly mandatoryFields: string[];
    loadingTriggered: boolean;

    constructor(_proxy: SlogrServiceProxy, _tracer: ITracer ) {
        super();
        this.proxy = _proxy;
        this.tracer = _tracer;
        this.cachedData = new DataCache();
        this.loadingTriggered = false;
        this.mandatoryFields = ['timestamp']
    }

    throttleCacheLoading = throttle(async currentIndex => {
        await this.cacheData(currentIndex);
    }, 300);

    async cacheData(currentIndex: number) {

        this.tracer.info("cacheData "+currentIndex);

        const cacheArea = this.calculateCachingArea(currentIndex);

        this.loadingTriggered = true;

        try {
            // Slogger Table API starts with index = 1. Therefore limits needs to be increased by 1 (datagrid starts with index = 0)
            this.cachedData.events = await this.proxy.
                getEventsFromTo( cacheArea.lowerLimit+1, cacheArea.upperLimit+1 ).
                then( (result: ISlogrServerTypes.GetEventsFromToResult) => { return result.events });
            this.cachedData.firstIndex = cacheArea.lowerLimit;
            this.cachedData.lastIndex = cacheArea.upperLimit;
        }

        catch(err) {
            this.tracer.error(err);
        }

        this.loadingTriggered = false;
        
    }

    setup(_fields: string[], size: number): void {
        this.totalRows = size;
        this.fields = _fields;
    }

    reinit(size: number): void {
        this.totalRows = size;
        this.resetCache();
        this.triggerDataModelChange();
    }

    private resetCache(): void {
        this.tracer.info("resetCache");
        this.cachedData.reset();
    }

    private triggerDataModelChange() : void {
        this.emitChanged( { type: "model-reset"}); 
    }

    hits(row: number): boolean {
        return (row >= this.cachedData.firstIndex && row <= this.cachedData.lastIndex);
    }

    rowCount(region: DataModel.RowRegion): number {
        region === 'body' ? this.totalRows : 1
        return region === 'body' ? this.totalRows : 1;
    }
    
    columnCount(region: DataModel.ColumnRegion): number {
        return region === 'body' ? this.mandatoryFields.length + this.fields.length : 0;
    }

    data(region: DataModel.CellRegion, row: number, column: number): any {
        if (region === 'row-header') {
            return 'row-header';
        }
        else if(region === 'column-header') {
            if(column == 0) {
                return this.mandatoryFields[column];
            }
            else {
                return this.fields[column-this.mandatoryFields.length]
            }
        }
        else if(region === 'corner-header') {
            return 'corner-header'
        }
        else if(region === 'body') {
 
            if(this.hits(row)) {
                const event = this.cachedData.events[row-this.cachedData.firstIndex];
                if(column == 0) {
                    return moment(new Date(event.timestamp/1000)).format('YYYY-MM-DD hh:mm:ss.SSS');
                }
                else {
                    const fieldName = this.fields[column - this.mandatoryFields.length]
                    const fieldIndex = event.data.find( (element: any) => element[KEY_IDX] === fieldName )
                    return fieldIndex[VALUE_IDX];
                }
            }
            else {
               this.throttleCacheLoading(row)?.then( () => this.triggerDataModelChange() );
//               if(!this.loadingTriggered) {
//                    this.throttleCacheLoading(row).then( () => this.triggerDataModelChange() );
//                    this.cacheData(row).then( () => this.triggerDataModelChange() );    
//                }
                return ''
            }

        }
    }
    
    /**
     * Calculated based on datagrid table index, which starts with 0
     * @param row 
     */
    calculateCachingArea(row: number) {
        return { lowerLimit: Math.max( 0, row - CHUNK_SIZE / 2), upperLimit: Math.min(this.totalRows, row + CHUNK_SIZE/2) - 1 }
    }

}

