/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { inject, injectable, postConstruct } from 'inversify';
import { Machine, interpret } from 'xstate';
import { ITracer } from '@slogr/core/lib/common/trace-protocol';
import { ISlogrServerTypes} from '@slogr/core/lib/common/slogr-protocol'
import { SlogrServiceProxy } from '@slogr/core/lib/browser/slogr-jsonrpc-proxy'
import { SlogrServerWatcher } from '@slogr/core/lib/browser/slogr-server-watcher'
import { Widget } from '@phosphor/widgets';

import { CommandService } from '@theia/core';
import { ResetFilterCommand} from './filter-commands'


const ON_REGISTRATION_REQUIRED: string = "ON_REGISTRATION_REQUIRED";
const ON_QUERY_CHANGED: string = "ON_QUERY_CHANGED";
const ON_NEW_DATA: string = "ON_NEW_DATA";
const ON_DATA_CLEARED: string = "ON_DATA_CLEARED";
const ON_APPLY_FILTER: string = "ON_APPLY_FILTER";

export const TABLE_ID: string = "event-table"

@injectable()
export abstract class AbstractLoggingTable {

    @inject(ITracer) protected readonly logger: ITracer;
    @inject(CommandService) protected readonly commandService: CommandService;

    @inject(SlogrServiceProxy) protected slogrProxy: SlogrServiceProxy;
    @inject(SlogrServerWatcher) private slogrWatcher: SlogrServerWatcher;

    @postConstruct()

    init(): void {
        this.slogrWatcher.onImportFile( event => {
            this.logger.info("LoggingTable handle onImportFile "+JSON.stringify(event));
            if(event.numberEvents > 0) {
                this.tableFSMService.send(ON_NEW_DATA, { data: event });
            }
            else {
                this.tableFSMService.send(ON_DATA_CLEARED);
            }
        });

        this.slogrWatcher.onQueryChanged( event => {
            this.logger.info("LoggingTable handle onQueryChanged "+JSON.stringify(event));
            if(event.notifcationData === ISlogrServerTypes.NotifcationData.OK){
                this.tableFSMService.send(ON_QUERY_CHANGED, { data: event } );
            }
        });

        this.slogrWatcher.onAvailableQueries( event => {
            this.logger.info("LoggingTable handle onAvailableQueries "+JSON.stringify(event));
        });

        this.slogrWatcher.onDataCleared( event => {
            this.logger.info("LoggingTable handle onDataCleared "+JSON.stringify(event));
            this.tableFSMService.send(ON_DATA_CLEARED);
        })
    }

    abstract clearTable(): void

    abstract createTable(distinctFields: any, numberEvents: any): void
    
    abstract reRenderTable(size: number): void
    
    abstract addTableToContainer(containerWidget: Widget): void

    readonly tableRendererFSM = Machine(
        {
            id: 'table',
            initial: 'empty',
            states: {
                empty: {          
                    on: {
                        ON_NEW_DATA: {
                            target: 'dataLoaded',
                            actions: ['actionCreateTable', 'actionApplyFilter']
                        },
                        ON_QUERY_CHANGED: 'empty',
                        ON_APPLY_FILTER: 'empty'
                    }          
                },
                dataLoaded: {
                    on: {
                        ON_APPLY_FILTER: {
                            target: 'dataFiltered',
                        },
                        ON_QUERY_CHANGED: 'dataLoaded'
                    }          
                },
                dataFiltered: {
                    on: {
                        ON_DATA_CLEARED: {
                            target: 'empty',
                            actions: ['actionClearTable']
                        },
                        ON_NEW_DATA: {
                            target: 'dataLoaded',
                            actions: ['actionApplyFilter']
                        },
                        ON_QUERY_CHANGED: { 
                            target: 'dataFiltered',
                            actions: ['actionRenderOnQueryChanged']
                        },
                    }          
                }
            },
        },
        {
            actions: {

                actionRegister: (context, event) => {
                    this.logger.info('actionRegister...' + JSON.stringify(context)+", "+JSON.stringify(event));
                    setTimeout( () => this.register(event.subscriber), 1000 );
                },

                actionRenderOnQueryChanged: (context, event) => {
                    this.logger.info('actionRenderOnQueryChanged...' + JSON.stringify(context)+", "+JSON.stringify(event));
                    this.reRenderTable(event.data.resultSize);
                },

                actionCreateTable: (context, event) => {
                    this.logger.info('actionCreateTable...' + JSON.stringify(context)+", "+JSON.stringify(event));
                    this.createTable(event.data.distinctFields, event.data.numberEvents);
                },

                actionClearTable: (context, event) => {
                    this.logger.info('actionClearTable...' + JSON.stringify(context)+", "+JSON.stringify(event));
                    this.clearTable();
                },

                actionApplyFilter: (context, event) => {
                    this.logger.info('actionApplyFilter...' + JSON.stringify(context)+", "+JSON.stringify(event));
                    this.resetFilter();
                },

            },
        }
    
    );

    readonly tableFSMService = interpret(this.tableRendererFSM).onTransition(state =>
        this.logger.logState( { state: state.value, event: state.event })
    );

    register(subscriber: string): void {
        this.logger.warn("logging-tablr.register(): Deprecated call. To be removed");
    }

    renderTable(): void {
        this.logger.logState("renderTable");

        this.tableFSMService.start();

        this.tableFSMService.send(ON_REGISTRATION_REQUIRED, { subscriber: "registerOnQueryChanged" } );
        this.tableFSMService.send(ON_REGISTRATION_REQUIRED, { subscriber: "registerOnImportFile" } );
    }

    async closeTable(): Promise<any> {

        this.logger.logState("closeTable");
        this.logger.error("TODO: Unregister to be implemented")
    }

    resetFilter(): void {

        this.logger.logState("resetFilter");

        this.commandService.executeCommand( ResetFilterCommand.id).then( 
            () => this.tableFSMService.send(ON_APPLY_FILTER)
        )  ;

    }

};