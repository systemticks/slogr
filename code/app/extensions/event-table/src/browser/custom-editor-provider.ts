/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable } from 'inversify';
import URI from '@theia/core/lib/common/uri';
import { MonacoEditorProvider } from "@theia/monaco/lib/browser/monaco-editor-provider";
import { MonacoEditor } from '@theia/monaco/lib/browser/monaco-editor';

@injectable()
export class CustomMonacoEditorProvider extends MonacoEditorProvider {

    async getEmbeddedEditor(uri: URI, node: HTMLElement, options?: MonacoEditor.IOptions): Promise<MonacoEditor> {
        return this.doCreateEditor(uri, async (override, toDispose) => {
            // override.contextMenuService = {
            //     showContextMenu: () => {/* no-op*/ }
            // };
            const model = await this.getModel(uri, toDispose);
            const monacoEditorOptions = Object.assign({
                model: model.textEditorModel,
                isSimpleWidget: true,
                autoSizing: false,
                minHeight: 3,
                maxHeight: 3                
            }, MonacoEditorProvider.inlineOptions, options);
            return new MonacoEditor(
                uri,                
                model,                
                node,
                this.services,
                monacoEditorOptions,
                override
            );
        });
    }
}