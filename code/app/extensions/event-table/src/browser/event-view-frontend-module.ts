/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { ContainerModule, interfaces } from 'inversify';
import { EventViewWidget } from './event-view-widget';
import { WidgetFactory, bindViewContribution, KeybindingContribution, FrontendApplicationContribution } from '@theia/core/lib/browser';
import { EventViewContribution } from './event-view-contribution';

import { AbstractLoggingTable } from './logging-table';
import { DatagridTable } from './datagrid-table'
import { MonacoEditorProvider } from '@theia/monaco/lib/browser/monaco-editor-provider';
import { CustomMonacoEditorProvider } from './custom-editor-provider';
import { ApplyFilterContribution } from './apply-filter-contribution';
import { CommandContribution } from '@theia/core';

export default new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind, isBound: interfaces.IsBound, rebind: interfaces.Rebind) => {

    bindViewContribution(bind, EventViewContribution);
    bind(FrontendApplicationContribution).toService(EventViewContribution);
    bind(EventViewWidget).toSelf();
    bind(WidgetFactory).toDynamicValue(context => ({
        id: EventViewWidget.ID,
        createWidget: () => context.container.get<EventViewWidget>(EventViewWidget)
    })).inSingletonScope();
    

    bind( AbstractLoggingTable ).to(DatagridTable).inSingletonScope;

    bind(CustomMonacoEditorProvider).toSelf().inSingletonScope();
    rebind(MonacoEditorProvider).toService(CustomMonacoEditorProvider);

    bind(ApplyFilterContribution).toSelf().inSingletonScope();
    
    bind(CommandContribution).toService(ApplyFilterContribution);
    bind(KeybindingContribution).toService(ApplyFilterContribution);


});
