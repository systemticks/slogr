/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, postConstruct, inject } from 'inversify';
import { Message, Widget, BaseWidget } from '@theia/core/lib/browser';
import { AbstractLoggingTable } from './logging-table';
import { Layout, SplitPanel, BoxPanel, BoxLayout } from '@phosphor/widgets';

import { WorkspaceService } from '@theia/workspace/lib/browser';

import { FileService } from '@theia/filesystem/lib/browser/file-service';

import { MonacoEditor } from '@theia/monaco/lib/browser/monaco-editor'
import { CustomMonacoEditorProvider } from './custom-editor-provider';

import '../../src/browser/style/editor-widget.css';

import URI from '@theia/core/lib/common/uri';
import { CommandService } from '@theia/core';

import { ApplyFilterCommand, ResetFilterCommand, FILTER_EDITOR_QUERY} from './filter-commands'

const APPLY_FILTER_BTN = 'apply-filter-btn'
const RESET_FILTER_BTN = 'reset-filter-btn'
const CLEAR_EDITOR_BTN = 'clear-editor-btn'

const BUTTON_CLASS = 'slogr-button'

@injectable()
export class EventViewWidget extends BaseWidget {

    @inject(CustomMonacoEditorProvider)
    protected readonly monacoEditorProvider: CustomMonacoEditorProvider;

    @inject(AbstractLoggingTable) private table: AbstractLoggingTable;

    @inject(WorkspaceService)
    protected readonly workspaceService: WorkspaceService;

    @inject(FileService)
    protected readonly fileService: FileService;

    @inject(CommandService) protected readonly commandService: CommandService;
    
    readonly filterEditorWidget: Widget;
    readonly eventTableWidget: Widget;

    filterEditor: MonacoEditor;

    static readonly ID = 'event.view.widget';
    static readonly LABEL = 'Event View';

    constructor() {
        super();
        this.id = EventViewWidget.ID;
        this.title.label = EventViewWidget.LABEL;
        this.title.caption = EventViewWidget.LABEL;
        this.title.iconClass = 'fa fa-list';
        this.title.closable = false;
        this.addClass('theia-output');
        this.node.tabIndex = 0;

        this.filterEditorWidget = this.createEditorWidget();
        this.eventTableWidget = this.createEventTableWidget();
    }

    @postConstruct()
    protected async init(): Promise<void> {

        this.update();

        this.layout = this.createLayout();

        const buttonBarContainer = document.createElement('div');
        buttonBarContainer.id = 'editor-buttons';
        buttonBarContainer.innerHTML = 
            `
            <button title="Apply Filter" id="${APPLY_FILTER_BTN}" class="${BUTTON_CLASS}"><i class="fa fa-check"></i></button>
            <button title="Reset Filter" id="${RESET_FILTER_BTN}" class="${BUTTON_CLASS}"><i class="fa fa-close"></i></button>
            <button title="Clear Editor" id="${CLEAR_EDITOR_BTN}" class="${BUTTON_CLASS}"><i class="fa fa-trash"></i></button>
            `
        this.filterEditorWidget.node.appendChild(buttonBarContainer);

        const editorContainer = document.createElement('div');
        editorContainer.id = 'editor-container';
        this.filterEditorWidget.node.appendChild(editorContainer);

        const workspace = this.workspaceService.tryGetRoots()[0];
        if (workspace) {
            let uri: URI | undefined;
            const file = workspace.children?.find(c => c.resource.toString().endsWith("/predefined.lql"));
            if (file) {
                uri = new URI(file.resource.toString());
            }
            if (!uri) {
                uri = new URI(`${workspace.resource.toString()}/predefined.lql`);
                await this.fileService.createFile(uri)
            }

            this.filterEditor = await this.monacoEditorProvider.getEmbeddedEditor(uri, editorContainer);
            this.toDispose.push(this.filterEditor);
            this.filterEditor.node.focus();
            this.filterEditor.resizeToFit();

            this.update();
        }

        this.table.addTableToContainer(this.eventTableWidget);

        //this.toDispose.push(this.eventTableWidget);
    }

    protected createLayout(): Layout | null {
        
        const layout = new BoxLayout( );
        layout.addWidget(this.createSplitPanel());
        return layout;
        
    }

    public createSplitPanel(): SplitPanel {
        const panel = new SplitPanel( { orientation: 'vertical' });

        panel.addWidget(this.filterEditorWidget);
        panel.addWidget(this.eventTableWidget);
        
        panel.setRelativeSizes( [10, 90] );

        return panel;
    }

    protected createEditorWidget(): Widget {

        const editorWidget = new Widget( );

        editorWidget.title.label = "Filter Editor"
        editorWidget.title.caption = "Filter Editor"
        editorWidget.title.iconClass = "fa fa-filter"

        editorWidget.addClass('filterEditorWidget');

        return editorWidget;        
    }

    protected createEventTableWidget(): Widget {

        const tableWidget = new BoxPanel();

        tableWidget.title.label = "Events (0)"
        tableWidget.title.caption = "Events"        
        tableWidget.title.iconClass = "fa fa-table"
        tableWidget.id = "TableWidgetContainer"

        return tableWidget;
    }


    protected onAfterAttach(msg: Message): void {
        super.onAfterAttach(msg);

        const applyFilterBtn = document.querySelector(`#${APPLY_FILTER_BTN}`) as HTMLInputElement
        applyFilterBtn?.addEventListener('click', (event) => {
            this.commandService.executeCommand( ApplyFilterCommand.id, FILTER_EDITOR_QUERY) ;
        });

        const resetFilterBtn = document.querySelector(`#${RESET_FILTER_BTN}`) as HTMLInputElement
        resetFilterBtn?.addEventListener('click', (event) => {
            this.commandService.executeCommand( ResetFilterCommand.id) ;
        });

        const clearEditorBtn = document.querySelector(`#${CLEAR_EDITOR_BTN}`) as HTMLInputElement
        clearEditorBtn?.addEventListener('click', (event) => {
            this.filterEditor.document.textEditorModel.setValue("");
        });

        this.table.renderTable();

        this.update();
    }

    protected onActivateRequest(msg: Message): void {
        super.onActivateRequest(msg);
    }

    protected registerListener(outputChannel: any): void {

    }

    protected onCloseRequest(msg: Message): void {
//        super.onCloseRequest(msg);
//        this.table.closeTable();
    }

    public clear(): void {

    }


    protected resizeEditor() {
        if (this.filterEditor) {
            this.filterEditor.setSize({ height: 100, width: this.filterEditorWidget.node.offsetWidth });
        }
    }

    onUpdateRequest(args: any) {
        super.onUpdateRequest(args);
        this.resizeEditor();
    }

    onResize(msg: Widget.ResizeMessage) {
        super.onResize(msg);
        this.resizeEditor();
    }

}
