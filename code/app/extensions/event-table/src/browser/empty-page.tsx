/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import * as React from 'react';
import { ReactWidget } from '@theia/core/lib/browser';

export class EmptyPageWidget extends ReactWidget {
    
    protected render(): React.ReactNode {
        return <div id="empty-page">
            <h2>No data loaded. Please import a file.</h2>
            <hr></hr>
            <h3>While you are here ...</h3>
            <div id="column_block">
                <div id="col1">
                    <h4>Intro</h4>
                    <p><b>slogr</b> is at home in large scale automotive projects.</p>
                    <p>Lots of issue tickets have piled up and want to be burned down.</p>

                    <p><b>slogr</b> provides a way to analyze DLT logs with an easy to use query language.
                    IDE features like syntax completion and validation help you write even complex queries with ease. A backing database returns results with minimal delay.
                    Existing queries can be persisted, shared and reused on any DLT log file.</p>
                </div>
                <div id="col2">
                    <h4>Disclaimer</h4>
                    <p><b>slogr</b> is currently in early development.
                    Our current status is pre-alpha.
                    See our <a href="https://gitlab.com/systemticks/slogger/-/blob/master/doc/roadmap.md" rel="nofollow noreferrer noopener" target="_blank">roadmap</a> for more.</p>
                    <table><tbody>                        
                        <tr>
                            <td>✔</td>
                            <td>Pre-alpha</td>
                            <td>fit for early demos conducted by experienced developers</td>
                        </tr>
                        <tr>
                            <td>-</td>
                            <td>Alpha</td>
                            <td>an installable packet is available that can easily be executed and played with</td>
                        </tr>
                        <tr>
                            <td>-</td>
                            <td>Beta</td>
                            <td>feature complete but not stable</td>
                        </tr>
                        <tr>
                            <td>-</td>
                            <td>v1.0</td>
                            <td>first stable release</td>
                        </tr>
                    </tbody></table>
                </div>
                <div id="col3">
                    <h4>How to support <b>slogr</b></h4>
                    <p><b>slogr</b> is initiated and currently developed and maintained
                        by <a href="https://systemticks.de" rel="nofollow noreferrer noopener" target="_blank">systemticks</a>.</p>
                    <p>If you are thinking, how can I ...</p>
                    <ul>
                        <li>... speed up development of <b>slogr</b>?</li>
                        <li>... request feature X?</li>
                        <li>... demonstrate my appreciation?</li>
                        <li>... provide feedback and critique?</li>
                    </ul>
                    <p>You can currently do one of the following:</p>
                    <ul>
                        <li>create a <a href="https://gitlab.com/systemticks/slogger/-/issues" rel="nofollow noreferrer noopener" target="_blank">ticket</a></li>
                        <li>tweet about <em>#slogr</em> and <a href="https://twitter.com/systemticks/" rel="nofollow noreferrer noopener"
                            target="_blank">@systemticks</a></li>
                        <li>drop us an <a href="hello@systemticks.de">e-mail</a></li>
                    </ul>
                    <p>We are happy to answer all inquiries about</p>
                    <ul>
                        <li>financial support</li>
                        <li>coding contributions</li>
                        <li>bespoke feature requests</li>
                        <li>proprietary extensions</li>
                        <li>trainings and support</li>
                    </ul>
                    <p>Please <a href="hello@systemticks.de" rel="nofollow noreferrer noopener" target="_blank">get in touch</a>!</p>
                </div>
            </div>
        </div>
    }

}
