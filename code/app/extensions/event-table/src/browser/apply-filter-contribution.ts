/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject } from "inversify";

import { WorkspaceService } from '@theia/workspace/lib/browser';
import { ISlogrServer } from '@slogr/core/lib/common/slogr-protocol'

import { KeybindingContribution, KeybindingRegistry } from "@theia/core/lib/browser";
import { CommandContribution, CommandRegistry } from "@theia/core";

import { ApplyFilterCommand, ResetFilterCommand, PYTHON_GEN_FOLDER, FILTER_EDITOR_QUERY} from './filter-commands'

@injectable()
export class ApplyFilterContribution implements CommandContribution, KeybindingContribution {

    @inject(ISlogrServer) private slogrProxy: ISlogrServer;

    @inject(WorkspaceService) protected readonly workspaceService: WorkspaceService;

    registerCommands(registry: CommandRegistry): void {

        registry.registerCommand(ApplyFilterCommand, {
            execute: async ( query ) => {

                const workspace = this.workspaceService.tryGetRoots()[0];

                await this.slogrProxy.changeQuery(workspace.resource.toString().slice(7) +PYTHON_GEN_FOLDER, query);
            }
        });

        registry.registerCommand(ResetFilterCommand, {
            execute: async () => {

                const workspace = this.workspaceService.tryGetRoots()[0];

                await this.slogrProxy.changeQuery(workspace.resource.toString().slice(7)+PYTHON_GEN_FOLDER, "");
            }
        });

    }

    registerKeybindings(keybindings: KeybindingRegistry): void {
        keybindings.registerKeybinding( {  command: ApplyFilterCommand.id,
                                           keybinding: "ctrl+enter",
                                           when: "editorLangId == lql",
                                           args: FILTER_EDITOR_QUERY });
        keybindings.registerKeybinding( {  command: ResetFilterCommand.id, keybinding: "alt+u", when: "editorLangId == lql"} );
    }

}
