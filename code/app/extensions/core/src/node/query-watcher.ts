/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, postConstruct, inject } from 'inversify'
import { FSWatcher} from "chokidar"
import * as lr from 'line-reader';
import { ISlogrClient, ISlogrServerTypes } from '../common/slogr-protocol';
import { ITracer } from '../common/trace-protocol';
import debounce = require('lodash.debounce');

const GENERATED_PY_GEN_PATH = "/home/ubuntu/tools/slogger/python-db-gen";
const QUERY_PATTERN = /# Query: (\w+)\.(\w+)\|(\w+(?:\.\w+)?)$/

const DEBOUNCE_ON_CHANGE_MILLISECONDS: number = 200;

@injectable()
export class QueryWatcher {

    protected watcher: FSWatcher;
    protected queryMap: Map<string, string[]>
    protected client: ISlogrClient;

    @inject(ITracer) protected readonly logger: ITracer;

    @postConstruct()
    init(): void {

        this.watcher = new FSWatcher( { depth: 0, ignored: GENERATED_PY_GEN_PATH+'/predefined.py' });
        this.queryMap = new Map();

        this.watcher.add(GENERATED_PY_GEN_PATH);

        this.watcher.on('add', async path => {
            this.logger.info("QueryWatcher: File "+path+ " added");
            const queries = await this.extractQueries(path)
            this.queryMap.set(path, queries);
            this.fireAvailableQueries();
        });

        this.watcher.on('change', debounce(
                async path => {
                    this.logger.info("QueryWatcher: File "+path+ " changed");
                    const queries = await this.extractQueries(path)
                    this.queryMap.set(path, queries);
                this.fireAvailableQueries();
            }, DEBOUNCE_ON_CHANGE_MILLISECONDS)
        );

/*
        this.watcher.on('change',
            async path => {
                this.logger.info("QueryWatcher: File "+path+ " changed");
                const queries = await this.extractQueries(path)
                this.queryMap.set(path, queries);
            this.fireAvailableQueries();
        });
*/
        this.watcher.on('unlink', path => {
            this.logger.info("QueryWatcher: File "+path+ " removed");
            this.queryMap.delete(path);
            this.fireAvailableQueries();
        });

    }

    public setClient(client: ISlogrClient): void {
        this.client = client;
    }

    private fireAvailableQueries() {
        if(this.client) {
            const result = new ISlogrServerTypes.OnAvailableQueries();
            result.queries = new Array();
            this.queryMap.forEach( (value, key ) => {
                value.forEach ( element => {
                    result.queries.push( {src: key, name: element});
                })
            });
            this.client.onAvailableQueries(result);
        }
        else {
            this.logger.warn("QueryWatcher: Cannot fire available queries. Client not set");
        }
    }

    private extractQueries(path: string): Promise<string[]> {

        return new Promise((resolve, reject) => {
            const queries: string[] = []

            lr.eachLine(path, (line) => {
                if(!line.startsWith('#')) {
                    resolve(queries);
                    return false;
                }
                const match = line.match(QUERY_PATTERN);
                if(match) {
                    const name = match.slice(-1)[0]
                    queries.push(name);
                }
            });
        });
    }

}