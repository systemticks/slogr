/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject } from 'inversify';
import { BackendApplicationContribution } from '@theia/core/lib/node';
import { ProcessErrorEvent } from '@theia/process/lib/node/process';
import { ProcessManager } from '@theia/process/lib/node/process-manager';
import { RawProcess, RawProcessFactory } from '@theia/process/lib/node/raw-process';
import * as cp from 'child_process';
import * as express from 'express';
import { TracerNode } from './slogr-log4js';
import * as path from 'path';

@injectable()
export class SlogrServiceStarter implements BackendApplicationContribution {

  @inject(ProcessManager) protected readonly processManager: ProcessManager;
  @inject(RawProcessFactory) protected readonly processFactory: RawProcessFactory;
  @inject(TracerNode) protected readonly logger: TracerNode;

  rawProcess: RawProcess;
  readonly launch: boolean = true;

  initialize() {

    const spath = path.join( __dirname, "..", "..", "..", "..", "..", "backend", "slogr-service")

    this.logger.info("Current directory "+ spath);

    if (this.launch) {
      this.logger.logState("SlogrServiceStarter.initialize");

      const bazel = "slogr_service.exe"

      setTimeout( () => {
        this.logger.logState(spath+" "+bazel);
        this.spawnProcessAsync(bazel, [],
          { cwd: spath, shell: "cmd" });          
      }, 5000)

    }
  }

  onStop(app?: express.Application) {
    if (this.launch) {
      this.logger.logState("SlogrServiceStarter.stop");
      this.rawProcess.inputStream.write('q\n');
      this.rawProcess.kill();
    }
  }

  protected spawnProcessAsync(command: string, args?: string[], options?: cp.SpawnOptions): Promise<RawProcess> {
    this.rawProcess = this.processFactory({ command, args, options });
    this.rawProcess.errorStream.on('data', this.logInfo.bind(this));
    this.rawProcess.outputStream.on('data', this.logInfo.bind(this));

    return new Promise<RawProcess>((resolve, reject) => {
      this.rawProcess.onError((error: ProcessErrorEvent) => {
        this.onDidFailSpawnProcess(error);
        if (error.code === 'ENOENT') {
          const guess = command.split(/\s+/).shift();
          if (guess) {
            reject(new Error(`Failed to spawn ${guess}\nPerhaps it is not on the PATH.`));
            return;
          }
        }
        reject(error);
      });
      process.nextTick(() => resolve(this.rawProcess));
    });
  }

  protected onDidFailSpawnProcess(error: Error | ProcessErrorEvent): void {
    this.logger.error(error.message);
  }

  protected logInfo(data: string | Buffer) {
    if (data) {
      this.logger.info(`SlogrServiceStarter: ${data}`);
    }
  }

}