/*******************************************************************************
 * Copyright (c) 2022 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject, postConstruct } from "inversify"
import { ISlogrClient, ISlogrServer, ISlogrServerTypes, CONNECTION_TIMEOUT } from "../common/slogr-protocol";

import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';
import { QueryWatcher } from "./query-watcher";
import { TracerNode } from './slogr-log4js';
import { IpcMessageType } from '../common/trace-protocol';
import { GrokImporterProvider } from "./grok-importer-provider";
import { RecentFileHandler } from "./recent-file-handler";

var PROTO_PATH = __dirname + '/../../api/slogr_service.proto';

@injectable()
export class SlogrRpcBridgeDynamic implements ISlogrServer {

    protected client: ISlogrClient | undefined;
    
    @inject(QueryWatcher)
    readonly queryWatcher: QueryWatcher;

    readonly grpcProxy: any;

    @inject(TracerNode) protected readonly logger: TracerNode;

    @inject(GrokImporterProvider)
    readonly grokProvider: GrokImporterProvider;
    
    @inject(RecentFileHandler)
    readonly recentFileHandler: RecentFileHandler;

    constructor () {

        var packageDefinition = protoLoader.loadSync(
            PROTO_PATH,
            {keepCase: true,
             longs: String,
             enums: String,
             defaults: true,
             oneofs: true
            });
        
        var slogr_service: any = grpc.loadPackageDefinition(packageDefinition).slogrservice;
        this.grpcProxy = new slogr_service.SlogrService('localhost:50051', grpc.credentials.createInsecure());    
    }

    @postConstruct()
    init(): void {

        const deadline = new Date();
        deadline.setSeconds(deadline.getSeconds() + CONNECTION_TIMEOUT);

        this.logger.logState("initialize gRPC connection");

        this.grpcProxy.waitForReady(deadline, (error?: Error) => 
            { 
                const connectionStatus = new ISlogrServerTypes.OnConnectionStatus(false);
                if(error) {
                    this.logger.logState("gRPC connection cannot be establisehd");
                    connectionStatus.connected = false;
                }
                else {
                    this.logger.logState("gRPC connection establisehd");
                    connectionStatus.connected = true;
                    this.registerOnImportFile();
                    this.registerOnQueryChanged();            
                }
                if(this.client) {
                    this.client.onConnectionStatus(connectionStatus);
                }
                else {
                    this.logger.error("Cannot send gRPC status to Theia");
                }
            });

    }

    importFile(path: string, grokPatterns: string[]): Promise<ISlogrServerTypes.ImportFileResult> {

        let self = this;

        return new Promise(function (resolve, reject) {

            const request = {
                path: path,
                grokImporterPaths: grokPatterns
            };

            self.logger.logIpcCall(0, IpcMessageType.Request, "slogr-service.importFile", request);

            self.grpcProxy.importFile(request, (err: any, response: any) => {
                if(err) {
                    self.logger.logIpcCall(0, IpcMessageType.Error, "slogr-service.importFile", err.message);
                    reject(new Error(err.message));
                }
                const result = new ISlogrServerTypes.ImportFileResult();
                result.resultCode = (<any>ISlogrServerTypes.ImportFileResultCode)[response.resultCode];
                self.logger.logIpcCall(0, IpcMessageType.Response, "slogr-service.importFile", result);
                if(result.resultCode === ISlogrServerTypes.ImportFileResultCode.OK) {
                    self.recentFileHandler.addFile(path);
                }

                resolve(result);
            });

        })
    }

    changeQuery(path: string, query: string): Promise<ISlogrServerTypes.ChangeQueryResult> {

        let self = this;

        return new Promise(function(resolve, reject) {

            const request = {
                queryName: query,
                queryRootDirPath: path
            }
            self.logger.logIpcCall(0, IpcMessageType.Request, "slogr-service.changeQuery", request);

            self.grpcProxy.changeQuery(request, (err: any, response: any) => {
                if(err) {
                    self.logger.logIpcCall(0, IpcMessageType.Error, "slogr-service.changeQuery", err.message);
                    reject(new Error(err.message));
                }
                const result = new ISlogrServerTypes.ChangeQueryResult();
                resolve(result);
            });

        });


    }

    getEventsFromTo(fromIndex: number, toIndex: number): Promise<ISlogrServerTypes.GetEventsFromToResult> {
        let self = this;

        return new Promise(function(resolve, reject) {

            const request = {
                fromIndex: fromIndex,
                toIndex: toIndex                
            };

            self.logger.logIpcCall(0, IpcMessageType.Request, "slogr-service.getEventsFromTo", request);

            self.grpcProxy.getEventsFromTo(request, (err: any, response: any) => {
                if(err) {
                    self.logger.logIpcCall(0, IpcMessageType.Error, "slogr-service.getEventsFromTo", err.message);
                    reject(new Error(err.message));
                }

                const result = new ISlogrServerTypes.GetEventsFromToResult();
                self.logger.logIpcCall(0, IpcMessageType.Response, "slogr-service.getEventsFromTo", {size: response.events.length});
                const events = response.events.map( (e: { rowid: number; timestamp: number; data: any })  => { 
                    const event = new ISlogrServerTypes.Events();
                    event.rowid = e.rowid;
                    event.timestamp = e.timestamp;                    
                    event.data = Object.entries(e.data)
                    return event;
                });
                result.events = events
                resolve(result);

            });

        });
    }

    clearData(): Promise<ISlogrServerTypes.ClearDataResult> {

        let self = this;

        return new Promise((resolve, reject) => {

            const request = {};
            self.logger.logIpcCall(0, IpcMessageType.Request, "slogr-service.clearData", request);

            self.grpcProxy.clearData(request, (err: any, response: any) => {
                if(err) {
                    reject(new Error(err.message));
                }
                self.logger.logIpcCall(0, IpcMessageType.Response, "slogr-service.clearData", response);
                const result = new ISlogrServerTypes.ClearDataResult();
                self.client?.onDataCleared(new ISlogrServerTypes.OnDataClearedNotification());
                resolve(result);
            });

        });        

    }

    recentImportedFiles(): Promise<ISlogrServerTypes.RecentImportedFilesResult> {
        return new Promise( (resolve, reject) => {            
            const result = new ISlogrServerTypes.RecentImportedFilesResult();
            result.recentFiles = this.recentFileHandler.recentFiles
            resolve(result);
        });
    }

    grokImporterFiles(): Promise<ISlogrServerTypes.GrokImporterFileResult> {
        return new Promise( (resolve, reject) => {
             const result = new ISlogrServerTypes.GrokImporterFileResult();
             this.grokProvider.getGrokImporterDefinitions().then( items => {
                 result.grokImporterFiles = items
                 resolve(result);  
             })
        }); 
     }
 
    registerOnQueryChanged(): void {

        this.logger.logIpcCall(0, IpcMessageType.Request, "slogr-service.registerOnQueryChanged", {} );

        const call = this.grpcProxy.registerOnQueryChanged({});

        let self = this;

        call.on('data', function(notification: any) {
            const result = new ISlogrServerTypes.OnQueryChangedNotification();
            result.resultSize = notification.resultSize;
            result.notifcationData = ISlogrServerTypes.NotifcationData.OK;
            self.logger.logIpcCall(0, IpcMessageType.Notification, "slogr-service.registerOnQueryChanged", result);
            self.client?.onQueryChanged(result);
        });

        call.on('error', function(error: grpc.ServiceError) {
            const result = new ISlogrServerTypes.OnQueryChangedNotification();
            result.notifcationData = ISlogrServerTypes.NotifcationData.ERROR;
            self.logger.logIpcCall(0, IpcMessageType.Notification, "slogr-service.registerOnQueryChanged", result);
            self.client?.onQueryChanged(result);
        });

        call.on('end', function() {
            const result = new ISlogrServerTypes.OnQueryChangedNotification();
            result.notifcationData = ISlogrServerTypes.NotifcationData.STOPPED;
            self.logger.logIpcCall(0, IpcMessageType.Notification, "slogr-service.registerOnQueryChanged", result);
            self.client?.onQueryChanged(result);
        });


    }

    registerOnImportFile(): void {

        this.logger.logIpcCall(0, IpcMessageType.Request, "slogr-service.registerOnImportFile", {} );

        const call = this.grpcProxy.registerOnImportFile({});

        let self = this;

        call.on('data', function(notification: any) {
            const result = new ISlogrServerTypes.OnImportFileNotification();
            result.numberEvents = notification.numberEvents;
            result.distinctFields = notification.distinctFields;
            result.notifcationData = ISlogrServerTypes.NotifcationData.OK;
            self.logger.logIpcCall(0, IpcMessageType.Notification, "slogr-service.registerOnImportFile", result);
            self.client?.onImportFile(result);
        });

        call.on('error', function(error: grpc.ServiceError) {
            const result = new ISlogrServerTypes.OnImportFileNotification();
            result.notifcationData = ISlogrServerTypes.NotifcationData.ERROR;
            self.logger.logIpcCall(0, IpcMessageType.Notification, "slogr-service.registerOnImportFile", result);
            self.client?.onImportFile(result);
        });

        call.on('end', function() {
            const result = new ISlogrServerTypes.OnImportFileNotification();
            result.notifcationData = ISlogrServerTypes.NotifcationData.STOPPED;
            self.logger.logIpcCall(0, IpcMessageType.Notification, "slogr-service.registerOnImportFile", result);
            self.client?.onImportFile(result);
        });

    }

    stop(): void {
        this.logger.logState("gRPC connection closed");
        this.grpcProxy.close();
    }

    setClient(client: ISlogrClient | undefined): void {
        this.client = client;
        this.queryWatcher.setClient(client as ISlogrClient);
    }

    dispose(): void {
        /* no op */
    }

}
