/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { configure, getLogger, Logger, addLayout } from 'log4js';
import * as moment from 'moment';
import { injectable } from 'inversify';
import { ITracer, IpcMessageType } from '../common/trace-protocol';

@injectable()
export class TracerNode implements ITracer {
    
    readonly logger: Logger;

    readonly APPLICATION: string = "slogr-ui";

    constructor() {

        addLayout('json', (config) => {
            return (logEvent) => { 
                return JSON.stringify( {                     
                    ts: moment(logEvent.startTime).format("YYYY-MM-DDTHH:mm:ss.SSS"),
                    app: this.APPLICATION,
                    context: logEvent.context?.type ?? "plain",
                    component: logEvent.categoryName,
                    loglevel: logEvent.level.levelStr,
                    payload: (
                        (ctx) => { if(ctx === null || ctx === undefined) 
                                    return { message: logEvent.data[0] }
                                else
                                    return JSON.parse(logEvent.data[0])
                        })(logEvent.context.type) 
                });
            }
          });

        configure({

            appenders: { 
                file: { type: 'file', 
                        filename: `${this.APPLICATION}_${moment().format('YYYY-MM-DD_HH-mm')}.slog`}, 
                out: { type: 'stdout' }
            }, 
            categories: { default: { appenders: ['file', 'out'], level: 'debug' } }
        });

        this.logger = getLogger();
    }

    logIpcCall(id: number, type: IpcMessageType, name: string, params: any): void {

        this.logger.addContext('type', 'ipc');
        if(type === IpcMessageType.Error)
        {
            this.error( JSON.stringify({ callid: id, msg_type: type, msg_name: name, params: params }));
        }
        else
        {
            this.info( JSON.stringify({ callid: id, msg_type: type, msg_name: name, params: params }));
        }
        this.logger.removeContext('type');

    }

    logState(state: any): void {
        this.logger.addContext('type', 'state');
        this.info(JSON.stringify( { message: state} ));
        this.logger.removeContext('type');
    }

    debug(message: string, ...args: any[]): void {
        this.logger.debug(message, args);
    }

    info(message: string, ...args: any[]): void {
        this.logger.info(message, args);
    }

    error(message: string, ...args: any[]): void {
        this.logger.error(message, args);
    }

    warn(message: string, ...args: any[]): void {
        this.logger.warn(message, args);
    }

}