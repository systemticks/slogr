/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, postConstruct, inject} from 'inversify';
import * as fs from 'fs-extra';
import { SlogrPathProvider } from './slogr-path-provider';
import { TracerNode } from './slogr-log4js';

export const RecentFileStorage = Symbol('RecentFileStorage');

export interface RecentFileStorage {
    read() : Promise<string[]>
    write(recentFiles: string[]): void
    capacity: number;
}


@injectable()
export class RecentFileStorageFile implements RecentFileStorage {
        
    @inject(SlogrPathProvider) 
    protected readonly pathProvider: SlogrPathProvider;

    @inject(TracerNode) protected readonly logger: TracerNode;

    async read() : Promise<string[]> {
        
        return await this.pathProvider.recentLogFilesPath().then( uri => {
            this.logger.info("Read recenfiles from "+uri.path.fsPath())
            if(fs.existsSync(uri.path.fsPath())) {
                return JSON.parse(fs.readJsonSync(uri.path.fsPath()));
            }
            else {
                return [];
            }            
        });        

    }

    write(recentFiles: string[]): void {
        this.pathProvider.recentLogFilesPath().then( uri => { 
            this.logger.info("Write to recenfiles in "+uri.path.fsPath())
            fs.writeJsonSync(uri.path.fsPath(), JSON.stringify(recentFiles)) })
    }

    get capacity() : number {
        return 10;
    }
}

@injectable()
export class RecentFileHandler {

    private _recentFiles: string[];

    @inject(RecentFileStorage) protected readonly storage: RecentFileStorage;

    @postConstruct()
    load(): void {
        this.storage.read().then( files => this._recentFiles = files);
    }

    addFile(path: string) {
        
        if(this._recentFiles.includes(path)) {
            this._recentFiles.forEach( (item, i) => {
                if(item === path) {
                    this._recentFiles.splice(i, 1);
                }
            })
        }
        
        this._recentFiles.unshift(path);

        if(this._recentFiles.length > this.storage.capacity) {
            this._recentFiles.splice(-1, 1);
        }

        this.storage.write(this._recentFiles);
    }

    get recentFiles(): string[] {
        return this._recentFiles;
    }

}