/*******************************************************************************
 * Copyright (c) 2022 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import URI from '@theia/core/lib/common/uri';
import { inject, injectable } from '@theia/core/shared/inversify';
import { WorkspaceServer } from "@theia/workspace/lib/common"
import klawSync = require('klaw-sync');
import { TracerNode } from './slogr-log4js';

const GROK_IMPORTER_GEN_DIR = "importer-gen"

@injectable()
export class GrokImporterProvider {

    @inject(TracerNode) protected readonly logger: TracerNode;

    @inject(WorkspaceServer)
    protected readonly server: WorkspaceServer;

    async getGrokImporterDefinitions(): Promise<string[]> {

        const workspaces = await this.server.getRecentWorkspaces()
        //TODO What if more than one workspaces open ?
        const uri = (new URI(workspaces[0] + "/" + GROK_IMPORTER_GEN_DIR)).path.fsPath()

        this.logger.info("getGrokImporterDefinitions: "+uri)

        //TODO what, if directory is empty ?
        return klawSync(uri, { nodir: true }).map( item => item.path) 

    }
}