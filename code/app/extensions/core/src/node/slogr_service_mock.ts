/*******************************************************************************
 * Copyright (c) 2023 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject, postConstruct } from "inversify"
import { ISlogrClient, ISlogrServer, ISlogrServerTypes } from "../common/slogr-protocol";
import { IpcMessageType } from "../common/trace-protocol";
import { GrokImporterProvider } from "./grok-importer-provider";
import { QueryWatcher } from "./query-watcher";
import { RecentFileHandler } from "./recent-file-handler";
import { TracerNode } from "./slogr-log4js";
import * as grok from "grok-js";
import * as fs from 'fs-extra';
import * as moment from 'moment';

@injectable()
export class SlogrServiceMock implements ISlogrServer {

    protected client: ISlogrClient | undefined;
    
    @inject(QueryWatcher) readonly queryWatcher: QueryWatcher;

    @inject(TracerNode) protected readonly logger: TracerNode;

    @inject(GrokImporterProvider)
    readonly grokProvider: GrokImporterProvider;
    
    @inject(RecentFileHandler)
    readonly recentFileHandler: RecentFileHandler;

    events: Array<ISlogrServerTypes.Events>
    
    @postConstruct()
    init(): void {

        this.logger.logState("initialize SlogrServiceMock connection...");

        this.events = new Array();

        this.registerOnImportFile();
        this.registerOnQueryChanged();

        setTimeout(() => {
            if(this.client) {
                this.client.onConnectionStatus(new ISlogrServerTypes.OnConnectionStatus(true));
            }
        }, 5000);

    }

    importFile(path: string, grokPatterns: string[]): Promise<ISlogrServerTypes.ImportFileResult> {

        let self = this;

        return new Promise(function (resolve, reject) {
            
            self.logger.logIpcCall(0, IpcMessageType.Request, "importFile", {path: path, grokPatterns: grokPatterns })
            const result = new ISlogrServerTypes.ImportFileResult();

            const grokPatternObj = fs.readJSONSync(grokPatterns[0])
    
            const custom = grokPatternObj.custom_patterns
    
            const grokCollection = new grok.GrokCollection()
            Object.keys(custom).forEach( (key) =>  {
                grokCollection.createPattern(custom[key], key)
              })
    
            const finalPattern = grokCollection.createPattern(grokPatternObj.pattern)
    
            const allFileContents = fs.readFileSync(path, 'utf-8');

            let idx = self.events.length + 1
            allFileContents.split(/\r?\n/).forEach(line => {
                const logEvent = finalPattern.parseSync(line)
                if(logEvent) {
                    const ts: number = moment(logEvent.ts, moment.HTML5_FMT.TIME_MS).valueOf() * 1000
                    delete logEvent.ts
                    self.events.push( { rowid: idx, timestamp: ts, data: Object.entries(logEvent)})
                    idx += 1
                }
                else{
                    self.logger.warn("Grok pattern cannot be assigned to %s",line)
                }
            });

            result.resultCode = ISlogrServerTypes.ImportFileResultCode.OK

            const distinctFields = self.events[0].data.map( tuple => tuple[0])

            self.importFileNotification(self.events.length, distinctFields)
            if(result.resultCode === ISlogrServerTypes.ImportFileResultCode.OK) {
                self.recentFileHandler.addFile(path);
            }

            resolve(result)

        })

    }

    changeQuery(path: string, query: string): Promise<ISlogrServerTypes.ChangeQueryResult> {
     
        return new Promise(function(resolve, reject) {
            resolve(new ISlogrServerTypes.ChangeQueryResult());
        })

    }

    getEventsFromTo(fromIndex: number, toIndex: number): Promise<ISlogrServerTypes.GetEventsFromToResult> {

        let self = this

        return new Promise(function (resolve, reject) {        
            const eventsResult = new ISlogrServerTypes.GetEventsFromToResult()
            eventsResult.events = self.events.slice(fromIndex-1, toIndex)
            resolve( eventsResult)
        });
    }

    clearData(): Promise<ISlogrServerTypes.ClearDataResult> {

        return new Promise((resolve, reject) => {
            this.events.length = 0
            const result = new ISlogrServerTypes.ClearDataResult();
            this.client?.onDataCleared(new ISlogrServerTypes.OnDataClearedNotification());
            resolve(result);
        })

    }

    recentImportedFiles(): Promise<ISlogrServerTypes.RecentImportedFilesResult> {
        return new Promise( (resolve, reject) => {            
            const result = new ISlogrServerTypes.RecentImportedFilesResult();
            result.recentFiles = this.recentFileHandler.recentFiles
            resolve(result);
        });
    }

    grokImporterFiles(): Promise<ISlogrServerTypes.GrokImporterFileResult> {
        return new Promise( (resolve, reject) => {
             const result = new ISlogrServerTypes.GrokImporterFileResult();
             this.grokProvider.getGrokImporterDefinitions().then( items => {
                 result.grokImporterFiles = items
                 resolve(result);  
             })
        }); 
     }

    registerOnQueryChanged(): void {
        this.logger.logState("SlogrServiceMock.registerOnQueryChanged");
    }

    registerOnImportFile(): void {
        this.logger.logState("SlogrServiceMock.registerOnImportFile");
    }

    importFileNotification(nmbrEvents: number, distinctFields: string[]): void {

        const result = new ISlogrServerTypes.OnImportFileNotification();
        result.numberEvents = nmbrEvents;
        result.distinctFields = distinctFields;
        result.notifcationData = ISlogrServerTypes.NotifcationData.OK;
        
        this.logger.logIpcCall(0, IpcMessageType.Notification, "SlogrServiceMock.registerOnImportFile", result);
        this.client?.onImportFile(result);
    }

    stop(): void {
        throw new Error("Method not implemented.");
    }

    dispose(): void {
        throw new Error("Method not implemented.");
    }

    setClient(client: ISlogrClient | undefined): void {
        this.logger.logState("SlogrServiceStub setClient");
        this.client = client;
        this.queryWatcher.setClient(client as ISlogrClient);
    }

    getClient?(): ISlogrClient | undefined {
        throw new Error("Method not implemented.");
    }
}
