/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject } from 'inversify';
import { URI } from '@theia/core/lib/common/uri';
import { EnvVariablesServer } from '@theia/core/lib/common/env-variables';


export const SlogrPathProvider = Symbol('SlogrPathProvider');

export interface SlogrPathProvider {

    /**
     * Returns a promise that resolves to an URI, representing the file for persisting the recent loaded log files
     */
    recentLogFilesPath(): Promise<URI>;

    /**
     * Returns a promise that resolves to an URI, representing the default workspace.
     */
    defaultWorkspacePath(): Promise<URI>;
}

const SLOGR_HOME = "/.slogr"
const RECENT_FILES = SLOGR_HOME + "/.recentFiles"
const DEFAULT_WORKSPACE = SLOGR_HOME + "/workspace"

@injectable()
export class DefaultSlogrPathProvider implements SlogrPathProvider {

    @inject(EnvVariablesServer)
    protected readonly envService: EnvVariablesServer;

    async recentLogFilesPath(): Promise<URI> {
        const home = await this.envService.getHomeDirUri()
        return new URI(home + RECENT_FILES);
    }

    async defaultWorkspacePath(): Promise<URI> {
        const home = await this.envService.getHomeDirUri()
        return new URI(home + DEFAULT_WORKSPACE);
    }

}