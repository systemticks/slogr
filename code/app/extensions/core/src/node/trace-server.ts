/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { inject, injectable } from 'inversify';
import { ITracer, IpcMessageType } from "../common/trace-protocol"
import { TracerNode } from './slogr-log4js';

@injectable()
export class TraceServer implements ITracer {

    @inject(TracerNode) protected readonly logger: TracerNode;

    info(message: string, ...args: any[]): void {
        this.logger.info(message, args);
    }
    debug(message: string, ...args: any[]): void {
        this.logger.debug(message, args);
    }
    error(message: string, ...args: any[]): void {
        this.logger.error(message, args);
    }
    warn(message: string, ...args: any[]): void {
        this.logger.warn(message, args);
    }
    logIpcCall(id: number, type: IpcMessageType, name: string, params: any): void {
        this.logger.logIpcCall(id, type, name, params);    
    }
    
    logState(state: any): void {
        this.logger.logState(state);
    }

}