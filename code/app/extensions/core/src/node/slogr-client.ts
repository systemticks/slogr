/*******************************************************************************
 * Copyright (c) 2022 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject, postConstruct } from "inversify"
import { ISlogrClient, ISlogrServerTypes } from "../common/slogr-protocol";
import { TracerNode } from "./slogr-log4js";

@injectable()
export class DispatchingSLogrClient implements ISlogrClient {

    readonly clients = new Set<ISlogrClient>();

    @inject(TracerNode) protected readonly logger: TracerNode;
    
    connectionStatus: ISlogrServerTypes.OnConnectionStatus;    

    @postConstruct()
    init(): void {
        this.connectionStatus = new ISlogrServerTypes.OnConnectionStatus(false);
    }

    push(client: ISlogrClient) {
        this.logger.info("DispatchingSLogrClient.push");
        if(this.connectionStatus && this.connectionStatus.connected === true)
        {
            client.onConnectionStatus(this.connectionStatus);
        }
        this.clients.add(client);
    }

    remove(client: ISlogrClient) {
        this.logger.info("DispatchingSLogrClient.remove");
        this.clients.delete(client);
    }

    onQueryChanged(event: ISlogrServerTypes.OnQueryChangedNotification): void {
        this.logger.info("DispatchingSLogrClient.onQueryChanged");
        this.clients.forEach(client => client.onQueryChanged(event));
    }

    onImportFile(event: ISlogrServerTypes.OnImportFileNotification): void {
        this.logger.info("DispatchingSLogrClient.onImportFile");
        this.clients.forEach(client => client.onImportFile(event));
    }

    onDataCleared(event: ISlogrServerTypes.OnDataClearedNotification): void {
        this.logger.info("DispatchingSLogrClient.onDataCleared");
        this.clients.forEach(client => client.onDataCleared(event));
    }

    onAvailableQueries(event: ISlogrServerTypes.OnAvailableQueries): void {
        this.logger.info("DispatchingSLogrClient.onAvailableQueries");
        this.clients.forEach(client => client.onAvailableQueries(event));
    }

    onConnectionStatus(event: ISlogrServerTypes.OnConnectionStatus): void {
        this.logger.info("DispatchingSLogrClient.OnConnectionStatus "+this.clients.size);
        this.connectionStatus = event;
        this.clients.forEach(client => client.onConnectionStatus(event));
    }
}
