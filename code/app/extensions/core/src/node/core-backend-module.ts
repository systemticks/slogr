import { ContainerModule } from 'inversify';
import { SlogrServiceStarter } from './slogr-service-starter';
import { BackendApplicationContribution } from '@theia/core/lib/node/backend-application';

/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { ITracer, TRACER_SERVICE_WS_PATH } from '../common/trace-protocol';
import { TracerNode } from './slogr-log4js';
import { ISlogrServer, ISlogrClient, SLOGR_SERVICE_WS_PATH } from '../common/slogr-protocol';
import { DispatchingSLogrClient } from './slogr-client';
import { ConnectionHandler, JsonRpcConnectionHandler } from '@theia/core';
import { TraceServer } from './trace-server';
import { RecentFileHandler, RecentFileStorage, RecentFileStorageFile } from './recent-file-handler';
import { QueryWatcher } from './query-watcher';
import { SlogrPathProvider, DefaultSlogrPathProvider } from './slogr-path-provider';
import { GrokImporterProvider } from './grok-importer-provider';
//import { SlogrRpcBridgeDynamic } from './slogr-dynamic-server';
import { SlogrServiceMock } from './slogr_service_mock';

export default new ContainerModule(bind => {

    bind(SlogrPathProvider).to(DefaultSlogrPathProvider).inSingletonScope();

    /********* RECENT FILE HANDLING ***********/
    bind(RecentFileStorage).to(RecentFileStorageFile);
    bind(RecentFileHandler).toSelf();

    /********* DIRECTORY WATCHER OF GENERATED PYTHON QUERIES ***********/
    bind(QueryWatcher).toSelf().inSingletonScope();

    /********* FOR PROVIDING THE GENERATED GROK IMPORTERS ***********/
    bind(GrokImporterProvider).toSelf().inSingletonScope();

    /********* SLOGR-SECTION START ***********/
    bind(DispatchingSLogrClient).toSelf().inSingletonScope();

    bind(ISlogrServer).to(SlogrServiceMock).inSingletonScope().onActivation( (context, server: ISlogrServer) => {
        const client = context.container.get(DispatchingSLogrClient);
        server.setClient(client);
        return server;
    });
    
    bind(ConnectionHandler).toDynamicValue(ctx => 
        new JsonRpcConnectionHandler<ISlogrClient>(SLOGR_SERVICE_WS_PATH, (client: any) => {
            const dispatcher = ctx.container.get(DispatchingSLogrClient);
            dispatcher.push(client);
            client.onDidCloseConnection( () => dispatcher.remove(client));
            return ctx.container.get<ISlogrServer>(ISlogrServer);
        })
    ).inSingletonScope();

    bind(BackendApplicationContribution).to(SlogrServiceStarter);
    /********* SLOGR-SECTION END ***********/

    /********* TRACING-SECTION START ***********/

    // Setup the Trace Server for remote logging (logs geerated from UI)
    bind(TraceServer).toSelf().inSingletonScope();    
    bind(ITracer).toService(TraceServer);

    bind(ConnectionHandler).toDynamicValue(ctx =>
        new JsonRpcConnectionHandler(TRACER_SERVICE_WS_PATH, () =>
            ctx.container.get(ITracer)
        )
    ).inSingletonScope();

    // Setup the Logger (for *.slog files)
    bind(TracerNode).toSelf().inSingletonScope();
    /********* TRACING-SECTION END ***********/

});