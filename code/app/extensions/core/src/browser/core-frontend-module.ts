/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { ContainerModule } from 'inversify';
import { ITracer, TRACER_SERVICE_WS_PATH } from '../common/trace-protocol';
import { ISlogrServer, SLOGR_SERVICE_WS_PATH } from '../common/slogr-protocol';

import { WebSocketConnectionProvider, FrontendApplicationContribution } from '@theia/core/lib/browser'
import { SlogrServiceProxy } from './slogr-jsonrpc-proxy';
import { SlogrServerWatcher } from './slogr-server-watcher';

export default new ContainerModule(bind => {

    bind(SlogrServiceProxy).toSelf().inSingletonScope();

    bind(SlogrServerWatcher).toSelf().inSingletonScope();

    bind(FrontendApplicationContribution).toService(SlogrServiceProxy);

    bind(ISlogrServer).toDynamicValue(ctx => {
        const slogrWatcher = ctx.container.get(SlogrServerWatcher);
        const connection = ctx.container.get(WebSocketConnectionProvider);
        const target = connection.createProxy<ISlogrServer>(SLOGR_SERVICE_WS_PATH, slogrWatcher.getSlogrClient());
        return target;
    }).inSingletonScope();

    bind(ITracer).toDynamicValue( ctx => {
        const provider = ctx.container.get(WebSocketConnectionProvider);
        return provider.createProxy<ITracer>(TRACER_SERVICE_WS_PATH);
    }).inSingletonScope();

});