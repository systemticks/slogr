/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable } from 'inversify';
import { ISlogrServerTypes, ISlogrClient } from "../common/slogr-protocol";
import { Emitter, Event } from '@theia/core';

@injectable()
export class SlogrServerWatcher {
   
    private onImportFileEmitter = new Emitter<ISlogrServerTypes.OnImportFileNotification>();
    private onQueryChangedEmitter = new Emitter<ISlogrServerTypes.OnQueryChangedNotification>();
    private onDataClearedEmitter = new Emitter<ISlogrServerTypes.OnDataClearedNotification>();
    private onAvailableQueriesEmitter = new Emitter<ISlogrServerTypes.OnAvailableQueries>();
    private onConnectionStatusEmitter = new Emitter<ISlogrServerTypes.OnConnectionStatus>();

    getSlogrClient(): ISlogrClient {

        const importFileEmitter = this.onImportFileEmitter;
        const queryChangedEmiiter = this.onQueryChangedEmitter;
        const dataClearedEmitter = this.onDataClearedEmitter;
        const availableQueriesEmitter = this.onAvailableQueriesEmitter;
        const connectionStatusEmitter = this.onConnectionStatusEmitter;

        return {

            onImportFile(event: ISlogrServerTypes.OnImportFileNotification): void {
                importFileEmitter.fire(event);
            },

            onQueryChanged(event: ISlogrServerTypes.OnQueryChangedNotification): void {
                queryChangedEmiiter.fire(event);
            },

            onDataCleared(event: ISlogrServerTypes.OnDataClearedNotification): void {
                dataClearedEmitter.fire(event);
            },
            
            onAvailableQueries(event: ISlogrServerTypes.OnAvailableQueries): void {
                availableQueriesEmitter.fire(event);
            },

            onConnectionStatus(event: ISlogrServerTypes.OnConnectionStatus): void {
                connectionStatusEmitter.fire(event);
            }
        }
    }

    get onImportFile(): Event<ISlogrServerTypes.OnImportFileNotification> {
        return this.onImportFileEmitter.event;
    }

    get onQueryChanged(): Event<ISlogrServerTypes.OnQueryChangedNotification> {
        return this.onQueryChangedEmitter.event;
    }

    get onDataCleared(): Event<ISlogrServerTypes.OnDataClearedNotification> {
        return this.onDataClearedEmitter.event;
    }

    get onAvailableQueries() : Event<ISlogrServerTypes.OnAvailableQueries> {
        return this.onAvailableQueriesEmitter.event;
    }

    get onConnectionStatus(): Event<ISlogrServerTypes.OnConnectionStatus> {
        return this.onConnectionStatusEmitter.event;
    }
}