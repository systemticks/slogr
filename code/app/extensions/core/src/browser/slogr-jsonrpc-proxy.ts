/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable, inject } from 'inversify';
import { ISlogrServerTypes, ISlogrServer } from "../common/slogr-protocol";
import { ITracer } from '../common/trace-protocol';
import { FrontendApplicationContribution, FrontendApplication } from '@theia/core/lib/browser';
import { SlogrServerWatcher } from './slogr-server-watcher';

@injectable()
export class SlogrServiceProxy implements FrontendApplicationContribution{

    @inject(ISlogrServer)
    protected readonly server: ISlogrServer;

    @inject(ITracer) protected readonly logger: ITracer;

    @inject(SlogrServerWatcher) private slogrWatcher: SlogrServerWatcher;

    configure(app: FrontendApplication): Promise<void> {

        this.logger.info("Wait for slogr-service connection");

        return new Promise<void>( (resolve, reject) => { 
            
            this.slogrWatcher.onConnectionStatus( event => {
                if(event.connected) {
                    this.logger.info("Connection to slogr-service succeeded");
                    resolve();
                }
                else {
                    this.logger.info("Connection to slogr-service failed");
                    reject("Connection to slogr-service failed");
                }
            });

        });

    }

    onStop(app: FrontendApplication): void {
        this.logger.info("SlogrServiceProxy.onStop");
        this.server.stop();
    }

    importFile(path: string, grokPatterns: string[]): Promise<ISlogrServerTypes.ImportFileResult> {
        this.logger.info("SlogrServiceProxy.importFile");
        return this.server.importFile(path, grokPatterns);
    }

    changeQuery(path: string, query: string): Promise<ISlogrServerTypes.ChangeQueryResult> {
        this.logger.info("SlogrServiceProxy.changeQuery");
        return this.server.changeQuery(path, query);
    }

    getEventsFromTo(fromIndex: number, toIndex: number): Promise<ISlogrServerTypes.GetEventsFromToResult> {
       this.logger.info("SlogrServiceProxy.getEventsFromTo");
       return this.server.getEventsFromTo(fromIndex, toIndex);
    }

    clearData(): Promise<ISlogrServerTypes.ClearDataResult> {
        this.logger.info("SlogrServiceProxy.clearData");
        return this.server.clearData();
    }

    recentImportedFiles(): Promise<ISlogrServerTypes.RecentImportedFilesResult> {
        this.logger.info("SlogrServiceProxy.recentImportedFiles");
        return this.server.recentImportedFiles();
    }

    grokImporterFiles(): Promise<ISlogrServerTypes.GrokImporterFileResult> {
        this.logger.info("SlogrServiceProxy.grokImporterFiles");
        return this.server.grokImporterFiles();
    }


}

