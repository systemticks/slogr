/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import * as React from 'react';
import { ConfirmDialog, Message } from '@theia/core/lib/browser';
import ReactDOM = require('react-dom');
import ReactJson from 'react-json-view-ts'
import { Disposable } from '@theia/core';

const CONTENT_CONTAINER = "selected-field"

export class SelectedFieldWidget extends ConfirmDialog {
    
    fieldValue: string;
    
    protected createMessageNode(msg: string | HTMLElement): HTMLElement {

        if (typeof msg === 'string') {
            const messageNode = document.createElement('div');
            messageNode.id = CONTENT_CONTAINER
            this.fieldValue = msg
            this.toDispose.push(Disposable.create(() => ReactDOM.unmountComponentAtNode(messageNode)));
            return messageNode;
        }
        return msg;
    }

    protected onUpdateRequest(message: Message): void {

        super.onUpdateRequest(message);

        if(this.fieldValue.length >= 2 && this.fieldValue.startsWith('{') && this.fieldValue.endsWith('}')) {
            try {
                ReactDOM.render(<ReactJson src={JSON.parse(this.fieldValue)}/>, document.getElementById(CONTENT_CONTAINER));            
            }
            catch(error) {
                ReactDOM.render(<p>{this.fieldValue}</p>, document.getElementById(CONTENT_CONTAINER));            
            }     
        }
        else {
            ReactDOM.render(<p>{this.fieldValue}</p>, document.getElementById(CONTENT_CONTAINER));            
        }

    }


}