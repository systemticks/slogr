/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { injectable } from 'inversify';

export const TRACER_SERVICE_WS_PATH = '/services/tracer-service';

export const ITracer = Symbol('ITracer');

export enum IpcMessageType {
    Request = "Request",
    Response = "Response",
    Notification = "Notification",
    Error = "Error"
}

export interface ITracer {

    info(message: string, ...args: any[]): void;

    error(message: string, ...args: any[]): void;

    warn(message: string, ...args: any[]): void;

    debug(message: string, ...args: any[]): void;

    logIpcCall(id: number, type: IpcMessageType, name: string, params: any): void;

    logState(state: any) : void;
}

@injectable()
export class TracerDefault implements ITracer {

    logState(state: any): void {
        console.info("state ", state);
    }

    logIpcCall(id: number, type: IpcMessageType, name: string, params: any): void {
        console.info("ipc ", type, name, params);
    }

    info(message: string, ...args: any[]): void {
        console.info(message, args);
    }

    error(message: string, ...args: any[]): void {
        console.error(message, args);
    }

    warn(message: string, ...args: any[]): void {
        console.warn(message, args);
    }

    debug(message: string, ...args: any[]): void {
        console.debug(message, args);
    }

}

