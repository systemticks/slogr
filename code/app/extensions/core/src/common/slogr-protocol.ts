/*******************************************************************************
 * Copyright (c) 2020 systemticks GmbH
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     systemticks GmbH - initial API and implementation
 *******************************************************************************/
import { JsonRpcServer } from "@theia/core";

export namespace ISlogrServerTypes {
    
    export enum NotifcationData {
        OK = 0,
        ERROR = 1,
        STOPPED = 2
    }

    export class ImportFileResult {
        resultCode: ImportFileResultCode;
    }

    export enum ImportFileResultCode {
        OK = 0,
        UNKNOWN_FAILURE = 1,
        NO_GROK_PATTERNS = 2,
        NO_GROK_PATTERN_MATCHES = 3,
        EMPTY_FILE = 4
    }

    export class ChangeQueryResult {
    }

    export class GetEventsFromToResult {
        events: Events[];
    }

    export class Events {
        rowid: number;
        timestamp: number;
        data: Array<[string, string]>;;
    }

    export class ClearDataResult {
    }

    export class OnQueryChangedResult {
    }

    export class OnQueryChangedNotification {
        resultSize: number;
        notifcationData?: NotifcationData;
    }

    export class OnImportFileResult {
    }

    export class OnImportFileNotification {
        numberEvents: number;
        distinctFields: string[];
        notifcationData?: NotifcationData;
    }

    export class OnDataClearedNotification {
        
    }

    export class OnAvailableQueries {
        queries: Array<{ src: string, name: string}>;
    }

    export class RecentImportedFilesResult {
        recentFiles: string[];
    }

    export class GrokImporterFileResult {
        grokImporterFiles: string[];
    }

    export class OnConnectionStatus {
        constructor(public connected: boolean){}
    }
}

export const SLOGR_SERVICE_WS_PATH = '/services/slogr-service';
export const CONNECTION_TIMEOUT = 60;

export const ISlogrServer = Symbol('ISlogrServer');

export interface ISlogrServer extends JsonRpcServer<ISlogrClient> {
    
    importFile(path: string, grokPatterns: string[]): Promise<ISlogrServerTypes.ImportFileResult>;

    changeQuery(path: string, query: string): Promise<ISlogrServerTypes.ChangeQueryResult>;

    getEventsFromTo(fromIndex: number, toIndex: number): Promise<ISlogrServerTypes.GetEventsFromToResult>;

    clearData(): Promise<ISlogrServerTypes.ClearDataResult>;

    recentImportedFiles(): Promise<ISlogrServerTypes.RecentImportedFilesResult>;

    grokImporterFiles(): Promise<ISlogrServerTypes.GrokImporterFileResult>;

    registerOnQueryChanged(): void;

    registerOnImportFile(): void;

    stop(): void;

}

export const ISlogrClient = Symbol('ISlogrClient');

export interface ISlogrClient {

    onQueryChanged(event: ISlogrServerTypes.OnQueryChangedNotification): void;

    onImportFile(event: ISlogrServerTypes.OnImportFileNotification): void;

    onDataCleared(event: ISlogrServerTypes.OnDataClearedNotification): void;

    onAvailableQueries(event: ISlogrServerTypes.OnAvailableQueries): void;

    onConnectionStatus(event: ISlogrServerTypes.OnConnectionStatus): void;
}
