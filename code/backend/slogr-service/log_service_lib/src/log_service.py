# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0

import queue
import select
import socket
import time
from threading import Thread
from time import gmtime, strftime

MESSAGE_SEPARATOR = '}\n'


class LogServer(Thread):
    def __init__(self, port=50006, max_clients=5):
        Thread.__init__(self)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(('localhost', port))
        self.sock.listen(max_clients)
        self.sock_threads = []
        self.log_queue = queue.Queue(10)
        self.log_file = '/tmp/slogr-' + \
            strftime("%Y-%m-%dT%H:%M:%S", gmtime()) + '.slog'
        self.log_consumer = LogAppender(self.log_queue, self.log_file)
        self.log_consumer.start()

    def get_log_file(self):
        return self.log_file

    def run(self):
        self.__stop = False
        while not self.__stop:
            self.sock.settimeout(1)
            try:
                client_sock, client_addr = self.sock.accept()
            except socket.timeout:
                client_sock = None

            if client_sock:
                client_thr = ClientLogProcessor(
                    client_sock, client_addr, self.log_queue)
                self.sock_threads.append(client_thr)
                client_thr.start()
        self.close()
        self.log_consumer.stop()

    def close(self):
        for thr in self.sock_threads:
            thr.stop()
            thr.join()

        if self.sock:
            self.sock.close()
            self.sock = None

    def stop(self):
        self.__stop = True


class ClientLogProcessor(Thread):
    def __init__(self, socket, address, log_queue):
        Thread.__init__(self)
        self.socket = socket
        self.address = address
        self.log_queue = log_queue

    def run(self):
        self.__stop = False
        buffer = ''
        while not self.__stop:
            if self.socket:
                try:
                    rdy_read, rdy_write, sock_err = select.select(
                        [self.socket, ], [self.socket, ], [], 5)
                except select.error as err:
                    self.stop()
                    return

                if len(rdy_read) > 0:
                    read_data = self.socket.recv(1024)
                    buffer += read_data.decode('utf-8')
                    messages = buffer.split(MESSAGE_SEPARATOR)

                    if not buffer.endswith(MESSAGE_SEPARATOR):
                        if len(messages) > 1:
                            messages = messages[:-1]
                            buffer = messages[-1]
                        else:
                            messages = []
                    else:
                        buffer = ''

                    for message in messages:
                        if len(message) > 0:
                            self.log_queue.put(message + MESSAGE_SEPARATOR)

                    if len(read_data) == 0:
                        self.stop()
                    else:
                        pass
            else:
                self.stop()
        self.close()

    def stop(self):
        self.__stop = True

    def close(self):
        if self.socket:
            self.socket.close()


class LogAppender(Thread):

    def __init__(self, log_queue, log_file):
        Thread.__init__(self)
        self.log_queue = log_queue
        self.log_file = log_file

    def run(self):
        self.__stop = False
        with open(self.log_file, "w+") as central_log:
            while(not self.__stop):
                if self.log_queue.qsize() > 10 or self.log_queue.full():
                    self._consume_message_queue(central_log)
                time.sleep(.1)
            self._consume_message_queue(central_log)

    def _consume_message_queue(self, central_log):
        while not self.log_queue.empty():
            log_message = self.log_queue.get()
            central_log.write(log_message)

    def stop(self):
        self.__stop = True
