# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0

import logging
import time

import pytest

import log_service
import pytest_helper
from slog_config import PlainSocketHandler

LOG_SERVER_PORT = 50008


@pytest.fixture(name="result_file_name")
def setup():
    server = log_service.LogServer(port=LOG_SERVER_PORT)
    server.start()
    test_logger = logging.getLogger('')
    test_logger.setLevel(logging.DEBUG)
    handler = PlainSocketHandler(port=LOG_SERVER_PORT)
    test_logger.addHandler(handler)
    logging.warn("my message")
    server.stop()
    server.join()
    time.sleep(.5)
    return server.get_log_file()


def test_append_log(result_file_name):
    with open(result_file_name, 'r') as result_file:
        found = False
        for message in result_file:
            if 'my message' in message:
                found = True
    assert found


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
