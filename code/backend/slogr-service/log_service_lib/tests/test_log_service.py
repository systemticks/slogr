# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0

import socket
import time
from threading import Thread

import log_service
import pytest_helper
import slog

expected_result_messages = [
    '{"name":"sender1", "value":"dummy json data"}\n',
    '{"name":"sender1", "value":"more dummy json data"}\n',
    '{"name":"sender1", "value":"one of two"}\n',
    '{"name":"sender1", "value":"two of two"}\n',
    '{"name":"sender2", "value":"dummy json data"}\n',
    '{"name":"sender2", "value":"more dummy json data"}\n',
    '{"name":"sender2", "value":"one of two"}\n',
    '{"name":"sender2", "value":"two of two"}\n'
]

LOG_SERVER_PORT = 50007


def test_slog_import():
    # as this is parallel and therefore timing critical,
    # execute the test multiple times (50 times worked locally)
    for i in range(1, 5):
        with open(_produce_logs(), "r") as result_file:
            # make sure result file is closed before we read from it
            time.sleep(.5)
            result_messages = [line for line in result_file]
        assert set(expected_result_messages) - set(result_messages) == set() and set(
            result_messages) - set(expected_result_messages) == set()


def _produce_logs():
    server = log_service.LogServer(port=LOG_SERVER_PORT)
    server.start()
    sender1 = ClientTestSender("sender1")
    sender2 = ClientTestSender("sender2")
    sender1.start()
    sender2.start()
    time.sleep(.5)  # give the clients a chance to finish
    server.stop()
    server.join()
    return server.get_log_file()


class ClientTestSender(Thread):

    def __init__(self, name):
        Thread.__init__(self)
        self.name = name
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def run(self):
        self.sock.connect(('localhost', LOG_SERVER_PORT))
        self.sock.sendall(str.encode(
            f'{{"name":"{self.name}", "value":"dummy'))
        time.sleep(0.1)
        self.sock.sendall(b' json data"}\n')
        self.sock.sendall(str.encode(
            f'{{"name":"{self.name}", "value":"more dummy json data"}}\n'))
        time.sleep(0.1)
        self.sock.sendall(str.encode(
            f'{{"name":"{self.name}", "value":"one of two"}}\n{{"name":"{self.name}", "value":"two of two"}}\n'))
        self.sock.close()


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
