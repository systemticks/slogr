// Copyright (C) 2020 systemticks GmbH
// 
// This program and the accompanying materials are made
// available under the terms of the Eclipse Public License 2.0
// which is available at https://www.eclipse.org/legal/epl-2.0/
// 
// SPDX-License-Identifier: EPL-2.0

#include "gtest/gtest.h"

#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"

#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

#include "json.hpp"

using json = nlohmann::json;

int32_t count_msg_in_file(std::string path)
{
    std::ifstream infile(path);
    int32_t count = 0;
    std::string line;
    while (std::getline(infile, line))
    {
        count++;
    }

    return count;
}

std::string get_first_line(std::string path)
{
    std::ifstream infile(path);
    std::string line;
    std::getline(infile, line);
    return line;
}

TEST(message_test, content)
{
    try
    {
        auto my_logger = spdlog::basic_logger_mt("basic_logger", "basic-log.txt", true);
        spdlog::set_default_logger(my_logger);
        my_logger->set_pattern("{\"ts\":\"%Y-%m-%dT%T.%e\",\"appid\":\"dlt-parser\",\"component\":\"parser\",\"context\":\"state\",\"payload\":{\"message\":\"%v\"}}");
        spdlog::error("Welcome to spdlog!");
        my_logger->flush();
    }
    catch (const spdlog::spdlog_ex &ex)
    {
        std::cout << "Log init failed: " << ex.what() << std::endl;
    }

    json logline = json::parse(get_first_line("basic-log.txt"));

    EXPECT_EQ(count_msg_in_file("basic-log.txt"), 1);
    EXPECT_EQ(logline["appid"], "dlt-parser");
    EXPECT_EQ(logline["component"], "parser");
    EXPECT_EQ(logline["context"], "state");
    EXPECT_EQ(logline["payload"]["message"], "Welcome to spdlog!");
}