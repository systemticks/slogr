# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0

import pytest
import json

import dba
import pytest_helper
import grok_importer
from query_builder import Details, EqualsTo, StrTerminal


@pytest.fixture
def db():
    with dba.DBA() as db:
        yield db


def test_grok_import(db):
    importer = grok_importer.GrokImporter(
        db, ["grok_importer_lib/resources/example.grok"])
    importer.import_file("grok_importer_lib/resources/example.glog")

    db.set_query(None)

    result = db.get_events_from_to(1, 1)

    assert result[0][0] == 1
    assert result[0][1] == 1569484680000000
    assert result[0][2] == '{"loglevel": "INFO", "message": "Hello, World!"}'


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
