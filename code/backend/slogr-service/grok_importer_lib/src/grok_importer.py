# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0


import datetime
import json

import slog

from pygrok import Grok


class GrokImporter():

    def __init__(self, db, grok_importer_configs=["grok_importer_lib/resources/example.grok"]):
        self.db = db
        self.grok_importer_configs = grok_importer_configs

    def import_file(self, path):
        for next_grok_importer_config in self.grok_importer_configs:
            with open(next_grok_importer_config) as config:
                config_as_dict = json.load(config)
                pattern = config_as_dict['pattern']
                custom_patterns_as_dict = config_as_dict['custom_patterns']
                grok = Grok(pattern, custom_patterns=custom_patterns_as_dict)
                with open(path) as log_file:
                    for text in log_file:
                        slog.info_state("grok-importer",
                                        "Importing from text '{}' started.".format(text))
                        # Match
                        m = grok.match(text)

                        ts = datetime.datetime.fromisoformat(
                            m['ts']).timestamp() * 1000000
                        del m['ts']

                        details = json.dumps(m)

                        slog.info_state("grok-importer",
                                        "Details {}".format(details))

                        # TODO: Import events, should come from a generator
                        self.db.insert_events([(ts, details)])
                        slog.info_state("grok-importer",
                                        "Matched text {}".format(m))
                        slog.info_state("grok-importer",
                                        "Importing from text '{}' finished.".format(text))
