# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0


import datetime
import logging
from logging.handlers import SocketHandler

import slog

FORMATTER = logging.Formatter(
    fmt='{"ts":"%(asctime)s.%(msecs)03d", %(message)s}', datefmt='%Y-%m-%dT%H:%M:%S')


class SlogConfig():

    def __init__(self):
        self.cur_log_file_path = None
        self._activate_logging()

    def _activate_logging(self):
        self.cur_log_file_path = self._create_log_file_path()
        file_handler = logging.FileHandler(
            mode='w', filename=self.cur_log_file_path)
        file_handler.setFormatter(FORMATTER)
        file_handler.setLevel(level=logging.INFO)
        logging.getLogger().addHandler(file_handler)
        logging.getLogger().setLevel(level=logging.INFO)
        slog.info_state("core", 'Logging activated.')

    def _create_log_file_path(self):
        now = datetime.datetime.now()
        return 'slogger_service_{}-{}-{}_{}-{}.slog'.format(
            now.year, now.month, now.day, now.hour, now.minute)

    def get_cur_log_file_path(self):
        return self.cur_log_file_path


class PlainSocketHandler(SocketHandler):

    def __init__(self, port=50006):
        SocketHandler.__init__(self, 'localhost', port)

    def makePickle(self, record):
        if not self.formatter:
            self.formatter = FORMATTER
        message = self.formatter.format(record) + '\n'
        return message.encode()
