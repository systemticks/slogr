# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import json

import json_format
import slog


def rpccall():
    def decorate(func):
        def call(*args, **kwargs):
            func_name = func.__name__
            log_rpc("Request", func_name, args[1])
            result = func(*args, **kwargs)
            log_rpc("Response", func_name, result)
            return result
        return call
    return decorate


def rpcstream():
    def decorate(func):
        def call(*args, **kwargs):
            func_name = func.__name__
            log_rpc("NotificationStreamStart", func_name, args[1])
            for result in func(*args, **kwargs):
                log_rpc("Notification", func_name, result)
                yield result
            log_rpc("NotificationStreamStop", func_name, args[1])
        return call
    return decorate


def log_rpc(msg_type, msg_name, msg):
    slog.info_rpc("core", msg_type, msg_name, json_format.MessageToJson(
        message=msg, indent=0, including_default_value_fields=True).replace('\n', ''))
