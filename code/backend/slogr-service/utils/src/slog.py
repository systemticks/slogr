# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import json
import logging


def fatal_state(component, msg):
    msg = _create_state_log(component, msg, 'FATAL')
    logging.critical(msg)


def _create_state_log(component, msg, level):
    log_dict = {}
    log_dict['app'] = "slogger-service"
    log_dict['component'] = component
    log_dict['loglevel'] = level
    log_dict['context'] = "state"
    log_dict['payload'] = {}
    log_dict['payload']['message'] = msg

    return json.dumps(log_dict)[1:-1]


def err_state(component, msg):
    msg = _create_state_log(component, msg, 'ERROR')
    logging.error(msg)


def warn_state(component, msg):
    msg = _create_state_log(component, msg, 'WARN')
    logging.warning(msg)


def info_state(component, msg):
    msg = _create_state_log(component, msg, 'INFO')
    logging.info(msg)


def debug_state(component, msg):
    msg = _create_state_log(component, msg, 'DEBUG')
    logging.debug(msg)


def trace_state(component, msg):
    msg = _create_state_log(component, msg, 'TRACE')
    logging.debug(msg)


def info_rpc(component, msg_type, msg_name, params, level='INFO'):
    log_msg = """
    "app": "slogger-service",
    "component": "{0}",
    "loglevel": "{1}",
    "context": "ipc",
    "payload": {{"msg_type": "{2}","msg_name":"{3}","params": {4}}}
    """.replace('    ', '').replace('\n', '').format(component, level, msg_type, msg_name, params.replace('\n', '\\n')).strip()
    logging.info(log_msg)
