# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import sys

import ptvsd


def wait_for_debugger_if_needed(args={}):
    if args and args.debug:
        port = 3000
        print(f'slogr-service: Debug mode activated. Waiting on port {port}.')
        ptvsd.enable_attach(address=('localhost', port))
        ptvsd.wait_for_attach(30)
        
