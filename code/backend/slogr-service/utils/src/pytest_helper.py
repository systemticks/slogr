# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import os
import sys

import pytest

import debug
import slog_config

from shutil import copy

def run_test(file):
    conf = slog_config.SlogConfig()
    debug.wait_for_debugger_if_needed()
    result = pytest.main(["-s", file])
    bazel_test_out_dir = os.environ.get("TEST_UNDECLARED_OUTPUTS_DIR")
    copy(conf.get_cur_log_file_path(), bazel_test_out_dir, follow_symlinks=True)    
    sys.exit(result)
