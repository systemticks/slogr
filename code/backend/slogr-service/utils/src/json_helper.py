# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import json


def transform_top_level_to_dict(input):
    result = {}
    for key, value in json.loads(input).items():
        result[key] = str(value).replace("'", '"')
    return result
