# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import queue


class Notifier():

    def __init__(self):
        self.queue = queue.Queue()

    def start(self):
        while True:
            item = self.queue.get()
            if item == None:
                break
            yield item

    def notify(self, item):
        self.queue.put(item)

    def stop(self):
        self.queue.put(None)
