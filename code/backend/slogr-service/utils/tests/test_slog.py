# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import json
import sys

import pytest

import pytest_helper
import slog


def test_create_state_log_simple():
    result = json.loads(
        '{' + slog._create_state_log('core', 'My state', 'INFO') + '}')

    assert result['app'] == 'slogger-service'
    assert result['component'] == 'core'
    assert result['loglevel'] == 'INFO'
    assert result['context'] == 'state'
    assert result['payload']['message'] == 'My state'


def test_create_state_log_linebreak():
    msg = """My state
    """

    result = json.loads(
        '{' + slog._create_state_log('core', msg, 'INFO') + '}')

    assert result['payload']['message'] == 'My state\n    '


def test_create_state_log_dict_in_msg():

    result = json.loads(
        '{' + slog._create_state_log('core', '{"My": "state"}', 'INFO') + '}')

    assert result['app'] == 'slogger-service'
    assert result['component'] == 'core'
    assert result['loglevel'] == 'INFO'
    assert result['context'] == 'state'
    assert result['payload']['message'] == '{"My": "state"}'


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
