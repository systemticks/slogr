# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import json_helper
import pytest_helper


def test_json_transform_top_level():
    input = '{"key1":"value1","key2": "value2", "key3":{"key31":"value31"},"key4":4, "key5":["value51","value52"]}'
    output = json_helper.transform_top_level_to_dict(input)

    assert output['key1'] == 'value1'
    assert output['key2'] == 'value2'
    assert output['key3'] == '{"key31": "value31"}'
    assert output['key4'] == '4'
    assert output['key5'] == '["value51", "value52"]'


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
