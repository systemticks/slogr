# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import datetime
import json

import slog


class SlogImporter():

    def __init__(self, db):
        self.db = db

    def import_file(self, path):
        slog.info_state("slog-importer",
                        "Importing from file {} started.".format(path))
        with open(path, 'r') as file:
            for line in file:
                self.db.insert_events(
                    self._convert_slog_line_to_db_tuple(line))
        slog.info_state("slog-importer",
                        "Importing from file {} finished.".format(path))

    def _convert_slog_line_to_db_tuple(self, line):
        line_as_json = json.loads(line)
        ts = datetime.datetime.fromisoformat(
            line_as_json['ts']).timestamp() * 1000000
        app = line_as_json['app']
        comp = line_as_json['component']
        context = line_as_json['context']
        loglevel = line_as_json['loglevel']
        payload = json.dumps(line_as_json['payload'])
        details = ''.join(
            ['{"app":"', app, '",', '"component":"', comp, '",', '"context":"', context, '",', '"loglevel":"', loglevel, '",', '"payload":', payload, '}'])
        yield (ts, details)
