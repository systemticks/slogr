# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import json

import pytest_helper
import slog_importer 


def test_slog_import():
    example = """
    {"ts":"2020-02-07T16:56:49.300", "app": "slogger-service","component": "core","loglevel": "INFO","context": "state","payload": {"message": "Logging activated."}}
    """

    importer = slog_importer.SlogImporter(None)
    result = next(importer._convert_slog_line_to_db_tuple(example))

    ts = result[0]
    details = json.loads(result[1])

    assert ts == 1581094609300000
    assert details['app'] == "slogger-service"
    assert details['component'] == "core"
    assert details['loglevel'] == "INFO"
    assert details['context'] == "state"
    assert details['payload']['message'] == "Logging activated."


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
