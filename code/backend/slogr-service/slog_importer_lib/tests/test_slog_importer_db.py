# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import pytest

import dba
import pytest_helper
import slog_importer
from query_builder import Details, EqualsTo, StrTerminal


@pytest.fixture
def db():
    with dba.DBA() as db:
        yield db


def test_slog_import(db):
    db.register_listener(_number_of_events_listener)
    importer = slog_importer.SlogImporter(db)
    importer.import_file("slog_importer_lib/resources/my.slog")
    db.set_query(EqualsTo(Details('payload.message'),
                          StrTerminal('Logging activated.')))

    assert pytest.num_of_events == 1


def pytest_namespace():
    return {'num_of_events': -1}


def _number_of_events_listener(events):
    pytest.num_of_events = events


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
