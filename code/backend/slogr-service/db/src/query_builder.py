# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


class CreateEventsTable():
    def build(self):
        return '''
            CREATE TABLE events( \
                TS INTEGER NOT NULL, \
                DETAILS JSON);
        '''


class DropEventsTable():
    def build(self):
        return "DROP TABLE IF EXISTS events;"


class CountEventsTable():
    def build(self):
        return "SELECT COUNT(rowid) FROM events;"


class DropEventsQueryTable():
    def build(self):
        return "DROP TABLE IF EXISTS filtered_events;"


class CountEventsQueryTable():
    def build(self):
        return "SELECT COUNT(rowid) FROM filtered_events;"


class InsertEvents():
    def build(self):
        return "INSERT INTO EVENTS (TS, DETAILS) VALUES (?,?)"


class GetEventsFromTo():
    def __init__(self, fromRowId, toRowId):
        self.fromRowId = fromRowId
        self.toRowId = toRowId

    def build(self):
        return "SELECT rowid, ts, details FROM filtered_events WHERE rowid >= {0} and rowid <= {1};".format(self.fromRowId, self.toRowId)


class DistinctFields():
    def build(self):
        return "select distinct fullkey from events, json_each(events.DETAILS);"


class DistinctValueForField():

    def __init__(self, value, prefix=''):
        self.value = value
        self.prefix = prefix

    def build(self):
        return "select distinct json_extract(DETAILS, '$.{0}') as vals from events where vals like '{1}%' limit 100;".format(self.value, self.prefix)


class AllEvents():
    def __init__(self):
        self.expr = None
        self.count = False

    def where(self, expr):
        self.expr = expr
        return self

    def build(self):
        if self.expr:
            return "CREATE TABLE filtered_events AS SELECT * FROM EVENTS WHERE {} ORDER BY TS;".format(self.expr.build())
        else:
            return "CREATE TABLE filtered_events AS SELECT * FROM EVENTS ORDER BY TS;"

# Test missing


class Not():
    def __init__(self, expr):
        self.expr = expr

    def build(self):
        return 'not ' + self.expr.build()


class BinOp():
    def __init__(self, op, expr1, expr2):
        self.op = op
        self.expr1 = expr1
        self.expr2 = expr2

    def build(self):
        return self.expr1.build() + ' ' + self.op + ' ' + self.expr2.build()


class And(BinOp):
    def __init__(self, expr1, expr2):
        BinOp.__init__(self, 'AND', expr1, expr2)

    def build(self):
        return '(' + self.expr1.build() + ' ' + self.op + ' ' + self.expr2.build() + ')'


class Or(BinOp):
    def __init__(self, expr1, expr2):
        BinOp.__init__(self, 'OR', expr1, expr2)

    def build(self):
        return '(' + self.expr1.build() + ' ' + self.op + ' ' + self.expr2.build() + ')'


class EqualsTo(BinOp):
    def __init__(self, expr1, expr2):
        BinOp.__init__(self, '==', expr1, expr2)


class LessThan(BinOp):
    def __init__(self, expr1, expr2):
        BinOp.__init__(self, '<', expr1, expr2)


class GreaterThan(BinOp):
    def __init__(self, expr1, expr2):
        BinOp.__init__(self, '>', expr1, expr2)


class Contains():
    def __init__(self, expr1, expr2):
        self.expr1 = expr1
        self.expr2 = expr2

    def build(self):
        expr2 = self.expr2.build()
        if expr2.startswith("'") and expr2.endswith("'"):
            expr2 = expr2[1:-1]
        return self.expr1.build() + " like " + "'%" + expr2 + "%'"

class In():
    def __init__(self, expr1, expr2):
        self.expr1 = expr1
        self.expr2 = expr2

    def build(self):
        return self.expr1.build() + " in " + "(" + self.expr2.build() + ")"


class Matches(BinOp):
    def __init__(self, expr1, expr2):
        BinOp.__init__(self, 'regexp', expr1, expr2)


class Terminal():
    def __init__(self, term):
        self.term = term

    def build(self):
        return self.term


class StrTerminal(Terminal):
    def __init__(self, term):
        Terminal.__init__(self, "'" + term + "'")

class StrTerminalList(Terminal):
    def __init__(self, term):
        Terminal.__init__(self, ', '.join(map(lambda t: "'" + t + "'", term)))

class NumTerminal(Terminal):
    def __init__(self, term):
        Terminal.__init__(self, str(term))


class Details(Terminal):
    def __init__(self, term):
        Terminal.__init__(self, "json_extract(DETAILS, '$.{}')".format(term))
