# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


class ListenerNotifier():

    def __init__(self):
        self.__listeners = []

    def __iadd__(self, listener):
        self.__listeners.append(listener)
        return self

    def __isub__(self, listener):
        self.__listeners.remove(listener)
        return self

    def notify(self, *args, **keywargs):
        for listener in self.__listeners:
            listener(*args, **keywargs)
