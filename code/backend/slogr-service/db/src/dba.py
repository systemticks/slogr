# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import os
import re
import sqlite3
import threading
import time

import slog
from listener_notifier import *
from query_builder import *

DB_PATH = ':memory:'


def regexp(expr, item):
    reg = re.compile(expr)
    return reg.fullmatch(item) is not None


class DBA():
    def __init__(self):
        slog.debug_state("db", "Init DBA.")
        self.num_filter_events = 0
        self.callback = None
        self.notifier = ListenerNotifier()
        self.lock_db = threading.Lock()

    def __enter__(self):
        slog.debug_state("db", "Enter DBA.")
        self.start()
        return self

    def start(self):
        self.connect()
        self.clear()

    def connect(self):
        self.lock_db.acquire()
        slog.info_state("db", "Open db.")
        self.db = sqlite3.connect(DB_PATH, check_same_thread=False)
        self.db.create_function("REGEXP", 2, regexp)
        self.lock_db.release()

    def clear(self):
        self.lock_db.acquire()
        slog.info_state("db", "Clear db.")
        self.db_exec(DropEventsQueryTable())
        self.db_exec(DropEventsTable())
        self.db_exec(CreateEventsTable())
        self.db_exec(AllEvents())
        self.num_filter_events = self.db_exec(
            CountEventsQueryTable()).fetchone()[0]
        self.lock_db.release()
        self.notifier.notify(events=self.num_filter_events)

    def db_exec(self, query):
        query_str = query.build()
        slog.info_state("sql", query_str)
        return self.db.execute(query_str)

    def register_listener(self, listener):
        self.notifier += listener

    def insert_events(self, events):
        self.lock_db.acquire()
        slog.info_state("db", "Insert events...")
        cur = self.db.executemany(InsertEvents().build(), events)
        self.db.commit()
        num_events = cur.rowcount
        slog.info_state("db", "Insert of {} events done.".format(num_events))
        self.lock_db.release()
        return num_events

    def set_query(self, query):
        self.lock_db.acquire()
        self.db_exec(DropEventsQueryTable())
        self.db.commit()
        if query:
            query_str = AllEvents().where(query)
            self.db_exec(query_str)
        else:
            self.db_exec(AllEvents())
        self.db.commit()
        self.num_filter_events = self.db_exec(
            CountEventsQueryTable()).fetchone()[0]
        self.lock_db.release()
        self.notifier.notify(events=self.num_filter_events)
        return self.num_filter_events

    def get_events_from_to(self, fromIndex, toIndex):
        self.lock_db.acquire()
        cur = self.db_exec(GetEventsFromTo(fromIndex, toIndex))
        # rage: Could have a performance impact if size is huge.
        result = cur.fetchall()
        self.lock_db.release()
        return result

    def get_distinct_fields(self):
        self.lock_db.acquire()
        cur = self.db_exec(DistinctFields())
        # rage: Could have a performance impact if first level of details has huge size.
        # Result looks likes this [('$.channel',''), ('$.payload',''), ...]
        # Take always first of tuple and remove '$.' from start
        result = [f[0][2:] for f in cur.fetchall()]
        self.lock_db.release()
        return result

    def get_distinct_value_for_field(self, value, prefix=''):
        self.lock_db.acquire()
        cur = self.db_exec(DistinctValueForField(value, prefix))
        # Result looks likes this [('DA1',''), ('DLTD',''), ('APP1',''), ...]
        # Take always first of tuple
        result = [v[0] for v in cur.fetchall()]
        self.lock_db.release()
        return result

    def count_all_events(self):
        self.lock_db.acquire()
        result = self.db_exec(CountEventsTable()).fetchone()[0]
        self.lock_db.release()
        return result

    def __exit__(self, exc_type, exc_val, exc_tb):
        slog.debug_state("db", "Exit DBA.")
        self.stop()

    def stop(self):
        self.lock_db.acquire()
        slog.info_state("db", "Close db.")
        self.db.close()
        self.lock_db.release()
