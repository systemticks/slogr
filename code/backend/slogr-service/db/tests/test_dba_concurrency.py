# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import threading
from string import Template

import pytest

import pytest_helper
from dba import *
from query_builder import *

details = Template("""
{
"channel": "$channel",
"payload": $payload
}
""".replace('\n', '').strip())


@pytest.fixture
def db():
    with DBA() as db:
        yield db


def pytest_namespace():
    return {'num_of_events': -1}


def test_dba_insert(db):
    db.register_listener(_number_of_events_listener)
    try:
        t1 = threading.Thread(target=_insert_events, args=(db,))
        t2 = threading.Thread(target=_insert_events, args=(db,))
        t1.start()
        t2.start()
        t1.join()
        t2.join()
    except:
        assert False

    assert db.count_all_events() == 20000


def _number_of_events_listener(events):
    pytest.num_of_events = events


def _insert_events(db):
    events = _gen_events()
    db.insert_events(events)


def _gen_events():
    for ts in range(10000):
        yield (ts * 100, details.substitute(channel='cpu.system', payload=1.0))


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
