# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


from string import Template

import pytest
import pytest_helper
from dba import *
from query_builder import *

details = Template("""
{
"channel": "$channel",
"payload": $payload
}
""".replace('\n', '').strip())


@pytest.fixture
def db():
    with DBA() as _db:
        yield _db


def pytest_namespace():
    return {'num_of_events': -1}


def test_dba_insert(db):
    assert db.insert_events(
        [(1000, details.substitute(channel='cpu.system', payload=1.0))]) == 1


def test_receiving_num_of_events(db):
    db.insert_events(
        [(1000, details.substitute(channel='cpu.system', payload=1.0))])
    db.register_listener(_number_of_events_listener)
    db.set_query(None)

    assert pytest.num_of_events == 1


def test_simple_filter(db):
    db.insert_events(
        [(1000, details.substitute(channel='cpu.system', payload=1.0))])
    db.insert_events(
        [(1000, details.substitute(channel='cpu.mem', payload=1000))])
    db.insert_events(
        [(2000, details.substitute(channel='cpu.system', payload=2.0))])
    db.insert_events(
        [(2000, details.substitute(channel='cpu.mem', payload=2000))])
    db.register_listener(_number_of_events_listener)
    db.set_query(
        Or(
            And(EqualsTo(Details("channel"), StrTerminal("cpu.system")),
                GreaterThan(Details("payload"), NumTerminal(1.0))),
            And(EqualsTo(Details("channel"), StrTerminal("cpu.mem")),
                LessThan(Details("payload"), NumTerminal(2000))),
        )
    )

    assert pytest.num_of_events == 2


def _number_of_events_listener(events):
    pytest.num_of_events = events


def test_get_events_from_to(db):
    db.insert_events(
        [(1000, details.substitute(channel='cpu.system', payload=1.0))])
    db.insert_events(
        [(1000, details.substitute(channel='cpu.mem', payload=1000))])
    db.insert_events(
        [(2000, details.substitute(channel='cpu.system', payload=2.0))])
    db.insert_events(
        [(2000, details.substitute(channel='cpu.mem', payload=2000))])
    db.set_query(None)

    result = db.get_events_from_to(2, 3)

    assert result[0][0] == 2
    assert result[0][1] == 1000
    assert result[0][2] == '{"channel": "cpu.mem","payload": 1000}'
    assert result[1][0] == 3
    assert result[1][1] == 2000
    assert result[1][2] == '{"channel": "cpu.system","payload": 2.0}'


def test_contains_num(db):
    db.insert_events(
        [(1000, details.substitute(channel='cpu.system', payload=0.1))])
    db.insert_events(
        [(1000, details.substitute(channel='cpu.mem', payload=1000))])
    db.insert_events(
        [(1000, details.substitute(channel='cpu.system', payload=0.2))])
    db.register_listener(_number_of_events_listener)

    db.set_query(Contains(Details('payload'), NumTerminal(1)))

    assert pytest.num_of_events == 2


def test_contains_str(db):
    db.insert_events(
        [(1000, details.substitute(channel='cpu.system', payload=0.1))])
    db.insert_events(
        [(1000, details.substitute(channel='cpu.mem', payload=1000))])
    db.insert_events(
        [(1000, details.substitute(channel='cpu.system', payload=0.2))])
    db.register_listener(_number_of_events_listener)

    db.set_query(Contains(Details('channel'), StrTerminal('mem')))

    assert pytest.num_of_events == 1

def test_in_strlist(db):
    db.insert_events([(1000, '{"channel": "messages", "payload": "one"}')])
    db.insert_events([(1100, '{"channel": "messages", "payload": "two"}')])
    db.insert_events([(1200, '{"channel": "messages", "payload": "three"}')])

    db.register_listener(_number_of_events_listener)

    db.set_query(In(Details('payload'),StrTerminalList(['one'])))    
    assert pytest.num_of_events == 1

    db.set_query(In(Details('payload'),StrTerminalList(['one', 'two'])))    
    assert pytest.num_of_events == 2

    db.set_query(In(Details('payload'),StrTerminalList(['one', 'two', 'three'])))    
    assert pytest.num_of_events == 3


def test_matches_str(db):
    db.insert_events([(1000, '{"channel": "messages", "payload": "word"}')])
    db.insert_events([(1100, '{"channel": "messages", "payload": "1234"}')])
    db.insert_events(
        [(1200, '{"channel": "messages", "payload": "anotherword"}')])
    db.insert_events(
        [(1300, '{"channel": "messages", "payload": "two words"}')])
    db.insert_events(
        [(1300, '{"channel": "messages", "payload": "special char#"}')])
    db.register_listener(_number_of_events_listener)

    db.set_query(Matches(Details('payload'), StrTerminal(r'[a-z]+\s[a-z]+')))

    assert pytest.num_of_events == 1
    assert db.get_events_from_to(
        1, 1)[0][2] == '{"channel": "messages", "payload": "two words"}'


def test_destinct_fields(db):
    db.insert_events(
        [(1000, details.substitute(channel='cpu.system', payload=0.1))])
    result = db.get_distinct_fields()
    assert len(result) == 2
    assert result[0] == 'channel'
    assert result[1] == 'payload'


def test_destinct_value_for_field(db):
    db.insert_events(
        [(1000, details.substitute(channel='messages', payload='"core"'))])
    db.insert_events(
        [(2000, details.substitute(channel='messages', payload='"db"'))])

    result = db.get_distinct_value_for_field('payload', 'c')
    assert len(result) == 1
    assert result[0] == 'core'


def test_count_all_events(db):
    db.insert_events(
        [(1000, details.substitute(channel='messages', payload='"core"'))])
    db.insert_events(
        [(2000, details.substitute(channel='messages', payload='"db"'))])

    # Query should not have any influence on size of all events
    db.set_query(Contains(Details('payload'), StrTerminal('core')))

    assert db.count_all_events() == 2


def test_clear_all_data(db):
    db.insert_events(
        [(1000, details.substitute(channel='messages', payload='"core"'))])
    db.insert_events(
        [(2000, details.substitute(channel='messages', payload='"db"'))])

    db.register_listener(_number_of_events_listener)
    db.set_query(None)
    db.clear()

    assert db.count_all_events() == 0
    assert pytest.num_of_events == 0


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
