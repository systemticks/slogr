# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import pytest_helper
from query_builder import *


def test_all_events():
    assert AllEvents().build(
    ) == 'CREATE TABLE filtered_events AS SELECT * FROM EVENTS ORDER BY TS;'


def test_match_channel_query():
    assert AllEvents().where(EqualsTo(Details('channel'), StrTerminal('cpu.system'))).build() == \
        "CREATE TABLE filtered_events AS SELECT * FROM EVENTS WHERE json_extract(DETAILS, '$.channel') == 'cpu.system' ORDER BY TS;"


def test_and_op():
    assert And(
        And(EqualsTo(Details('channel'), StrTerminal('cpu.system')),
            GreaterThan(Details('summary'), NumTerminal(0))),
        LessThan(Details('summary'), NumTerminal(100))).build() == "((json_extract(DETAILS, '$.channel') == 'cpu.system' AND json_extract(DETAILS, '$.summary') > 0) AND json_extract(DETAILS, '$.summary') < 100)"


def test_details():
    assert EqualsTo(Details('appid'), StrTerminal('bla')).build(
    ) == "json_extract(DETAILS, '$.appid') == 'bla'"


def test_not():
    assert Not(EqualsTo(Details('appid'), StrTerminal('bla'))).build(
    ) == "not json_extract(DETAILS, '$.appid') == 'bla'"


def test_get_events_from_to():
    assert GetEventsFromTo(1, 19).build(
    ) == "SELECT rowid, ts, details FROM filtered_events WHERE rowid >= 1 and rowid <= 19;"


def test_contains_str():
    assert Contains(Details('payload'), StrTerminal('road')).build(
    ) == "json_extract(DETAILS, '$.payload') like '%road%'"


def test_contains_num():
    assert Contains(Details('payload'), NumTerminal(0)).build(
    ) == "json_extract(DETAILS, '$.payload') like '%0%'"

def test_in_strlist():
    assert In(Details('appid'),StrTerminalList(['DA1', 'DLTD'])).build(
    ) == "json_extract(DETAILS, '$.appid') in ('DA1', 'DLTD')"

def test_matches():
    assert Matches(Details('payload'), StrTerminal(r'ab*')).build(       
    ) == "json_extract(DETAILS, '$.payload') regexp 'ab*'"


def test_distinct_fields():
    assert DistinctFields().build(
    ) == "select distinct fullkey from events, json_each(events.DETAILS);"


def test_distinct_value_for_field():

    assert DistinctValueForField('ctxid').build(
    ) == "select distinct json_extract(DETAILS, '$.ctxid') as vals from events where vals like '%' limit 100;"

    assert DistinctValueForField('appid', 'D').build(
    ) == "select distinct json_extract(DETAILS, '$.appid') as vals from events where vals like 'D%' limit 100;"


def test_num_all_events():
    assert CountEventsTable().build() == "SELECT COUNT(rowid) FROM events;"


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
