# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0

"""This is only an example."""
import sys

import pytest_helper


def test_python_version():
    """This tests if python version is 3.7.4"""

    assert sys.version.split()[0] == "3.9.10"


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
