# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0


import sys
import time

import grpc
import pytest

import _thread
import count_down_latch
import pytest_helper
import slogr_service
import slogr_service_api.proto.slogr_service_pb2 as api_msgs
import slogr_service_api.proto.slogr_service_pb2_grpc as api_calls


@pytest.fixture
def server():
    server = slogr_service.SlogrServiceServicer()
    server.start()
    yield server
    server.stop()


@pytest.fixture
def stub(server):
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = api_calls.SlogrServiceStub(channel)
        yield stub


def test_import_slog_file(stub):
    response = stub.importFile(api_msgs.ImportFileRequest(
        path="slogr_service_lib/resources/my.slog", grokImporterPaths=[]))
    assert response.resultCode == api_msgs.ImportFileResponse.ImportFileResultCode.OK


def test_import_wrong_slog_file(stub):
    response = stub.importFile(
        api_msgs.ImportFileRequest(path="/path/to/logfile.slog", grokImporterPaths=[]))
    assert response.resultCode == api_msgs.ImportFileResponse.ImportFileResultCode.UNKNOWN_FAILURE


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
