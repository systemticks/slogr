# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0


import sys
import time

import grpc
import pytest

import _thread
import count_down_latch
import pytest_helper
import slogr_service
import slogr_service_api.proto.slogr_service_pb2 as api_msgs
import slogr_service_api.proto.slogr_service_pb2_grpc as api_calls


@pytest.fixture
def server():
    server = slogr_service.SlogrServiceServicer()
    server.start()
    yield server
    server.stop()


@pytest.fixture
def stub(server):
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = api_calls.SlogrServiceStub(channel)
        yield stub
        stub.unregisterOnImportFile(api_msgs.OnImportFileRequest())


def test_register_on_file_imported_without_import_before(stub):
    register_done = count_down_latch.CountDownLatch()
    try:
        _thread.start_new_thread(_wait_for_result, (stub, register_done))
    except:
        assert False

    register_done.wait()

    assert pytest.num_events_after_register == 0
    assert pytest.distinct_fields_after_register == []


def _wait_for_result(stub, register_done, num_iter=1):
    iterator = iter(stub.registerOnImportFile(
        api_msgs.OnImportFileRequest()))
    for _ in range(num_iter):
        response = next(iterator)
        pytest.num_events_after_register = response.numberEvents
        pytest.distinct_fields_after_register = response.distinctFields
    register_done.count_down()


def pytest_namespace():
    return {
        'num_events_after_register': -1,
        'distinct_fields_after_register': ['default']
    }


def test_register_on_file_imported_with_import_before(stub):
    stub.importFile(api_msgs.ImportFileRequest(
        path="slogr_service_lib/resources/my.slog"))
    register_done = count_down_latch.CountDownLatch()
    try:
        _thread.start_new_thread(_wait_for_result, (stub, register_done))
    except:
        assert False

    register_done.wait()

    assert pytest.num_events_after_register == 3
    assert pytest.distinct_fields_after_register == [
        'app', 'component', 'context', 'loglevel', 'payload']


def test_register_on_file_imported_with_import_after(stub):
    register_done = count_down_latch.CountDownLatch()
    try:
        _thread.start_new_thread(_wait_for_result, (stub, register_done, 2))
    except:
        assert False

    stub.importFile(api_msgs.ImportFileRequest(
        path="slogr_service_lib/resources/my.slog"))

    register_done.wait()

    assert pytest.num_events_after_register == 3
    assert pytest.distinct_fields_after_register == [
        'app', 'component', 'context', 'loglevel', 'payload']


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
