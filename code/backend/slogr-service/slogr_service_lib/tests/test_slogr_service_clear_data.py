# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0


import sys
import time

import grpc
import pytest

import _thread
import count_down_latch
import pytest_helper
import slogr_service
import slogr_service_api.proto.slogr_service_pb2 as api_msgs
import slogr_service_api.proto.slogr_service_pb2_grpc as api_calls


@pytest.fixture
def server():
    _server = slogr_service.SlogrServiceServicer()
    _server.start()
    yield _server
    _server.stop()


@pytest.fixture
def stub(server):
    with grpc.insecure_channel('localhost:50051') as channel:
        _stub = api_calls.SlogrServiceStub(channel)
        yield _stub
        _stub.unregisterOnImportFile(api_msgs.OnImportFileRequest())


def test_clear_data(stub):
    register_done = count_down_latch.CountDownLatch()
    try:
        _thread.start_new_thread(_wait_for_result, (stub, register_done, 3))
    except:
        assert False

    stub.importFile(api_msgs.ImportFileRequest(
        path="slogr_service_lib/resources/my.slog"))

    response = stub.clearData(api_msgs.ClearDataRequest())

    register_done.wait()

    assert response.resultCode == api_msgs.ClearDataResponse.ClearDataResultCode.OK
    assert pytest.num_events == 0
    assert pytest.distinct_fields == []


def _wait_for_result(stub, register_done, num_iter=1):
    iterator = iter(stub.registerOnImportFile(
        api_msgs.OnImportFileRequest()))
    for _ in range(num_iter):
        response = next(iterator)
        pytest.num_events = response.numberEvents
        pytest.distinct_fields = response.distinctFields
    register_done.count_down()


def pytest_namespace():
    return {
        'num_events': -1,
        'distinct_fields': ['unknown']
    }


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
