# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0


import datetime
import json
import os
import time
from pathlib import Path

import pytest

import pytest_helper
import slogr_service


@pytest.fixture(scope="module")
def server():
    server = slogr_service.SlogrServiceServicer()
    server.start()
    yield server
    server.stop()
    # os.remove(server.get_slog_config().get_cur_log_file_path())


def test_log_file_is_there(server):
    assert Path(server.get_slog_config().get_cur_log_file_path()).is_file()


def test_logging_activated_msg(server):
    logline = _read_log_file(
        server.get_slog_config().get_cur_log_file_path()).split('\n')[0].strip()
    print(logline)
    log_as_json = json.loads(logline)

    assert log_as_json['ts'] == datetime.datetime.fromisoformat(
        log_as_json['ts']).isoformat('T', 'milliseconds')
    assert log_as_json['app'] == 'slogger-service'
    assert log_as_json['component'] == 'core'
    assert log_as_json['loglevel'] == 'INFO'
    assert log_as_json['context'] == 'state'
    assert log_as_json['payload']['message'] == 'Logging activated.'


def _read_log_file(path):
    content = ''
    with open(path, 'r') as f:
        content = f.read()
    return content


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
