# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0


import os
import sys
import time
from shutil import copyfile

import grpc
import pytest

import _thread
import count_down_latch
import pytest_helper
import slogr_service
import slogr_service_api.proto.slogr_service_pb2 as api_msgs
import slogr_service_api.proto.slogr_service_pb2_grpc as api_calls


@pytest.fixture(scope="module")
def server():
    server = slogr_service.SlogrServiceServicer()
    copyfile(_resources_dir() + "/query1.py", "/tmp/query1.py")
    copyfile(_resources_dir() + "/query2.py", "/tmp/query2.py")
    server.start()
    yield server
    os.remove("/tmp/query1.py")
    os.remove("/tmp/query2.py")
    server.stop()


@pytest.fixture(scope="module")
def stub():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = api_calls.SlogrServiceStub(channel)
        stub.importFile(api_msgs.ImportFileRequest(
            path="slogr_service_lib/resources/my.slog"))
        yield stub
        stub.unregisterOnQueryChanged(api_msgs.OnQueryChangedRequest())


def pytest_namespace():
    return {
        'msg_size_before_query': -1,
        'msg_size_after_query': -1,
        'msg_size_after_query1_reloaded': -1,
        'msg_size_after_query2_reloaded': -1,
        'msg_size_after_query_reset': -1,
    }


def test_register_on_query_changed(server, stub):
    register_done = count_down_latch.CountDownLatch()
    query_changed = count_down_latch.CountDownLatch()
    query1_reloaded = count_down_latch.CountDownLatch()
    query2_reloaded = count_down_latch.CountDownLatch()
    query_reset = count_down_latch.CountDownLatch()
    try:
        _thread.start_new_thread(
            _wait_for_result, (stub, register_done, query_changed, query1_reloaded, query2_reloaded, query_reset))
    except:
        assert False

    register_done.wait()

    stub.changeQuery(api_msgs.QueryRequest(
        queryRootDirPath="/tmp", queryName="query1.MyQuery1"))

    query_changed.wait()

    copyfile(_resources_dir() + "/query1_reload.py", "/tmp/query1.py")
    stub.changeQuery(api_msgs.QueryRequest(
        queryRootDirPath="/tmp", queryName="query1.MyQuery1"))

    query1_reloaded.wait()

    copyfile(_resources_dir() + "/query1.py", "/tmp/query1.py")
    copyfile(_resources_dir() + "/query2_reload.py", "/tmp/query2.py")
    stub.changeQuery(api_msgs.QueryRequest(
        queryRootDirPath="/tmp", queryName="query1.MyQuery1"))

    query2_reloaded.wait()

    stub.changeQuery(api_msgs.QueryRequest(
        queryRootDirPath="/tmp", queryName=""))

    query_reset.wait()

    assert pytest.msg_size_before_query == 3
    assert pytest.msg_size_after_query == 2
    assert pytest.msg_size_after_query1_reloaded == 1
    assert pytest.msg_size_after_query2_reloaded == 0
    assert pytest.msg_size_after_query_reset == 3


def _wait_for_result(stub, register_done, query_changed, query1_reloaded, query2_reloaded, query_reset):
    iterator = iter(stub.registerOnQueryChanged(
        api_msgs.OnQueryChangedRequest()))
    response = next(iterator)
    pytest.msg_size_before_query = response.resultSize
    register_done.count_down()
    response = next(iterator)
    pytest.msg_size_after_query = response.resultSize
    query_changed.count_down()
    response = next(iterator)
    pytest.msg_size_after_query1_reloaded = response.resultSize
    query1_reloaded.count_down()
    response = next(iterator)
    pytest.msg_size_after_query2_reloaded = response.resultSize
    query2_reloaded.count_down()
    response = next(iterator)
    pytest.msg_size_after_query_reset = response.resultSize
    query_reset.count_down()


def _resources_dir():
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '../resources'))


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
