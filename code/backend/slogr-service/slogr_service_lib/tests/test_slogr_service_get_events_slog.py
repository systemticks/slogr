# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0


import os

import grpc
import pytest

import pytest_helper
import slogr_service
import slogr_service_api.proto.slogr_service_pb2 as api_msgs
import slogr_service_api.proto.slogr_service_pb2_grpc as api_calls


@pytest.fixture
def server():
    server = slogr_service.SlogrServiceServicer()
    server.start()
    yield server
    server.stop()


@pytest.fixture
def stub(server):
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = api_calls.SlogrServiceStub(channel)
        stub.importFile(api_msgs.ImportFileRequest(
            path="slogr_service_lib/resources/my.slog"))
        stub.changeQuery(api_msgs.QueryRequest(
            queryRootDirPath=_resources_dir(), queryName=""))
        yield stub


def _resources_dir():
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '../resources'))


def test_get_events(stub):

    response = stub.getEventsFromTo(
        api_msgs.GetEventsFromToRequest(fromIndex=1, toIndex=1))

    assert response.events[0].rowid == 1
    assert response.events[0].timestamp == 1581094609300000
    assert response.events[0].data['app'] == 'slogr-service'
    assert response.events[0].data['component'] == 'core'
    assert response.events[0].data['loglevel'] == 'INFO'
    assert response.events[0].data['context'] == 'state'
    assert response.events[0].data['payload'] == '{"message": "Logging activated."}'


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
