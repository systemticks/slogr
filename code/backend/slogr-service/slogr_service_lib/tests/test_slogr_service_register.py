# Copyright (C) 2020 systemticks GmbH
# 
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0


import sys
import time

import grpc
import pytest

import _thread
import count_down_latch
import pytest_helper
import slogr_service
import slogr_service_api.proto.slogr_service_pb2 as api_msgs
import slogr_service_api.proto.slogr_service_pb2_grpc as api_calls


@pytest.fixture(scope="module")
def server():
    server = slogr_service.SlogrServiceServicer()
    server.start()
    yield server
    server.stop()


@pytest.fixture(scope="module")
def stub():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = api_calls.SlogrServiceStub(channel)
        yield stub
        stub.unregisterOnQueryChanged(api_msgs.OnQueryChangedRequest())


def pytest_namespace():
    return {
        'msg_size_after_register': -1,
    }


def test_register_on_query_changed(server, stub):
    register_done = count_down_latch.CountDownLatch()
    try:
        _thread.start_new_thread(_wait_for_result, (stub, register_done))
    except:
        assert False

    register_done.wait()

    assert pytest.msg_size_after_register == 0


def _wait_for_result(stub, register_done):
    iterator = iter(stub.registerOnQueryChanged(
        api_msgs.OnQueryChangedRequest()))
    response = next(iterator)
    pytest.msg_size_after_register = response.resultSize
    register_done.count_down()


if __name__ == "__main__":
    pytest_helper.run_test(__file__)
