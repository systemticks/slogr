import importlib;
import query2

from query_builder import *


class MyQuery1():

    def __init__(self):
        importlib.reload(query2)

    def build(self):
        return query2.MyQuery2().build()