from query_builder import *

from query2 import *


class MyQuery2():

    def build(self):
        return EqualsTo(Details('payload'), StrTerminal('Hello, World')).build()
