# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0

import argparse
import json
import os
import sys
from concurrent import futures

import grpc

import debug
import json_helper
import notifier
import slog
import slog_config
import slog_importer
import slogr_service_api.proto.slogr_service_pb2 as api_msgs
import slogr_service_api.proto.slogr_service_pb2_grpc as api_calls
import strace
from dba import DBA
from query_builder import *
from slog_importer import SlogImporter
from grok_importer import GrokImporter


DEFAULT_PORT = 50051


class SlogrServiceServicer(api_calls.SlogrServiceServicer):

    def __init__(self, config=slog_config.SlogConfig()):
        self.slog_config = config
        self.db = DBA()
        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
        self.registered = False
        self.query_changed_notifier = notifier.Notifier()
        self.import_file_notifier = notifier.Notifier()

    def get_slog_config(self):
        return self.slog_config

    @strace.rpccall()
    def importFile(self, request, context):
        slog.info_state("slogger_service", "Importing for file {}.".format(
            request.path))
        return_code = api_msgs.ImportFileResponse.ImportFileResultCode.OK
        try:
            if self._get_file_extension(request.path) == '.slog':
                slog.info_state("slogger_service", "Using SlogImporter.")
                importer = SlogImporter(self.db)
            elif request.grokImporterPaths:
                slog.info_state("slogger_service", "Using GrokImporter.")
                importer = GrokImporter(self.db, request.grokImporterPaths)
            else:
                # TODO Should return error, only until UI is providing importer
                importer = GrokImporter(self.db)

            if importer:
                importer.import_file(request.path)
            else:
                slog.warn_state("slogger_service", "Could not find importer for file {}.".format(
                    request.path))
                return_code = api_msgs.ImportFileResponse.ImportFileResultCode.UNKNOWN_FAILURE
        except Exception as e:
            slog.err_state("slogger_service", str(e))
            return_code = api_msgs.ImportFileResponse.ImportFileResultCode.UNKNOWN_FAILURE

        if return_code == api_msgs.ImportFileResponse.ImportFileResultCode.OK:
            self.import_file_notifier.notify(
                api_msgs.OnImportFileNotification(numberEvents=self.db.count_all_events(), distinctFields=self.db.get_distinct_fields()))

        return api_msgs.ImportFileResponse(resultCode=return_code)

    def _get_file_extension(self, path):
        importer = None
        file_path_split = os.path.splitext(path)
        if len(file_path_split) > 0:
            file_ext = file_path_split[1]
            return file_ext

    @strace.rpcstream()
    def registerOnQueryChanged(self, request, context):
        try:
            self.db.register_listener(self._on_query_changed)
            self.db.set_query(None)
            yield from self.query_changed_notifier.start()
        except Exception as e:
            slog.err_state("slogger_service", str(e))

    def _on_query_changed(self, events):
        self.query_changed_notifier.notify(
            api_msgs.OnQueryChangedNotification(resultSize=events))

    @strace.rpccall()
    def changeQuery(self, request, context):
        try:
            if request.queryName == '':
                # Reset to all events
                self.db.set_query(None)
            else:
                self._update_sys_path(request)
                self.db.set_query(self._build_query(request))
                self._reset_sys_path(request)
        except Exception as e:
            slog.err_state("slogger_service", str(e))
        return api_msgs.QueryResponse()

    def _update_sys_path(self, request):
        if request.queryRootDirPath not in sys.path:
            sys.path.append('{}'.format(request.queryRootDirPath))

    def _build_query(self, request):
        module_name = os.path.splitext(request.queryName)[0]
        exec('import importlib; import {0}; importlib.reload({0});'.format(
            module_name))
        return eval('{}()'.format(request.queryName))

    def _reset_sys_path(self, request):
        if request.queryRootDirPath in sys.path:
            sys.path.remove('{}'.format(request.queryRootDirPath))

    @strace.rpccall()
    def getEventsFromTo(self, request, context):
        try:
            db_events = self.db.get_events_from_to(
                request.fromIndex, request.toIndex)
            api_events = list(map(lambda e: api_msgs.Events(
                rowid=e[0], timestamp=e[1], data=json_helper.transform_top_level_to_dict(e[2])), db_events))
        except Exception as e:
            slog.err_state("slogger_service", str(e))
        return api_msgs.GetEventsFromToResponse(events=api_events)

    @strace.rpccall()
    def getDistinctFields(self, request, context):
        try:
            distinct_fields = self.db.get_distinct_fields()
        except Exception as e:
            slog.err_state("slogger_service", str(e))
        return api_msgs.GetDistinctFieldsResponse(fields=distinct_fields)

    @strace.rpccall()
    def getDistinctValuesForField(self, request, context):
        try:
            if request.prefix:
                values_for_field = self.db.get_distinct_value_for_field(
                    request.field, request.prefix)
            else:
                values_for_field = self.db.get_distinct_value_for_field(
                    request.field)
        except Exception as e:
            slog.err_state("slogger_service", str(e))
        return api_msgs.GetDistinctValuesForFieldResponse(values=values_for_field)

    @strace.rpccall()
    def unregisterOnQueryChanged(self, request, context):
        try:
            self.query_changed_notifier.stop()
        except Exception as e:
            slog.err_state("slogger_service", str(e))
        return api_msgs.OnQueryChangedResponse()

    @strace.rpcstream()
    def registerOnImportFile(self, request, context):
        try:
            self.import_file_notifier.notify(
                api_msgs.OnImportFileNotification(numberEvents=self.db.count_all_events(), distinctFields=self.db.get_distinct_fields()))
            yield from self.import_file_notifier.start()
        except Exception as e:
            slog.err_state("slogger_service", str(e))

    @strace.rpccall()
    def unregisterOnImportFile(self, request, context):
        try:
            self.import_file_notifier.stop()
        except Exception as e:
            slog.err_state("slogger_service", str(e))
        return api_msgs.OnImportFileResponse()

    @strace.rpccall()
    def clearData(self, request, context):
        result_code = api_msgs.ClearDataResponse.ClearDataResultCode.OK
        try:
            self.db.clear()
        except Exception as e:
            slog.err_state("slogger_service", str(e))
            result_code = api_msgs.ClearDataResponse.ClearDataResultCode.UNKOWN_FAILURE

        if result_code == api_msgs.ClearDataResponse.ClearDataResultCode.OK:
            self.import_file_notifier.notify(
                api_msgs.OnImportFileNotification(numberEvents=0, distinctFields=[]))

        return api_msgs.ClearDataResponse(resultCode=result_code)

    def start(self, port=DEFAULT_PORT):
        self.db.start()
        api_calls.add_SlogrServiceServicer_to_server(self, self.server)
        self.server.add_insecure_port('[::]:' + str(port))
        self.server.start()
        print(f"slogr-service: Listening on {port}.")

    def stop(self):
        self.import_file_notifier.stop()
        self.query_changed_notifier.stop()
        self.server.stop(0)
        self.db.stop()


def _parse_cmd_args():
    parser = argparse.ArgumentParser(description='Backend service for slogr.')
    parser.add_argument(
        '--port',
        type=int,
        help='Port where slogr-service will listen for external calls.'
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='Activate debug mode on port 3000.'
    )

    return parser.parse_args()


def _get_port(args={}):
    return args.port if args and args.port else DEFAULT_PORT


if __name__ == "__main__":
    args = _parse_cmd_args()
    debug.wait_for_debugger_if_needed(args)
    print("slogr-service: Starting.")
    SERVICE = SlogrServiceServicer()
    SERVICE.start(_get_port(args))
    print("slogr-service: Started.")
    while True:
        USERINPUT = input("slogr-service: Please enter 'q' to exit:\n")
        if USERINPUT.lower() == 'q':
            SERVICE.stop()
            break
    print("slogr-service: Stopped.")
