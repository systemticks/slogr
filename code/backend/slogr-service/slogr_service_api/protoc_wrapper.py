# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0

import os
import subprocess
import sys

print('Programm arguments: {}'.format(sys.argv))

output_folder = sys.argv[1]
proto_files = [sys.argv[2]]

print('Output folder: {}'.format(output_folder))
print('Proto files: {}'.format(proto_files))

for file in proto_files:
    file_path = os.path.join(output_folder, file)
    print(file_path)
    args = "--proto_path={0} --python_out={0} --grpc_python_out={0} {1}".format(
        output_folder, file_path)
    cmd = "{} -m grpc_tools.protoc {}".format(sys.executable, args)
    print('Call: {}'.format(cmd))
    result = subprocess.call(cmd, shell=True)
    print("grpc generation result for '{0}': code {1}".format(file, result))
