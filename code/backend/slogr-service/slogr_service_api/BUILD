# Copyright (C) 2020 systemticks GmbH
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0

load("@pips//:requirements.bzl", "requirement")

genrule(
    name="gen_proto_files",
    srcs=[
        ":slogr_service_proto_files"
    ],
    cmd="""
    $(location :protoc_wrapper) $$PWD $(SRCS); echo $(SRCS); echo $$PWD; echo $(rootpaths :protoc_wrapper); cp -r slogr_service_api/proto $(@D)
    """,
    tools=[":protoc_wrapper"],
    outs=[
        "proto/slogr_service_pb2.py",
        "proto/slogr_service_pb2_grpc.py",
    ],
)

filegroup(
    name="slogr_service_proto_files",
    srcs=[
        "proto/slogr_service.proto",
    ]
)

py_library(
    name="lib",
    # imports=["proto", ],
    srcs=[
        ":gen_proto_files"
    ],
    visibility=["//visibility:public"],
)

py_binary(
    name="protoc_wrapper",
    srcs=["protoc_wrapper.py"],
    deps=[requirement('grpcio'), requirement('grpcio-tools')],
    visibility=["//visibility:public"]
)
