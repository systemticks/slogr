// Copyright (C) 2020 systemticks GmbH
//
// This program and the accompanying materials are made
// available under the terms of the Eclipse Public License 2.0
// which is available at https://www.eclipse.org/legal/epl-2.0/
//
// SPDX-License-Identifier: EPL-2.0

syntax = "proto3";

option java_package = "de.systemticks.slogr.api";

package slogrservice;

service SlogrService {
  rpc importFile(ImportFileRequest) returns (ImportFileResponse);

  rpc registerOnQueryChanged(OnQueryChangedRequest)
      returns (stream OnQueryChangedNotification);
  rpc unregisterOnQueryChanged(OnQueryChangedRequest)
      returns (OnQueryChangedResponse);

  rpc changeQuery(QueryRequest) returns (QueryResponse);
  rpc changeQueryDir(QueryDirRequest) returns (QueryDirResponse);

  rpc getEventsFromTo(GetEventsFromToRequest) returns (GetEventsFromToResponse);

  rpc getDistinctFields(GetDistinctFieldsRequest)
      returns (GetDistinctFieldsResponse);
  rpc getDistinctValuesForField(GetDistinctValuesForFieldRequest)
      returns (GetDistinctValuesForFieldResponse);

  rpc registerOnImportFile(OnImportFileRequest)
      returns (stream OnImportFileNotification);
  rpc unregisterOnImportFile(OnImportFileRequest)
      returns (OnImportFileResponse);

  rpc clearData(ClearDataRequest) returns (ClearDataResponse);
}

message ImportFileRequest {
  string path = 1;
  repeated string grokImporterPaths = 2;
}

message ImportFileResponse {
  enum ImportFileResultCode {
    OK = 0;
    UNKNOWN_FAILURE = 1;
  }
  ImportFileResultCode resultCode = 1;
}

message OnQueryChangedRequest {}

message OnQueryChangedNotification { int32 resultSize = 1; }

message OnQueryChangedResponse {
  enum OnQueryChangedResultCode {
    OK = 0;
    UNKNOWN_FAILURE = 1;
  }
  OnQueryChangedResultCode resultCode = 1;
}

// queryRootDirPath must be absolute path to root of query folder
// queryName must be queryFileName + '.' + queryName, e.g.
// query1.MyQuery1 if query1.py is filename and MyQuery1 is query name
message QueryRequest {
  // deprecated - use changeQueryDir instead
  string queryRootDirPath = 1;
  string queryName = 2;
}

message QueryResponse {
  enum QueryResponseResultCode {
    OK = 0;
    UNKNOWN_FAILURE = 1;
  }
  QueryResponseResultCode resultCode = 1;
}

// queryRootDirPath must be absolute path to root of query folder
message QueryDirRequest { string queryRootDirPath = 1; }

message QueryDirResponse {
  enum QueryDirResponseResultCode {
    OK = 0;
    UNKNOWN_FAILURE = 1;
  }
  QueryDirResponseResultCode resultCode = 1;
}

// Returns events from fromIndex to toIndex
// of the actual query
message GetEventsFromToRequest {
  // inclusive starting from 1
  int32 fromIndex = 1;
  // inclusive
  int32 toIndex = 2;
}

message GetEventsFromToResponse {
  enum GetEventsFromToResultCode {
    OK = 0;
    UNKNOWN_FAILURE = 1;
  }
  GetEventsFromToResultCode resultCode = 1;
  repeated Events events = 2;
}

message Events {
  int32 rowid = 1;
  int64 timestamp = 2;
  map<string, string> data = 3;
}

message GetDistinctFieldsRequest {}

message GetDistinctFieldsResponse {
  enum GetDistinctFieldsResultCode {
    OK = 0;
    UNKNOWN_FAILURE = 1;
  }
  GetDistinctFieldsResultCode resultCode = 1;
  repeated string fields = 2;
}

message GetDistinctValuesForFieldRequest {
  string field = 1;
  string prefix = 2;
}

message GetDistinctValuesForFieldResponse {
  enum GetDistinctValuesForFieldResultCode {
    OK = 0;
    UNKNOWN_FAILURE = 1;
  }
  GetDistinctValuesForFieldResultCode resultCode = 1;
  repeated string values = 2;
}

message OnImportFileRequest {}

message OnImportFileNotification {
  int32 numberEvents = 1;
  repeated string distinctFields = 2;
}

message OnImportFileResponse {
  enum OnImportFileResultCode {
    OK = 0;
    UNKNOWN_FAILURE = 1;
  }
  OnImportFileResultCode resultCode = 1;
}

message ClearDataRequest {}

message ClearDataResponse {
  enum ClearDataResultCode {
    OK = 0;
    UNKNOWN_FAILURE = 1;
  }
  ClearDataResultCode resultCode = 1;
}
