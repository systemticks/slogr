# slogr

## Rapid system issue isolation, based on DLT logs

**slogr** is at home in large scale automotive projects.
Lots of issue tickets have piled up and want to be burned down.

**slogr** provides a way to analyze DLT logs with an easy to use [log query language](doc/use_dsl.md).
IDE features like syntax completion and validation help you write even complex queries with ease. A backing database returns results with minimal delay.
Existing queries can be persisted, shared and reused on any DLT log file.

![slogr](doc/img/slogr.gif)

**slogr** is currently in early development.
Have a look at our [roadmap](doc/roadmap.md) to see, what we plan for the first
release. If you like to speed up development, we are happy to discuss 
[ways of sponsoring](doc/sponsoring.md).

In case you get stuck somewhere [here](doc/support.md) is how to get support.


| **Learn more!** |
|-|
| [**Launch slogr**](doc/launch.md)<br/>Run slogr in various deployment variants! |
| [**User instructions**](doc/use.md)<br/>Learn how to use slogr! |
| [**Build instructions**](doc/build.md)<br/>Learn how to build slogr! |
| [**Roadmap**](doc/roadmap.md)<br/>See, where we are going! |
| [**Sponsoring**](doc/sponsoring.md)<br/>Here's how you can support us! |
| [**Support**](doc/support.md)<br/>How can we help you? |
| [**Disclaimer**](doc/disclaimer.md)<br/>slogr is currently pre-alpha. |
| [**License**](LICENSE)<br/>slogr is published under Eclipse Public License 2.0. |
| [**Legal Notice**](doc/notice.md)<br/>Third party content |


<!--


## Usecase: Fast ticket rejection with log files
    - Tons of tickets with error reports (log files)
    - Find tickets where team is responsible for root cause 
    - Takes a lot of time to analyze log files manually. 

## Why ticket rejection is time consuming
    - Weakly structured log data
    - Inconsistant log data
    - Tooling:
        - Scripting: 
            - Analysis workflow limited
            - Boilerplate
        - UI-Based: 
            - Automation workflow not supported
            - Search/Filter possibilities restricted

## How slogger improves fast ticket rejection
    - Slogger workflow:
        - Import logs
            - DLT files can be imported
                - with storage header 
                - no FIBEX
                - non-verbose if payload contains readable text
                - verbose argument lists with all primitive argument types
                - available customer logs payload should be consistent and readable at least to the extent the DLT Viewer shows
                - visible columns
                    - log level is visible
                    - timestamp is taken from the standard header. if timestamp is not available, import should fail.
                    - app id
                    - ctx id
                    - payload
            - import is blocking queries
                - [] test if import while importing is possible
                    - if not, grey-out Import menu entry
            - if a query is active, new data will lead to another execution of the query    
            - Menu entry to "Import DLT Files"
                - multi selection possible
                    - Files will be imported sequentially into the same DB
            - Loading animation instead of progress bar
            - Deny loading a file twice
                - Absolute path cannot be the same
                - Decision will be made in Frontend
            - Loaded files will be displayed 
                - If possible, show list below Explorer, or new side-panel, or status bar drop-down
                - will be handled in Frontend
                - File name will be visible immediately, absolute path on hover, (maybe not MVP: absolute path can be copied via context menu)
        - Pre-analyze (with existing rules)
            - 
        - Analyze fast (with existing filters)
            - log data can be filtered with DSL
                - Resolving available log data fields
                - Logical expressions for refined queries
                    - and
                    - or
                    - not
                    - brackets
                - Nested queries
                - Comparison
                    - equals
                    - contains
                    - RegEx (matches)
                    - in
                    - starts with
                    - ends with
                - JSON path notation
                - List Access
                    - first (nr)
                    - last (nr)
                    - element (nr)
                - Time based comparison
                    - >/>= (single element only or integer timestamp copied from table)
                    - </<= (single element only or integer timestamp copied from table)
                    - and (to simulate between)
                    - +/- (nr of seconds)
                - Grouping of queries (e.g. phoneCall.herbert)
                - Importing of queries from other files
            - a search can be applied to currently loaded data
                - Search expressions will be written with DSL Editor
                - Additional UI Elements needed for 
                    - jump/step to previous/next match
                    - number of matches
                - Rows in table of matched result will be highlighted
                - By convention the matched result list (list of indices) is limited to 1000 elements
            - Filter & Search UI element
                - One apply button for both features
                - 
            - Editing supports
                - Syntax highlighting
                - Code completion
                - Validation
                - Cross referencing
                - Outline
                - Refactoring
            - Query results are displayed in a Table
                - columns
                    - are sized by the user
                    - initial values are included in deployed package
                    - on shutdown the column with is persisted in combination with the imported file type and reloaded on start
                    - in case a different file format is loaded after restart, it is set to the defaults
                - select and copy
                    - is possible for single cells
                    - timestamp will be handled as number only
        - Definition and usage of RegEx patterns in DSL
            - Re-use grok-like syntax (check if it's possible to avoid 'pattern' as mandatory keyword)
            - No selectors in first version
            - Allow inline regex expression and referenced grok-like expressions
            - keyword to be defined for 'matches'

        - Define validation rules
            - No full UI integration. But at least triggering needed from within UI
            - Executed "Unit-test"-like
            

        (- Share filter and rules)
-->
