#!/bin/bash

# *******************************************************************************
# * Copyright (c) 2020 systemticks GmbH
# *
# * This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License 2.0
# * which accompanies this distribution, and is available at
# * https://www.eclipse.org/legal/epl-2.0/
# *
# * SPDX-License-Identifier: EPL-2.0
# *
# * Contributors:
# *     systemticks GmbH - initial API and implementation
# *******************************************************************************/

VERBOSE=$1
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCAN_DIR="$SCRIPT_DIR/../../code/backend $SCRIPT_DIR/../../code/app/extensions"


# Read ignore-file regex from .license-ignore
declare regex
regex=""

while IFS= read -r line
do
  if [[ -n $line && $line != \#* ]]; then
    if [[ -n $regex ]]; then
      regex="$regex|$line"
    else
      regex="$line"
    fi
  fi
done < <(cat $SCRIPT_DIR/.license-ignore)

if [[ $VERBOSE == "-v" ]]; then
  echo "Using following regex: $regex"
fi

declare -i result
result=0

while read file; do
  if [ -f "$file" ]; then
    if !(head -10 "$file" | grep -q Copyright);then
      if [[ $file =~ $regex ]]; then
        if [[ $VERBOSE == "-v" ]]; then
          echo "Ignoring $file."
        fi
      else
        echo "No License Header found in $file."
        result=1
      fi
    fi
  # else
  #   echo "Ignoring directory $file"
  fi
done < <(find $SCAN_DIR)


if [[ $result > 0 ]]; then
  echo "Error: License header not found!"
  exit $result
fi

