#!/bin/bash

# *******************************************************************************
# * Copyright (c) 2020 systemticks GmbH
# *
# * This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License 2.0
# * which accompanies this distribution, and is available at
# * https://www.eclipse.org/legal/epl-2.0/
# *
# * SPDX-License-Identifier: EPL-2.0
# *
# * Contributors:
# *     systemticks GmbH - initial API and implementation
# *******************************************************************************/

DEFAULT_PYENV_ROOT=~/.pyenv
EXPORT_PYENV_ROOT="export PYENV_ROOT=\"$DEFAULT_PYENV_ROOT\""
EXPORT_PATH='export PATH="$PYENV_ROOT/bin:$PATH"'
EVAL_PYENV_STR='eval "$(pyenv init -)"'
EVAL_PYENV_VIRT_STR='eval "$(pyenv virtualenv-init -)"'


main() {
    if [ -n "$PYENV_ROOT" ]; then
        echo "PYENV install dir: $PYENV_ROOT"
    else
        PYENV_ROOT="$DEFAULT_PYENV_ROOT"
        echo "Set PYENV_ROOT to $PYENV_ROOT"
    fi 
    PWD=`pwd`
    num_args=$1
    args=$2

    check_cmd_args $num_args
    parse_cmd_args $args
    if [ "$HELP" == "YES" ]; then
        show_help
    elif [ "$INSTALL" == "YES" ]; then
        install_pyenv
        install_service_venv
    elif [ "$UPDATE" == "YES" ]; then
        if [ -x "$(command -v pyenv)" ]; then
            pyenv update
        else
            echo "pyenv not found. Install it."
            install_pyenv
        fi
        install_service_venv
    elif [ "$REMOVE" == "YES" ]; then
        rm_pyenv
    fi
}

check_cmd_args() {
    num_args=$1
    if [ "$num_args" == 0 ]; then
        UPDATE=YES
    elif [ "$num_args" -ne 1 ]; then
        show_help
        exit 1
    fi
}

show_help() {
    cat <<EOF
Usage: bazel run //tools/pyenv:install -- [options]

-h|--help           Shows this help.
-i|--install        Install pyenv
-u|--update         Update or install pyenv [DEFAULT]
-r|--remove         Remove pyenv 
EOF
}

parse_cmd_args() {
    for i in "$@"
    do
    case $i in
        -h|--help)
        HELP=YES
        shift # past argument with no value
        ;;
        -i|--install)
        INSTALL=YES
        shift # past argument with no value
        ;;
        -u|--update)
        UPDATE=YES
        shift # past argument with no value
        ;;
        -r|--remove)
        REMOVE=YES
        shift # past argument with no value
        ;;
        *)
        ;;
    esac
    done
}

install_pyenv(){
    if [ -x "$(command -v pyenv)" ]; then
        echo 'pyenv already installed.'
    else
        echo 'pyenv will be installed.'
        install_apt_pkgs
        add_pyenv_to_path
        curl https://pyenv.run | bash
    fi
}

install_apt_pkgs() {
    CMD='apt-get install -y build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl git'
    SUDO=''
    if [ $EUID -ne 0 ]; then
        SUDO='sudo'
    fi
    eval "DEBIAN_FRONTEND=noninteractive $SUDO $CMD"
}

add_pyenv_to_path() {
    eval "export PYENV_ROOT=\"$PYENV_ROOT\""
    eval 'export PATH="$PYENV_ROOT/bin:$PATH"'

    add_str_to_file ~/.bashrc "$EXPORT_PYENV_ROOT"
    add_str_to_file ~/.bashrc "$EXPORT_PATH"
    add_str_to_file ~/.bashrc "$EVAL_PYENV_STR"
    add_str_to_file ~/.bashrc "$EVAL_PYENV_VIRT_STR"

    add_str_to_file ~/.zshrc "$EXPORT_PYENV_ROOT"
    add_str_to_file ~/.zshrc "$EXPORT_PATH"
    add_str_to_file ~/.zshrc "$EVAL_PYENV_STR"
    add_str_to_file ~/.zshrc "$EVAL_PYENV_VIRT_STR"
}

add_str_to_file() {
    file=$1
    str=$2

    if grep -Fxq "$str" "$file"
    then
        echo "$str already in $file."
    else
        echo "$str in $file"
        echo "$str" >> "$file"
    fi
}

install_service_venv() {
    if pyenv versions | grep -Fxq "  slogr-service"
    then
        echo "slogr-service virtual env already installed."
    else   
        pyenv install 3.7.4
        pyenv virtualenv 3.7.4 slogr-service
    fi
    echo "Updating slogr-service dependencies..."
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
    pyenv activate slogr-service
    pip install -r tools/pyenv/req-slogr-service.txt
    echo "Updating slogr-service dependencies...done."
}

rm_pyenv() {
    rm_str_from_file ~/.bashrc "$EXPORT_PYENV_ROOT"
    rm_str_from_file ~/.bashrc "$EXPORT_PATH"
    rm_str_from_file ~/.bashrc "$EVAL_PYENV_STR"
    rm_str_from_file ~/.bashrc "$EVAL_PYENV_VIRT_STR"
    
    rm_str_from_file ~/.zshrc "$EXPORT_PYENV_ROOT"
    rm_str_from_file ~/.zshrc "$EXPORT_PATH"
    rm_str_from_file ~/.zshrc "$EVAL_PYENV_STR"
    rm_str_from_file ~/.zshrc "$EVAL_PYENV_VIRT_STR"
    
    rm -rf $PYENV_ROOT
    UNSET $PYENV_ROOT
}

rm_str_from_file() {
    file=$1 
    str=$2

    if grep -Fxq "$str" "$file"
    then
        sed -i "s|$str||g" "$file"
    else
        echo "$str" not in "$file"
    fi
}


main $# $@


