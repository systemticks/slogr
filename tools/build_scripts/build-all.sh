#!/bin/bash

# *******************************************************************************
# * Copyright (c) 2020 systemticks GmbH
# *
# * This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License 2.0
# * which accompanies this distribution, and is available at
# * https://www.eclipse.org/legal/epl-2.0/
# *
# * SPDX-License-Identifier: EPL-2.0
# *
# * Contributors:
# *     systemticks GmbH - initial API and implementation
# *******************************************************************************/

BASEDIR=${PWD}

declare clean
clean=false
if [[ "$*" == *--clean* ]]
then
    clean=true
fi

# Build the slogr-service
echo "Start building slogr-service"
if [ ${clean} = true ]
    then
        cd "${BASEDIR}"/code/backend/slogr-service/
        bazel clean
        cd "${BASEDIR}"
        bazel clean
fi
bazel run //tools/pyenv:install
echo "Finished building slogr-service"

# Build the slogr-language-server
echo "Start building slogr-language-server"
cd "${BASEDIR}"/code/backend/slogr-language-server/
if [ ${clean} = true ]
    then
        ./gradlew clean
fi
./gradlew shadowJar
echo "Finished building slogr-language-server"

# Build the slogr-ui
echo "Start building slogr-ui"
cd "${BASEDIR}"/code/app/
if [ ${clean} = true ]
    then
        rm -rf node_modules
fi
yarn && yarn rebuild:browser 
if [[ "$*" == *electron* ]]
then
    yarn rebuild:electron
fi
echo "Finished building slogr-ui"
