## :thumbsup: Sponsoring

> NOTE: If you didn't already, please have a look at our 
[disclaimer](disclaimer.md) first!

**slogr** is initiated and currently developed and maintained by 
[systemticks](https://systemticks.de).

If you are thinking, how can I ...

* ... speed up development of slogr?
* ... request feature X?
* ... demonstrate my appreciation?
* ... provide feedback and critique?

You can currently do one of the following:

* create a [ticket](https://gitlab.com/systemticks/slogger/-/issues)
* tweet about _#slogr_ and [@systemticks](https://twitter.com/systemticks/)
* drop us an [e-mail](hello@systemticks.de)

We are happy to answer all inquiries about 

* financial support
* coding contributions
* bespoke feature requests
* proprietary extensions
* trainings and [support](support.md)

Please [get in touch](hello@systemticks.de)!
