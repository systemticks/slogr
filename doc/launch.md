# General Information

**slogr** can be built, deployed and executed in several ways.

It can be started as a self-contained desktop application or as a web-based application, running the UI in a browser and the backend on a server.

# Run slogr in a docker container

There is a pre-built docker image hosted on dockerhub, that can be used to run the slogr-backend in a docker container and the UI in a browser.

Pull the image and run the container:

`docker run -it --init -p 3000:3000 slogr/slogr-app:0.1`

When running the container for the very first time the image will be pulled foremost. This could take a few minutes. Please be patient. :)

Please wait until you see following output in your shell/console

```
slogr-service: Listening on 50051.
slogr-service: Started.
slogr-service: Please enter 'q' to exit:
```

You can now open a browser with

http://localhost:3000

As already mentioned above, the slogr-backend has access to the files on the server's filesysten, i.e. to the files in the docker container. Means, if you want to open a log file you need to get it into the container first.

You can either bind additional volumes when running the container (like "-v /my/local/folder:/home/slogr/morefiles") or you can upload files with the UI, as shown in following screenshot:

![slogr](../doc/img/upload-files.png)

# Run slogr as local web application

If you want to run slogr as a local web application you need to follow the [build instructions](build.md) first.

Once the build was successful you can start it with 

```
cd code/app/browser
yarn start
```

Please wait until you see following output in your shell/console

```
slogr-service: Listening on 50051.
slogr-service: Started.
slogr-service: Please enter 'q' to exit:
```

You can now open a browser with

http://localhost:3000


# Run slogr as local desktop application

If you want to run slogr as a local desktop application you need to follow the [build instructions](build.md) first.

Once the build was successful you can start it with 

```
cd code/app/electron
yarn start
```
