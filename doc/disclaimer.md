## :construction: Disclaimer

### Pre-alpha state

**slogr** is currently under development and in **pre-alpha state**. 
Even though we are using state-of-the-art technolgies and methodologies, we cannot guarantee, that **slogr** is already production ready.
That's why **using slogr is at own risk.**

We are planning for 4 stages:

|status|stage|description                                                                  |
|-|-        |-                                                                             |
|✔|Pre-alpha|fit for early demos conducted by experienced developers                       |
|-|Alpha    |an installable packet is available that can easily be executed and played with|
|-|Beta     |feature complete but not stable                                               |
|-|v1.0     |first stable release, please also have a look at our [roadmap](roadmap.md)    |