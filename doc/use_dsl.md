# The Log Query Language

The Log Query Language is one of the key features in **slogr**. It is designed to increase the ease of use regarding searching and filtering extensive log data.

It allows the expressive declaration of complex queries and comes with a full-featured editor, containing syntax check, syntax highlighting, code completion, refactoring, cross-referencing, quick fixes and much more.

The grammar is kept simple and does not require any major technical skills.

# Syntax Structure

We can break down the syntax of a query into following key elements: an optional name, fields, operators, values and keywords.

> **Note:** The Log Query Language is in an early stage. The concept and the structure (abstract syntax) will probably remain the same, but it might happen, that the names of keywords and operators (concrete syntax) will be slightly adapted in upcoming versions.


## Query Name

The query name in fact is optional. But only in case you have defined a query name your are able to refer to it later in other queries. You need to start with the keyword **query**, followed by the name:

```python
query HmiNavLogs: appid = "HMI" and ctxid = "NAVI"
```

In case you have a transient query and don't need it later in other queries you can omit the query name: 

```python
appid = "HMI" and ctxid = "NAVI"
```

## Fields and Values

The fields in the Log Query Language represent the message fields of a DLT message and are the same as the column headers in the log event table:

* ecuid
* appid
* ctxid
* payload

 Values are used to compare against a field to get a list of log elements.

 For fields and values the editors offers auto completion, depending on the content of your log data.

## Keywords

The available keywords are **group**, **query**, **pattern** and **import**. How and when they are used are explained in the subsequent chapters of this document.

## Operators

With the operators you can define logical operations and compare content of fields with given values.

### Logical Operators

Available logical operators are **and**, **or** and **not**. With round parenthesis **(...)** you can group and control the precedence of expressions.

Examples:

```python
appid = "HMI" and ctxid = "NAVI"
appid = "HMI" or ctxid = "NAVI"
```

```python
appid = "HMI" and not (ctxid = "NAVI")
not (appid = "HMI" or ctxid = "NAVI")
```

```python
ecuid = "ecu1" or (appid = "HMI" and not (ctxid = "NAVI"))
```

### Comparison Operators

Comparison operators are used to create a logical statement where a field is compared to a given value. 

> **Note:** As of now solely the comparison of strings is supported.

#### Equals

With the equals operator (=) you define, that a value **must exactly** match the field content.

```python
appid = "HMI"
```
#### Contains

The contains operator (~) let you search for a certain substring within a value. The search is not case sensitive.

```python
payload ~ "phone"
```

#### Pattern Matching

With the pattern matching operator (matches) you can apply a regular expression to your search statement. It will be executed as a full match.

```python
payload matches ".+#phone:\\d+"
```

# Enhanced Pattern Matching

Pattern Matching is a very powerful feature for defining expressive queries. But writing working and reasonable regular expressions can be a tedious work, that also requires some significant experience.

Therefore the Log Query Language supports the construction of complex patterns, composed of re-usable simple patterns.

The syntax is inspired by logstash grok, but is slightly adapted to integrate smoothly in the Log Query Language setup.

## Core patterns

Simple core patterns are defined with the keyword **pattern**, a name and the regular expression itself as plain text, like:

```python
pattern NUMBER: "\\d+"
pattern WORD: "\\w+"
pattern SPACE: "\\s+"
pattern DATA: ".*?"
pattern MSGTYPE: "(RP|EV|RQ)"
```

## Building complex patterns

With the help of those core patterns you can construct more complex patterns, whereat refering to an existing pattern is defined as %{SYNTAX:SEMANTIC}.

The SYNTAX is the name of the pattern that will match your text. 

The SEMANTIC is the identifier you give to the piece of text being matched. 

> **Note:** The SEMANTIC is currently ignored and will be used in latter releases.

A pattern can have alternating references to other patterns or plain textual regular expressions.

```python
pattern SERVICE: %{WORD:name}"#"%{NUMBER:instance}
pattern PARAMETERS: %{DATA}

pattern MESSAGE: %{SERVICE:service}":"%{MSGTYPE:type}":"%{WORD:method}"\\("%{PARAMETERS}"\\)"
```

The same pattern **MESSAGE** (from above) could also be expressed as **one** plain regex string instead:

```python
pattern MESSAGE: "\\w+#\\d+:(RP|EV|RQ):\\w+\\(.*?\\)"
```

# Search in structured data

Enhanced pattern matching is a powerful feature, in case your log data are only available in an unstructured manner, i.e. in a flat string.

But if you are lucky and the log data is already present in a json structure, the Log Query Language also allows to use a JSONPath-like notation for searching in JSON trees:

```python
payload["service"]["name"] = "Navigation"
```

> **Note:** The origin JSONPath notation supports dot notation as well as bracket notation for selecting child properties. We currently only support the bracket notation. Also more enhanced features like searching with indices, wildcards and filters are not supported yet.

# Cross Referencing

Another powerful feature is the capability to refer to already defined queries. This increases the re-usability tremendously, since you can build complex queries, based on available simple queries.

```python
query myQuery: appid = "HMI" and ctxid = "NAVI"

query phoneNumbers: myQuery and payload = ".+#phone:\\d+"
```

```python
pattern PHONE_NUMBER: ".+#phone:\\d+"
query myQuery: appid = "HMI" and ctxid = "NAVI"

query phoneNumbers: myQuery and payload = %{PHONE_NUMBER}
```

# Importing

Cross-referencing even works across multiple files, since you can import queries from other files.

```python
import "baseQueries.lql"

query phoneNumbers: myQuery and payload = %{PHONE_NUMBER}
```

> **Note:** Currently all Log Query Language (*.lql) files must be located in the same folder, to ensure that importing works correctly. 

# Grouping

Grouping is used for structuring coherent queries and patterns into separate modules. It can be seen as namespace feature. Queries and patterns within groups can be cross-referenced as long as the full qualified name (groupname + quername) is used.

```python
group base {
   query myQuery: appid = "HMI" and ctxid = "NAVI"
}

query guidance: base.myQuery and payload ~ "routeGuidance"
```

