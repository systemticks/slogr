# Legal Notice


**slogr** is published under [Eclipse Public License 2.0](LICENSE)

## Third party content

**slogr** has direct dependencies to following 3rd party libraries

### slogr-ui

theia/*

* Version: latest
* License: EPL 2.0
* Source Code: https://github.com/eclipse-theia/theia

xtsate

* Version: 4.9.1
* License: MIT
* Source Code: https://github.com/davidkpiano/xstate

moment (MIT)

* Version: 2.24.0
* License: MIT
* Source Code: https://github.com/moment/moment

phosphor/datagrid

* Version: 0.3.1
* License: BSD-3
* Source Code: https://github.com/phosphorjs/phosphor

log4js 

* Version: 6.1.1
* License: Apache 2.0
* Source Code: https://github.com/log4js-node/log4js-node

grpc/grpc-js

* Version: 0.7.2
* License: Apache 2.0
* Source Code: https://github.com/grpc/grpc-node/tree/master/packages/grpc-js

google-protobuf

* Version: 3.11.4
* License: BSD-3
* Source Code: https://github.com/protobufjs/protobuf.js

chokidar

* Version: 3.3.1
* License: MIT
* Source Code: https://github.com/paulmillr/chokidar

lodash.debounce

* Version: 4.0.8
* License: MIT
* Source Code: https://github.com/lodash/lodash

lodash.throttle

* Version: 4.1.1
* License: MIT
* Source Code: https://github.com/lodash/lodash

line-reader

* Version: 0.4.0
* License: MIT
* Source Code: https://github.com/nickewing/line-reader

rimraf

* Version: latest
* License: ISC
* Source Code: https://github.com/isaacs/rimraf

copyfiles

* Version: 2.2.0
* License: MIT
* Source Code: https://github.com/calvinmetcalf/copyfiles

replace-in-file

* Version: 5.0.2
* License: MIT
* Source Code: https://github.com/adamreisnz/replace-in-file

grpc_tools_node_protoc_ts

* Version: 2.5.10
* License: MIT
* Source Code: https://github.com/agreatfool/grpc_tools_node_protoc_ts

react-json-view-ts

* Version: 1.16.7
* License: MIT
* Source Code: https://github.com/mac-s-g/react-json-view

### slogr-language-server

Xtext

* Version: 2.20.0
* License: EPL 1.0
* Source Code: https://github.com/eclipse/xtext

org.eclipse.emf.mwe2.launch

* Version: 2.11.1
* License: EPL 2.0
* Source Code: https://github.com/eclipse/mwe/tree/master/plugins/org.eclipse.emf.mwe2.launch

protobuf-java 

* Version: 3.11.0
* License: BSD-3
* Source Code: https://github.com/protocolbuffers/protobuf/tree/master/java

protobuf-java-util

* Version: 3.11.0
* License: BSD-3
* Source Code: https://github.com/protocolbuffers/protobuf/tree/master/java/util

grpc/*

* Version: 1.26.0
* License: Apache 2.0
* Source Code: https://github.com/grpc/grpc-java

shadowJar

* Version: 5.1.0
* License: Apache 2.0
* Source Code: https://github.com/johnrengelman/shadow


### slogr-service

pytest

* Version: 5.4.1
* License: MIT
* Source Code: https://github.com/pytest-dev/pytest

grpcio-tools

* Version: 1.28.1
* License: Apache 2.0
* Source Code: https://github.com/grpc/grpc/tree/master/tools/distrib/python/grpcio_tools

ptvsd

* Version: 4.3.2
* License: MIT
* Source Code: https://github.com/microsoft/ptvsd

googletest

* Version: release-1.8.0
* License: BSD-3
* Source Code: https://github.com/google/googletest

spdlog

* Version: v1.5.0
* License: MIT
* Source Code: https://github.com/gabime/spdlog

kaitai struct compiler

* Version: 0.9-SNAPSHOT20190421.155424.87e6d43c
* License: GPLv3+
* Source Code: https://github.com/kaitai-io/kaitai_struct_compiler

kaitai struct C++/STL runtime

* Version: revision 3338c1a on master
* License: MIT
* Source Code: https://github.com/kaitai-io/kaitai_struct_cpp_stl_runtime
