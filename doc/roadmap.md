## Feature backlog

This is our feature backlog and high-level roadmap. 

If you have other feature ideas, please file a ticket in [Gitlab](https://gitlab.com/systemticks/slogger/-/issues) and tag it with the label *feature_request*.

### Planned for release 1.0

Following features are currently planned for Release 1.0. 

| **Feature**              | **Status**         | **Description**
|--------------------------|--------------------|-----------------
| **Data Import**          |                    |
| DLT File Import          | :heavy_check_mark: | Import single or multiple DLT log files
| DLT Viewer Filter Import |                    | Import user-defined filters from DLT Viewer and transform into Log Query Language compliant filter expressions
| Import Once              |                    | Save imported DLT log files in slogr native format. Files can then be re-opened without any loss of time
| **Filter & Search**      |                    |
| Event View               | :heavy_check_mark: | Contains a built-in query editor for writing filter and search expressions and a table, that shows the results, matching the given filter
| Log Query Language       | :heavy_check_mark: | Allows the expressive declaration of complex queries and comes with a full-featured editor, containing syntax check, syntax highlighting, code completion, refactoring, cross-referencing, quick fixes and much more.
| Filter                   | :heavy_check_mark: | Apply filter expressions, written with the Log Query Language
| Search                   |                    | Apply search expressions, written with the Log Query Language and highlight results. Can be combined with filters.
| **Validation**           |                    |
| Rules language           |                    | Expand the Log Query Language with the option to define validation rules. The rules can be applied to the logs for automatic checks of potential misbehaviour.
| **Collaboration**        |                    |
| Git extension            |                    | Share filter and search queries and validation rules in the team with underlying git support.

### Planned for future releases

These feature will be proberly implemented after Release 1.0. 

| **Feature**              | **Status**         | **Description**
|--------------------------|--------------------|-----------------
| **Scripting**            |                    |
| slogr service API        |                    | Provide an API for accessing slogr functionality programatically, e.g.: from the command line 
| Python support           |                    | Python language binding for the API and convenience function to execute scripts within slogr-ui
| **Data Import**          |                    |
| Decode on import         |                    | Hook to re-structure/decode unstructured log data during import
| **Visualizations**       |                    |
| Multiple Event Views     |                    | Support managing multiple event views, e.g. to compare different filter results with each other
| Markers                  |                    | Possibility to mark single or multiple log entries
| Charts                   |                    | Visualize numeric log data in appropriate time-line charts
| Graphs                   |                    | Visualize structured log data in graphs