# Launching

**slogr** can be executed as an web application or as an electron application, depending on how you have [built](doc/build.md) it.

## Launch as electron application

- `cd code/app/electron`
- `yarn start`

## Launch as browser application

- `cd code/app/browser`
- `yarn start`
- Open a browser and go to `localhost:3000`

> **Note**:
 <br />When you have freshly built **slogr** and launch it for the very first time some slogr-service modules will be compiled in the background. Wait until you see 
 <br />`> Start slogger-service`
 <br />`> Please enter 'q' to exit:` 
 <br />in the console logs and restart **slogr**. This needs only be done for the very first launch and will be fixed soon.


# Importing Logs

After launching slogr the **Imported Files** section is opened and you will be asked to import a file.

![slogr](../doc/img/start_page.png)

You can choose either importing a new file or selecting one from the **Recent Imports** list. Multiple files can be imported in one shot.
During an running import the UI is blocked and the loading status is displayed.

After an import has finished you can see the already loaded files in the **Currently Imported** section.

![slogr](../doc/img/files_loaded.png)

You can unload all imported data by selecting **clear all Data** in the Edit menu.

# Event View

After importing DLT logs into slogr you can now start analyzing them. The central place to go is the so-called **Event View**. It consists of two parts: 

* A built-in query editor for writing filter and search expressions
* A table, that shows the results, matching the given filter

## Query Editor

The event table has its own embedded query language editor. 

The query language is specifically designed for writing expressive and re-usable queries. Its syntax and examples are described [here](use_dsl.md) in more detail.

The **Event View**'s embedded editor is minimized on purpose (e.g. no line numbers, no outline, no minimap, etc.).
If you need to write more complex queries you should use the full-featured editor in the **Explorer View** instead.

The queries can be applied to the DLT logs filtered and the results will be shown immediately in the table below.

![slogr](../doc/img/slogr.gif)

## Event Table

The table shows the (filtered) DLT logs, where each DLT message fields has its own column. The table is always sorted by the timestamp of the logs. It has no paging concept, but allows an infinite virutal scrolling through the entire log content.

You can open the content of a selected cell in an own dialog by clicking onto it.

![slogr](../doc/img/selected_cell.png)

# Language Editor

slogr intergates the monaco editor from Visual Studio Code and thus benefits from its huge capabilities and features.

