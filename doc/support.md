## :eyeglasses: Support

If you found a :beetle: or want to request a feature, feel free to file a ticket 
[here](https://gitlab.com/systemticks/slogger/-/issues).

You can also reach out to us via [e-mail](hello@systemticks.de).