# General information

**slogr** is currently developed and tested under Linux only. systemticks GmbH is using Ubuntu 18.04 as development and runtime environment. Other operating systems might be supported in future.

**slogr** is based on a microservice architecture and consists of three major building blocks:

* slogr-ui (the user interface)
* slogr-service (the backend core services)
* slogr-language-server (the backend log query language server)

These modules require different runtime environments.

- slogr-ui development is using node-js (`> 10`) 
- slogr-service is using python3 (`> 3.7.4`).
- slogr-language-server is using JDK (`> 1.8.0`).

# Install build dependencies

As build pre-requsites you need to donwload and install following dependencies

## slogr-ui

Install slogr-ui dependencies
- `sudo apt install libx11-dev libxkbfile-dev curl`
- `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash`
- `nvm install 10`
- `curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -`
- `echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list`


## slogr-language-service

- `sudo apt update`
- `sudo apt -y install openjdk-8-jre`
- Install [gradle](http://yallalabs.com/devops/how-to-install-gradle-ubuntu-18-04-ubuntu-16-04/)

## slogr-service

Install slogr-service dependencies
- `sudo apt update`
- `sudo apt -y install curl gnupg2 python zip`
- `echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list`
- `curl https://bazel.build/bazel-release.pub.gpg | apt-key add -`
- `sudo apt update`
- `sudo apt -y install bazel`


# Build instructions

**slogr** can be build as a web application (running the UI in a browser) or as an electron application.

## Build all

For simplyfying the build process following script can be used to build all micro-services in one shot:

Browser Application:

`./tools/build_scripts/build-all.sh`

Electron Application:

`./tools/build_scripts/build-all.sh electron`

A clean build can be triggered with

`./tools/build_scripts/build-all.sh --clean`

## Build separately

Alternatively you can build the different services separately.
This is useful in case you have changes, that only impact a separate service.

### Build slogr-service
- `bazel run //tools/pyenv:install`

### Build slogr-language-server

- `cd code/backend/slogr-language-server`
- `./gradlew shadowJar`

### Build slogr-ui for browser

- `cd code/app`
- `yarn && yarn rebuild:browser`

### Build slogr-ui for electron

- `cd code/app`
- `yarn && yarn rebuild:electron`


## Tests

### Run slogr-service

- `cd code/backend/slogr-service`
- `bazel run //slogr_service:slogr_service`

### Run slogr-service tests

- `cd code/backend/slogr-service`
- `bazel test --config=unit //...`
- `bazel test --config=integration //...`

Note:
- Integration tests are trigger via grpc and labled with the bazel tag `integrationtest`. They cannot run in parallel. 
- Unit tests are all other internal tests and labled with the bazel tag `unittest`. They must be runnable in parallel. 

### Show slogr-service command line options

- `cd code/backend/slogger-service`
- `bazel run //slogr_service:slogr_service -- -h`

## Debug

### Debug slogr-service

Start slogr-service without debug
- `bazel run //slogr_service:slogr_service`

Debug slogr-service in Visual Studio Code:
- Start slogger-service with `bazel run //slogr_service:slogr_service -- --debug` 
- Start debug session in Visual Studio Code with F5.

Debug tests in Visual Studio Code:
- Start test with `--test_arg=--debug`, e.g. `bazel test --test_arg=--debug //test:test_python_version`
- Start debug session in Visual Studio Code with F5.

Know issue:
- All tests that import dlt files are die when run in debug mode (all tests in `code/backend/slogr-service/tests`)
