# slogr User Documentation

This is the user guide explaining the most important features of **slogr**.
Since **slogr** is in a very early stage, the documentation will be updated and improved gradually.

> **Note**: **slogr** is yet not available as a pre-built ready-to-use binary. Please read the [build instructions](doc/build.md) to learn how you can build **slogr** from its sources.

| **Learn more!** |
|-|
| [**User Guide**](use_ui.md)<br/>Learn how to use slogger! 
| [**Log Query Language**](use_dsl.md)<br/>Learn how to use the log query language for writing expressive queries |
